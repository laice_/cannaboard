package main

import (
	"bitbucket.org/laice_/cannaboard/FConfig"
	"bitbucket.org/laice_/cannaboard/FDB"
	"bitbucket.org/laice_/cannaboard/FDB/Tables"
	"bitbucket.org/laice_/cannaboard/lib"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"golang.org/x/time/rate"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type FServe struct {
	ServerConfig FConfig.ServerConfig
	DBConfig     FConfig.DBConfig
	DB           *FDB.DBPool
	Limiter      *rate.Limiter
}

func NewFServe() (fs *FServe) {
	fs = new(FServe)
	fs.Limiter = rate.NewLimiter(2, 5)
	fs.ServerConfig, fs.DBConfig = LoadConfiguration()
	db, err := FDB.New(fs.DBConfig)
	lib.C(err)
	fs.DB = db

	return
}

func main() {

	fmt.Printf("Starting server..\n")
	fserve := NewFServe()
	serverConfig, dbConfig := fserve.ServerConfig, fserve.DBConfig
	router := mux.NewRouter()
	db, err := FDB.New(dbConfig)
	if err != nil {
		panic(fmt.Sprintf("Failed to load DB:\n%s\n", err.Error()))
	}
	defer db.Close()

	router.HandleFunc("/fh/locations", GetLocations).Methods("GET")
	router.HandleFunc("/fh/inventory/{id}", GetInventoryByLocation).Methods("GET")
	router.HandleFunc("/fh/orders/{id}", GetOrdersByDate).Methods("GET")
	router.HandleFunc("/fh/orders/{id}/{createdAfter}", GetOrdersByDate).Methods("GET")
	router.HandleFunc("/fh/orders/{id}/{createdAfter}/{createdBefore}", GetOrdersByDate).Methods("GET")
	router.HandleFunc("/fh/orders/{id}/{createdAfter}/{createdBefore}/{limit}", GetOrdersByDate).Methods("GET")
	router.HandleFunc("/fh/orderDigest/{id}/{createdAfter}/{createdBefore}/{limit}", GetOrderDigestByDate).Methods("GET")
	router.HandleFunc("/fh/orderDigestPeriod/{period}/{id}/{createdAfter}/{createdBefore}/{limit}", fserve.GetOrderDigestByDateAndPeriod).Methods("GET")
	router.HandleFunc("/fh/itemCounts/{period}/{id}/{createdAfter}/{createdBefore}/{limit}", fserve.GetItemCountsByDate).Methods("GET")
	router.HandleFunc("/fh/categoryStats/{period}/{id}/{createdAfter}/{createdBefore}", fserve.GetCategoryStats).Methods("GET")
	router.HandleFunc("/fh/cannabinoidInfo/{pid}", GetCannabinoidInfo).Methods("GET")
	router.HandleFunc("/fh/weightTier/{pid}", GetWeightTier).Methods("GET")
	router.HandleFunc("/fh/cartItems/{oid}", GetCartItem).Methods("GET")
	router.HandleFunc("/fh/orderTotal/{oid}", GetOrderTotal).Methods("GET")
	router.HandleFunc("/fh/refresh", fserve.RefreshOrders).Methods("GET")
	router.HandleFunc("/fh/dow/{id}/{dow}", fserve.GetDOWStats).Methods("GET")

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost:8000", "http://compliance.puremmj.com", "https://compliance.puremmj.com"},
		AllowedHeaders:   []string{"Key"},
		AllowedMethods:   []string{"GET", "OPTIONS"},
		AllowCredentials: true,
		Debug:            false,
	})

	handler := c.Handler(fserve.limit(authHandler(router)))

	log.Fatal(http.ListenAndServeTLS(serverConfig.Port, serverConfig.Cert, serverConfig.Key, handler))

}

func (fs *FServe) limit(next http.Handler) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		if fs.Limiter.Allow() == false {
			http.Error(res, http.StatusText(429), http.StatusTooManyRequests)
			return
		}

		next.ServeHTTP(res, req)
	})
}

func authHandler(h http.Handler) http.Handler {

	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		if checkSecret(res, req) == false {
			return
		}
		h.ServeHTTP(res, req)
	})
}

func checkSecret(res http.ResponseWriter, req *http.Request) (auth bool) {

	sc, _ := LoadConfiguration()
	secret := req.Header.Get("Key")

	//fmt.Printf("Client Secret: %s\n", secret)

	for _, key := range sc.Secrets {
		if key == secret {
			return true
		}
	}

	unauthorized(res, req)

	return

}

func getIP(req *http.Request) (ip string) {

	ip = req.Header.Get("X-Forwarded-For")

	if ip != "" {
		return
	} else {
		ip = req.Header.Get("X-Real-IP")
		if ip != "" {
			return
		} else {
			fmt.Printf("Neither X-Forwarded-For nor X-Real-IP headers set, cannot get ip\n")
		}
	}

	return
}

func unauthorized(res http.ResponseWriter, req *http.Request) {

	fmt.Printf("Unauthorized request from: %s\n", getIP(req))

	res.WriteHeader(401)

	str := lib.RString(6, nil)
	io.WriteString(res, str)
}

func LoadConfiguration() (serverConfig FConfig.ServerConfig, dbConfig FConfig.DBConfig) {

	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))

	if err != nil {
		panic(err)
	}
	//	fmt.Printf("Loading config from: %s\n", dir)

	configFile := path.Join(dir, "config", "server.json")

	//	fmt.Printf("Config Path: %s\n", configFile)

	sc, err := ioutil.ReadFile(configFile)
	if err != nil {
		panic(err)
	}
	json.Unmarshal(sc, &serverConfig)

	configFile = path.Join(dir, "config", "db.json")
	sc, err = ioutil.ReadFile(configFile)
	if err != nil {
		panic(err)
	}
	json.Unmarshal(sc, &dbConfig)

	return
}

func GetServerConfig() (serverConfig FConfig.ServerConfig) {
	serverConfig, _ = LoadConfiguration()

	return
}

func GetFilters() (filters []int) {
	serverConfig := GetServerConfig()

	return serverConfig.Filter

}

func (fs *FServe) RefreshOrders(res http.ResponseWriter, req *http.Request) {

	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered at Refresh Orders", r)
		}
	}()

	go func() {

		p, err := exec.LookPath("../FFetch/FFetch")
		if err != nil {
			fmt.Println("Could not find FFetch, may need to compile?", err)
		}
		fmt.Println("FFetch path:", p)

		//go func() {
		ffetch := exec.Command(p)

		ffetch.Dir = path.Join("../FFetch")
		var out bytes.Buffer
		var er bytes.Buffer
		ffetch.Stdout = &out
		ffetch.Stderr = &er
		err = ffetch.Run()
		fmt.Printf("%s\n", out.String())
		fmt.Printf("%s\n", er.String())
		lib.C(err)

		//}()
	}()
	var empty []byte
	_, err := res.Write(empty)
	lib.C(err)

}

func (fs *FServe) GetDOWStats(res http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)

	dow := params["dow"]
	importid := params["id"]

	table, err := Tables.CartItemTable(Tables.CartItemsTableConfig{
		Pool: fs.DB,
	})
	lib.C(err)

	rows, err := table.GetCartStatisticDOW(importid, dow)

	lib.C(err)

	if len(rows) > 0 {
		lib.C(json.NewEncoder(res).Encode(rows))
	} else {
		_, err = io.WriteString(res, fmt.Sprintf(`{ "error" : "No dow stats found for %s on %s"}`, importid, dow))
	}

}

func GetLocations(res http.ResponseWriter, req *http.Request) {

	if (*req).Method == "OPTIONS" {
		return
	}

	fmt.Printf("All Locations request\n")

	_, dbConfig := LoadConfiguration()
	db, err := FDB.New(dbConfig)
	if err != nil {
		fmt.Printf("All Locations DB Error:\n%s\n", err.Error())
	}
	defer db.Close()

	locTable, err := Tables.LocationTable(Tables.LocationsTableConfig{
		Pool: db,
	})
	if err != nil {
		fmt.Printf("All Locations Table Error:\n%s\n", err.Error())
	}

	filter := GetFilters()

	locs, err := locTable.GetAllLocations(filter)

	if err != nil {
		fmt.Printf("Get All Locations Error:\n%s\n", err.Error())
	}

	lib.C(json.NewEncoder(res).Encode(locs))

}

func GetInventoryByLocation(res http.ResponseWriter, req *http.Request) {

	//if checkSecret(res, req) == false {
	//	return
	//}
	params := mux.Vars(req)

	fmt.Printf("Inventory by location request for %s\n", params["id"])

	_, dbConfig := LoadConfiguration()
	db, err := FDB.New(dbConfig)
	if err != nil {
		fmt.Printf("Inventory DB Error:\n%s\n", err.Error())
	}
	defer db.Close()

	locTable, err := Tables.InventoryTable(Tables.InventoriesTableConfig{
		Pool: db,
	})
	if err != nil {
		fmt.Printf("Inventory Table Error:\n%s\n", err.Error())
	}

	loc, err := strconv.Atoi(params["id"])
	if err != nil {
		fmt.Printf("Inventory Int Conversion Error: %s", err.Error())
	}
	inv, err := locTable.GetInventoryByLocationId(loc)

	if err != nil {
		fmt.Printf("Inventory Get All Locations Error:\n%s\n", err.Error())
	}

	json.NewEncoder(res).Encode(inv)

}

func GetOrdersByDate(res http.ResponseWriter, req *http.Request) {

	tz, err := time.LoadLocation("America/Denver")

	if err != nil {
		fmt.Printf("Error setting time zone in GetOrdersByDate:\n%s\n", err.Error())
	}

	fmt.Printf("Orders by date request\n")

	_, dbConfig := LoadConfiguration()
	fmt.Printf("got config\n")
	db, err := FDB.New(dbConfig)
	fmt.Printf("loaded db\n")
	if err != nil {
		fmt.Printf("Orders DB Error:\n%s\n", err.Error())
	}
	defer lib.C(db.Close())
	fmt.Printf("defered db\n")

	locTable, err := Tables.OrderTable(Tables.OrdersTableConfig{
		Pool: db,
	})
	if err != nil {
		fmt.Printf("Orders Table Error:\n%s\n", err.Error())
	}
	fmt.Printf("locTable good\n")

	params := mux.Vars(req)

	fmt.Printf("Params:\n%v\n", params)
	ca := params["createdAfter"]
	cb := params["createdBefore"]
	loc := params["id"]

	fmt.Printf("CreatedAfter: %s, CreatedBefore: %s, Id: %s\n", ca, cb, loc)

	if loc == "" {
		res.WriteHeader(404)
		_, err = io.WriteString(res, "Invalid Location/Import ID")
		if err != nil {
			fmt.Printf("Orders Table Error:\n%s\n", err.Error())
		}
		return
	}

	if ca == "" {
		ca = time.Now().Add(-36 * time.Hour).Format(time.RFC3339)
	} else {
		t, err := time.Parse("01-02-2006", ca)

		if err != nil {
			log.Fatal(fmt.Sprintf("Couldn't parse created after time: %s\n", err.Error()))
		}

		ca = t.Format(time.RFC3339)
	}

	if cb == "" {
		cb = time.Now().In(tz).Format(time.RFC3339)
	} else {
		t, err := time.Parse("01-02-2006", cb)
		if err != nil {
			log.Fatal(fmt.Sprintf("Couldn't parse created after time: %s\n", err.Error()))
		}

		cb = t.Format(time.RFC3339)

	}

	limit := params["limit"]
	lim := 0

	if limit != "" {
		lim, err = strconv.Atoi(limit)

		if err != nil {
			fmt.Printf("Orders Int Conversion Error:\n%s\n", err.Error())
		}
	}

	orders, err := locTable.GetOrdersByLocationIdBetweenDates(loc, ca, cb, lim)

	if err != nil {
		fmt.Printf("Orders Get All Locations Error:\n%s\n", err.Error())
	}

	if len(orders) > 0 {
		//
		//for _, ord := range orders {
		//	ord.CreatedAt = time.Parse
		//}

		err := json.NewEncoder(res).Encode(orders)
		if err != nil {
			fmt.Printf("Orders Table JSON Error:\n%s\n", err.Error())
		}
	} else {
		_, err := io.WriteString(res, fmt.Sprintf(`{ "error" : "No orders found for %s between %s and %s"}`, loc, ca, cb))
		if err != nil {
			fmt.Printf("Orders Table WriteString Error:\n%s\n", err.Error())
		}
	}

}

func GetCannabinoidInfo(res http.ResponseWriter, req *http.Request) {
	//fmt.Printf("Getting cannabinoid info..\n")

	_, dbConfig := LoadConfiguration()
	db, err := FDB.New(dbConfig)
	if err != nil {
		fmt.Printf("CBD DB Error:\n%s\n", err.Error())
	}
	defer db.Close()

	cbdTable, err := Tables.CannabinoidTable(Tables.CannabinoidsTableConfig{
		Pool: db,
	})

	if err != nil {
		fmt.Printf("CBD Table Error:\n%s\n", err.Error())
	}

	params := mux.Vars(req)

	pid := params["pid"]

	ipid, err := strconv.Atoi(pid)
	if err != nil {
		fmt.Printf("Error converting PID to int:\n%s\n", err.Error())
	}

	cbds, err := cbdTable.GetCannabinoidByProductId(ipid)

	if err != nil {
		fmt.Printf("CBD Table Error:\n%s\n", err.Error())
	}

	//spew.Dump(cbds)
	if len(cbds) > 0 {
		json.NewEncoder(res).Encode(cbds)
	} else {
		io.WriteString(res, fmt.Sprintf(`{ "error": "No cannabinoid info found for PID %s"}`, pid))
	}

}

func GetWeightTier(res http.ResponseWriter, req *http.Request) {

	_, dbConfig := LoadConfiguration()
	db, err := FDB.New(dbConfig)
	if err != nil {
		fmt.Printf("Weight Tier DB Error:\n%s\n", err.Error())
	}
	defer db.Close()

	weightTable, err := Tables.WeightTierTable(Tables.WeightTiersTableConfig{
		Pool: db,
	})

	if err != nil {
		fmt.Printf("Weight Tier Table Error:\n%s\n", err.Error())
	}

	params := mux.Vars(req)

	pid := params["pid"]

	ipid, err := strconv.Atoi(pid)
	if err != nil {
		fmt.Printf("Weight Tier Error converting PID to int:\n%s\n", err.Error())
	}

	wts, err := weightTable.GetWeightTiersByProductId(ipid)

	if err != nil {
		fmt.Printf("Weight Tier Table Error:\n%s\n", err.Error())
	}

	if len(wts) > 0 {
		json.NewEncoder(res).Encode(wts)
	} else {
		io.WriteString(res, fmt.Sprintf(`{ "error": "No weight tiers found for PID %s"}`, pid))
	}

}

func GetCartItem(res http.ResponseWriter, req *http.Request) {

	_, dbConfig := LoadConfiguration()
	db, err := FDB.New(dbConfig)
	if err != nil {
		fmt.Printf("Cart Item DB Error:\n%s\n", err.Error())
	}
	defer db.Close()

	cartTable, err := Tables.CartItemTable(Tables.CartItemsTableConfig{
		Pool: db,
	})

	if err != nil {
		fmt.Printf("Cart Item Table Error:\n%s\n", err.Error())
	}

	params := mux.Vars(req)

	oid := params["oid"]

	//ioid, err := strconv.Atoi(oid)
	if err != nil {
		fmt.Printf("Cart Item Error converting OID to int:\n%s\n", err.Error())
	}

	ci, err := cartTable.GetCartItemsByOrderId(oid)

	if err != nil {
		fmt.Printf("Cart Item Table Error:\n%s\n", err.Error())
	}

	if len(ci) > 0 {
		json.NewEncoder(res).Encode(ci)
	} else {
		io.WriteString(res, fmt.Sprintf(`{ "error": "No cart items found for ID %s"}`, oid))
	}

}

func GetOrderTotal(res http.ResponseWriter, req *http.Request) {

	_, dbConfig := LoadConfiguration()
	db, err := FDB.New(dbConfig)
	if err != nil {
		fmt.Printf("OrderTotal DB Error:\n%s\n", err.Error())
	}
	defer db.Close()

	totalTable, err := Tables.OrderTotalTable(Tables.OrderTotalsTableConfig{
		Pool: db,
	})

	if err != nil {
		fmt.Printf("OrderTotal Table Error:\n%s\n", err.Error())
	}

	params := mux.Vars(req)

	oid := params["oid"]

	//ioid, err := strconv.Atoi(oid)
	//if err != nil {
	//	fmt.Printf("OrderTotal Error converting OID to int:\n%s\n", err.Error())
	//}

	tt, err := totalTable.GetOrderTotalsByOrderId(oid)

	if err != nil {
		fmt.Printf("OrderTotal Table Error:\n%s\n", err.Error())
	}

	if len(tt) > 0 {
		json.NewEncoder(res).Encode(tt)
	} else {
		io.WriteString(res, fmt.Sprintf(`{ "error": "No totals found for ID %s"}`, oid))
	}

}

func GetOrderDigestByDate(res http.ResponseWriter, req *http.Request) {

	tz, err := time.LoadLocation("America/Denver")

	if err != nil {
		fmt.Printf("Error setting time zone in GetOrdersByDate:\n%s\n", err.Error())
	}

	fmt.Printf("Orders digest by date request\n")

	_, dbConfig := LoadConfiguration()
	db, err := FDB.New(dbConfig)
	if err != nil {
		fmt.Printf("Orders DB Error:\n%s\n", err.Error())
	}
	defer db.Close()

	salesTable, err := Tables.SaleDayTable(Tables.SalesDayTableConfig{
		Pool: db,
	})

	if err != nil {
		fmt.Printf("Orders Table Error:\n%s\n", err.Error())
	}

	params := mux.Vars(req)

	fmt.Printf("Params:\n%v\n", params)
	ca := params["createdAfter"]
	cb := params["createdBefore"]
	loc := params["id"]

	fmt.Printf("CreatedAfter: %s, CreatedBefore: %s, Id: %s\n", ca, cb, loc)

	if loc == "" {
		res.WriteHeader(404)
		_, err := io.WriteString(res, "Invalid Location/Import ID")
		if err != nil {
			fmt.Printf("Order Digest 404 Response Error:\n%s\n", err.Error())
		}

		return
	}

	if ca == "" {
		ca = time.Now().Add(-36 * time.Hour).Format(time.RFC3339)
	} else {
		t, err := time.Parse("01-02-2006", ca)

		if err != nil {
			log.Fatal(fmt.Sprintf("Couldn't parse created after time: %s\n", err.Error()))
		}

		ca = t.Format(time.RFC3339)
	}

	if cb == "" {

		cb = time.Now().In(tz).AddDate(0, 0, 2).Format(time.RFC3339)

	} else {
		t, err := time.Parse("01-02-2006", cb)
		if err != nil {
			log.Fatal(fmt.Sprintf("Couldn't parse created after time: %s\n", err.Error()))
		}

		cb = t.Format(time.RFC3339)

	}

	limit := params["limit"]
	lim := 0

	if limit != "" {
		lim, err = strconv.Atoi(limit)

		if err != nil {
			fmt.Printf("Orders Int Conversion Error:\n%s\n", err.Error())
			return
		}
	}

	//orders, err := locTable.GetOrdersByLocationIdBetweenDates(loc, ca, cb, lim)

	orders, err := salesTable.GetOrdersByLocationIdBetweenDates(loc, ca, cb, lim)

	if err != nil {
		fmt.Printf("Orders Get All Locations Error:\n%s\n", err.Error())
		return
	}

	var fullOrder []Tables.SalesDayRow

	for _, order := range orders {
		fullOrder = append(fullOrder, order)

	}

	fmt.Println(fullOrder)

	if len(fullOrder) > 0 {
		err = json.NewEncoder(res).Encode(fullOrder)

		if err != nil {
			fmt.Printf("SalesDay JSON Error:\n%s\n", err.Error())
			return
		}
	} else {
		_, err = io.WriteString(res, fmt.Sprintf(`{ "error" : "No orders found for %s between %s and %s"}`, loc, ca, cb))
		if err != nil {
			fmt.Printf("SalesDay WriteString Error:\n%s\n", err.Error())
			return
		}

	}

}

func (fs *FServe) GetOrderDigestByDateAndPeriod(res http.ResponseWriter, req *http.Request) {

	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in GetItemCountsByDate", r)
		}
	}()

	params := mux.Vars(req)
	period := params["period"]
	createdAfter := params["createdAfter"]
	createdBefore := params["createdBefore"]
	//limit := params["limit"]
	id := params["id"]

	if createdAfter != "" {
		ca, err := time.Parse("01-02-2006", createdAfter)
		lib.C(err)
		createdAfter = ca.Format(time.RFC3339)
	}

	if createdBefore != "" {
		cb, err := time.Parse("01-02-2006", createdBefore)
		lib.C(err)
		createdBefore = cb.Format(time.RFC3339)
	}

	period = strings.TrimSpace(period)

	fmt.Println("Period: ", period)

	switch period {
	case "hour":

		table, err := Tables.CartItemTable(Tables.CartItemsTableConfig{
			Pool: fs.DB,
		})
		lib.C(err)

		rows, err := table.GetCartStatisticHourly(id, createdAfter, createdBefore)

		lib.C(err)

		if len(rows) > 0 {
			lib.C(json.NewEncoder(res).Encode(rows))
		} else {
			_, err = io.WriteString(res, fmt.Sprintf(`{ "error" : "No hour order digest found for %s between %s and %s"}`, id, createdAfter, createdBefore))
		}

		//table, err := Tables.SaleDayTable(Tables.SalesDayTableConfig{
		//	Pool: fs.DB,
		//})
		//
		//lim, err := strconv.Atoi(limit)
		//
		//lib.C(err)
		//rows, err := table.GetOrdersByLocationIdBetweenDates(id, createdAfter, createdBefore, lim)
		//lib.C(err)
		//if len(rows) > 0 {
		//	lib.C(json.NewEncoder(res).Encode(rows))
		//} else {
		//	_, err = io.WriteString(res, fmt.Sprintf(`{ "error" : "No hour order digest found for %s between %s and %s"}`, id, createdAfter, createdBefore))
		//	lib.C(err)
		//}

		break
	case "week":
		table, err := Tables.CartItemTable(Tables.CartItemsTableConfig{
			Pool: fs.DB,
		})
		lib.C(err)

		rows, err := table.GetCartStatisticWeekly(id, createdAfter, createdBefore)

		lib.C(err)

		if len(rows) > 0 {
			lib.C(json.NewEncoder(res).Encode(rows))
		} else {
			_, err = io.WriteString(res, fmt.Sprintf(`{ "error" : "No hour order digest found for %s between %s and %s"}`, id, createdAfter, createdBefore))
		}
		//table, err := Tables.SaleDayTable(Tables.SalesDayTableConfig{
		//	Pool: fs.DB,
		//})
		//
		//lim, err := strconv.Atoi(limit)
		//
		//lib.C(err)
		//rows, err := table.GetOrdersByLocationIdBetweenDatesWeekly(id, createdAfter, createdBefore, lim)
		//lib.C(err)
		//if len(rows) > 0 {
		//	lib.C(json.NewEncoder(res).Encode(rows))
		//} else {
		//	_, err = io.WriteString(res, fmt.Sprintf(`{ "error" : "No week order digest found for %s between %s and %s"}`, id, createdAfter, createdBefore))
		//	lib.C(err)
		//}

		break
	case "month":
		table, err := Tables.CartItemTable(Tables.CartItemsTableConfig{
			Pool: fs.DB,
		})
		lib.C(err)

		rows, err := table.GetCartStatisticMonthly(id, createdAfter, createdBefore)

		lib.C(err)

		if len(rows) > 0 {
			lib.C(json.NewEncoder(res).Encode(rows))
		} else {
			_, err = io.WriteString(res, fmt.Sprintf(`{ "error" : "No hour order digest found for %s between %s and %s"}`, id, createdAfter, createdBefore))
		}
		//table, err := Tables.SaleDayTable(Tables.SalesDayTableConfig{
		//	Pool: fs.DB,
		//})
		//
		//lim, err := strconv.Atoi(limit)
		//
		//lib.C(err)
		//rows, err := table.GetOrdersByLocationIdBetweenDatesMonthly(id, createdAfter, createdBefore, lim)
		//lib.C(err)
		//if len(rows) > 0 {
		//	lib.C(json.NewEncoder(res).Encode(rows))
		//} else {
		//	_, err = io.WriteString(res, fmt.Sprintf(`{ "error" : "No month order digest found for %s between %s and %s"}`, id, createdAfter, createdBefore))
		//	lib.C(err)
		//}

		break
	case "year":
		table, err := Tables.CartItemTable(Tables.CartItemsTableConfig{
			Pool: fs.DB,
		})
		lib.C(err)

		rows, err := table.GetCartStatisticYearly(id, createdAfter, createdBefore)

		lib.C(err)

		if len(rows) > 0 {
			lib.C(json.NewEncoder(res).Encode(rows))
		} else {
			_, err = io.WriteString(res, fmt.Sprintf(`{ "error" : "No hour order digest found for %s between %s and %s"}`, id, createdAfter, createdBefore))
		}
		//table, err := Tables.SaleDayTable(Tables.SalesDayTableConfig{
		//	Pool: fs.DB,
		//})
		//
		//lim, err := strconv.Atoi(limit)
		//
		//lib.C(err)
		//rows, err := table.GetOrdersByLocationIdBetweenDatesYearly(id, createdAfter, createdBefore, lim)
		//lib.C(err)
		//if len(rows) > 0 {
		//	lib.C(json.NewEncoder(res).Encode(rows))
		//} else {
		//	_, err = io.WriteString(res, fmt.Sprintf(`{ "error" : "No year order digest found for %s between %s and %s"}`, id, createdAfter, createdBefore))
		//	lib.C(err)
		//}

		break
	case "day":
		fallthrough
	default:

		table, err := Tables.CartItemTable(Tables.CartItemsTableConfig{
			Pool: fs.DB,
		})
		lib.C(err)

		rows, err := table.GetCartStatisticDaily(id, createdAfter, createdBefore)

		lib.C(err)

		if len(rows) > 0 {
			lib.C(json.NewEncoder(res).Encode(rows))
		} else {
			_, err = io.WriteString(res, fmt.Sprintf(`{ "error" : "No hour order digest found for %s between %s and %s"}`, id, createdAfter, createdBefore))
		}
		//fmt.Println("Day request")
		//table, err := Tables.SaleDayTable(Tables.SalesDayTableConfig{
		//	Pool: fs.DB,
		//})
		//
		//lim, err := strconv.Atoi(limit)
		//
		//lib.C(err)
		//rows, err := table.GetOrdersByLocationIdBetweenDatesDaily(id, createdAfter, createdBefore, lim)
		//lib.C(err)
		//if len(rows) > 0 {
		//	lib.C(json.NewEncoder(res).Encode(rows))
		//} else {
		//	_, err = io.WriteString(res, fmt.Sprintf(`{ "error" : "No day order digest found for %s between %s and %s"}`, id, createdAfter, createdBefore))
		//	lib.C(err)
		//}
		break

	}

}

func (fs *FServe) GetItemCountsByDate(res http.ResponseWriter, req *http.Request) {

	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in GetItemCountsByDate", r)
		}
	}()

	params := mux.Vars(req)
	//period := params["period"]
	createdAfter := params["createdAfter"]
	createdBefore := params["createdBefore"]
	//limit := params["limit"]
	id := params["id"]

	if createdAfter != "" {
		ca, err := time.Parse("01-02-2006", createdAfter)
		lib.C(err)
		createdAfter = ca.Format(time.RFC3339)
	}

	if createdBefore != "" {
		cb, err := time.Parse("01-02-2006", createdBefore)
		lib.C(err)
		createdBefore = cb.Format(time.RFC3339)
	}

	table, err := Tables.CartItemTable(Tables.CartItemsTableConfig{
		Pool: fs.DB,
	})
	lib.C(err)

	CTrans, err := table.GetCartCounts(id, createdAfter, createdBefore)
	//fmt.Println("CTrans", CTrans)
	lib.C(err)
	lib.C(json.NewEncoder(res).Encode(CTrans))
	/*
		switch period {
		case "hour":
			//table, err := Tables.NewItemCountTable(Tables.ItemCountTableConfig{
			//	Pool: fs.DB,
			//})
			//
			//lim, err := strconv.Atoi(limit)
			//
			//lib.C(err)
			//rows, err := table.GetOrdersByLocationIdBetweenDates(id, createdAfter, createdBefore, lim)
			//lib.C(err)
			//if len(rows) > 0 {
			//	lib.C(json.NewEncoder(res).Encode(rows))
			//} else {
			//	_, err = io.WriteString(res, fmt.Sprintf(`{ "error" : "No item counts found for %s between %s and %s"}`, id, createdAfter, createdBefore))
			//	lib.C(err)
			//}

			table, err := Tables.CartItemTable(Tables.CartItemsTableConfig{
				Pool: fs.DB,
			})
			lib.C(err)

			CTrans, err := table.GetCartStatistic(id, createdAfter, createdBefore)
			//fmt.Println("CTrans", CTrans)
			lib.C(err)
			lib.C(json.NewEncoder(res).Encode(CTrans))

			break
		case "week":
			table, err := Tables.NewWeeklyItemCountTable(Tables.WeeklyItemCountTableConfig{
				Pool: fs.DB,
			})

			lim, err := strconv.Atoi(limit)

			lib.C(err)
			rows, err := table.GetOrdersByLocationIdBetweenDates(id, createdAfter, createdBefore, lim)
			lib.C(err)
			if len(rows) > 0 {
				lib.C(json.NewEncoder(res).Encode(rows))
			} else {
				_, err = io.WriteString(res, fmt.Sprintf(`{ "error" : "No item counts found for %s between %s and %s"}`, id, createdAfter, createdBefore))
				lib.C(err)
			}

			break
		case "month":
			table, err := Tables.NewMonthlyItemCountTable(Tables.MonthlyItemCountTableConfig{
				Pool: fs.DB,
			})

			lim, err := strconv.Atoi(limit)

			lib.C(err)
			rows, err := table.GetOrdersByLocationIdBetweenDates(id, createdAfter, createdBefore, lim)
			lib.C(err)
			if len(rows) > 0 {
				lib.C(json.NewEncoder(res).Encode(rows))
			} else {
				_, err = io.WriteString(res, fmt.Sprintf(`{ "error" : "No item counts found for %s between %s and %s"}`, id, createdAfter, createdBefore))
				lib.C(err)
			}

			break
		case "year":
			table, err := Tables.NewYearlyItemCountTable(Tables.YearlyItemCountTableConfig{
				Pool: fs.DB,
			})

			lim, err := strconv.Atoi(limit)

			lib.C(err)
			rows, err := table.GetOrdersByLocationIdBetweenDates(id, createdAfter, createdBefore, lim)
			lib.C(err)
			if len(rows) > 0 {
				lib.C(json.NewEncoder(res).Encode(rows))
			} else {
				_, err = io.WriteString(res, fmt.Sprintf(`{ "error" : "No item counts found for %s between %s and %s"}`, id, createdAfter, createdBefore))
				lib.C(err)
			}

			break
		case "day":
			fallthrough
		default:
			table, err := Tables.NewDailyItemCountTable(Tables.DailyItemCountTableConfig{
				Pool: fs.DB,
			})

			lim, err := strconv.Atoi(limit)

			lib.C(err)
			rows, err := table.GetOrdersByLocationIdBetweenDates(id, createdAfter, createdBefore, lim)
			lib.C(err)
			if len(rows) > 0 {
				lib.C(json.NewEncoder(res).Encode(rows))
			} else {
				_, err = io.WriteString(res, fmt.Sprintf(`{ "error" : "No item counts found for %s between %s and %s"}`, id, createdAfter, createdBefore))
				lib.C(err)
			}
			break

		}
	*/

}

func (fs *FServe) GetCategoryStats(res http.ResponseWriter, req *http.Request) {

	params := mux.Vars(req)
	period := params["period"]
	createdAfter := params["createdAfter"]
	createdBefore := params["createdBefore"]
	//limit := params["limit"]
	id := params["id"]

	table, err := Tables.CartItemTable(Tables.CartItemsTableConfig{
		Pool: fs.DB,
	})

	lib.C(err)

	//var transport Tables.CartStatTransport

	//transport.CategoryNames, transport.CategoryTotalSold, transport.CategoryTotalSales, transport.CategoryDates = table.GetCategoryStatistics(id, createdAfter, createdBefore, period)
	stats := table.GetCategoryStatistics(id, createdAfter, createdBefore, period)

	if len(stats) > 0 {
		lib.C(json.NewEncoder(res).Encode(stats))
	} else {
		_, err = io.WriteString(res, fmt.Sprintf(`{ "error" : "No category stats found for %s between %s and %s"}`, id, createdAfter, createdBefore))
		lib.C(err)
	}

}
