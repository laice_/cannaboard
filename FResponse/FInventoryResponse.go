package FResponse

import (
	. "bitbucket.org/laice_/cannaboard/FInventory"
	"encoding/json"
	"io"
)

//######################
//
//	FInventoryResponse
//
//**********************

type FInventoryResponse struct {
	Status int        `json:"status"`
	Data   []FProduct `json:"data"`
	Error  []string   `json:"error"`
}

func (fh *FInventoryResponse) getStatus() int {
	return fh.Status
}

func (fh *FInventoryResponse) getData() []FProduct {
	return fh.Data
}

func (fh *FInventoryResponse) getError() []string {
	return fh.Error
}

func (fh *FInventoryResponse) Decode(body io.Reader) {

	err := json.NewDecoder(body).Decode(&fh)

	if err != nil {
		panic(err)
	}

}

func (fh *FInventoryResponse) Parse(body io.Reader) *FInventoryResponse {

	fh.Decode(body)

	return fh

}
