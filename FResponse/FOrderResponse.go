package FResponse

import (
	"bitbucket.org/laice_/cannaboard/FOrders"
	"encoding/json"
	"fmt"
	"io"
	"time"
)

//######################
//
//	FOrderResponse
//
//**********************

type FOrderResponse struct {
	Total  int              `json:"total"`
	Orders []FOrders.FOrder `json:"orders"`
	Error  string           `json:"error"`
}

func (fh *FOrderResponse) getTotal() int {
	return fh.Total
}

func (fh *FOrderResponse) getOrders() []FOrders.FOrder {
	return fh.Orders
}

func (fh *FOrderResponse) Decode(body io.Reader) {

	err := json.NewDecoder(body).Decode(&fh)

	if err != nil {

		panic(err)
		return
	}

	if fh.Error != "" {
		panic(fmt.Sprintf("Order Response Error: %s", fh.Error))
	}

	tz, err := time.LoadLocation("America/Denver")
	if err != nil {
		panic(err)
	}

	for i, elem := range fh.Orders {
		created := elem.CreatedAt
		completed := elem.CompletedOn

		//layout := "2006-01-02T15:04:05.9Z0700"

		t, err := time.ParseInLocation(time.RFC3339, created, tz)

		//t = t.Add(-7 * time.Hour)
		//t =
		if err != nil {
			panic(err)
		}

		//fmt.Printf("Created:\n%s\n%s\n", created, t)
		fh.Orders[i].CreatedAt = t.Format(time.RFC3339)

		t, err = time.ParseInLocation(time.RFC3339, completed, tz)
		//t = t.Add(-7 * time.Hour) //.Format(time.)

		if err != nil {
			panic(err)
		}
		//fmt.Printf("\n%s\n%s\n", completed, t)

		fh.Orders[i].CompletedOn = t.Format(time.RFC3339)

	}

}

func (fh *FOrderResponse) Parse(body io.Reader) *FOrderResponse {

	fh.Decode(body)

	return fh

}
