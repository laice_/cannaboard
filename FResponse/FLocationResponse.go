package FResponse

import (
	. "bitbucket.org/laice_/cannaboard/FLocation"
	"encoding/json"
	"fmt"
	"io"
)

//######################
//
//	FLocationResponse
//
//**********************

type FLocationResponse struct {
	Status int         `json:"status"`
	Data   []FLocation `json:"data"`
	Error  []string    `json:"error"`
}

func (fh *FLocationResponse) getStatus() int {
	return fh.Status
}

func (fh *FLocationResponse) getData() []FLocation {
	return fh.Data
}

func (fh *FLocationResponse) getError() []string {
	return fh.Error
}

func (fh *FLocationResponse) Decode(body io.Reader) {

	err := json.NewDecoder(body).Decode(&fh)

	if err != nil {
		fmt.Println(err.Error())

	}

}

func (fh *FLocationResponse) Parse(body io.Reader) {

	fh.Decode(body)

}
