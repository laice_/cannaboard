package FResponse

import (
	"encoding/json"
	"io"
)

//######################
//
//	FResponse
//
//**********************

type FResponse struct {
	Status int      `json:"status"`
	Data   string   `json:"data"`
	Error  []string `json:"error"`
}

func (fh *FResponse) getStatus() int {
	return fh.Status
}

func (fh *FResponse) getData() string {
	return fh.Data
}

func (fh *FResponse) getError() []string {
	return fh.Error
}

func (fh *FResponse) Decode(body io.Reader) {

	err := json.NewDecoder(body).Decode(&fh)

	if err != nil {
		panic(err)
		return
	}

	/*
		buf := new(bytes.Buffer)
		buf.ReadFrom(body)
		s := buf.String()
		fmt.Println(s)
	*/
}

func (fh *FResponse) Parse(body io.Reader, callback func(fh *FResponse)) {

	fh.Decode(body)

	callback(fh)

}

//######################
//
//	FRes - Interface
//
//**********************

type FRes interface {
	getStatus()
	getData()
	getError()
	Parse()
	Decode()
}
