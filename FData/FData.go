package FData

import (
	. "bitbucket.org/laice_/cannaboard/Console"
	. "bitbucket.org/laice_/cannaboard/FBrand"
	. "bitbucket.org/laice_/cannaboard/FConfig"
	"bitbucket.org/laice_/cannaboard/FDB"
	"bitbucket.org/laice_/cannaboard/FDB/Tables"
	. "bitbucket.org/laice_/cannaboard/FLocation"
	"bitbucket.org/laice_/cannaboard/FRequest"
	"bitbucket.org/laice_/cannaboard/FResponse"
	"bitbucket.org/laice_/cannaboard/lib"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"strconv"
	"time"
)

//######################
//
//	FData
//
//**********************

type FData struct {
	autoUpdate   bool
	alive        bool
	Config       FConfig
	Brands       []FBrand    `json:"brands"`
	Locations    []FLocation `json:"locations"`
	serverConfig DBConfig
	CL           *CommandList
	DB           *FDB.DBPool
}

func NewFData(config FConfig, serverConf DBConfig, con *CommandList) (fData *FData) {
	var brands []FBrand
	var locations []FLocation

	return makeFData(brands, locations, config, serverConf, con)

}

func makeFData(Brands []FBrand, Locations []FLocation, config FConfig, server DBConfig, con *CommandList) (fData *FData) {
	var db *FDB.DBPool
	var err error

	db, err = FDB.New(server)
	if err != nil {
		panic(fmt.Sprintf("Failed to create new DB: %s", err.Error()))
	}

	var fdata = &FData{
		alive:        true,
		Config:       config,
		Brands:       Brands,
		Locations:    Locations,
		serverConfig: server,
		CL:           con,
		DB:           db,
	}
	if con != nil {
		fdata.InitCommands()
	}

	return fdata

}

func (fData *FData) InitCommands() {
	cl := fData.CL

	cl.NewCommand("locations", func(args []Arg) {
		fData.FetchLocations()
	})

	cl.NewCommand("inventory", func(args []Arg) {
		if len(fData.Locations) > 0 {
			for i, _ := range fData.Locations {
				fData.FetchInventory(i)
			}
		} else {
			cl.Log("Can't build Inventory Before Locations")
		}
	})

	cl.NewCommand("orders", func(args []Arg) {
		var pageSize = 0
		var createdBefore = ""
		var createdAfter = ""
		for _, arg := range args {
			switch arg.Key {
			case "-ps":
				s, err := strconv.Atoi(arg.Value)
				if err != nil {
					cl.Log(err.Error())
					return
				}

				pageSize = s
				break
			case "-ca":
				createdAfter = arg.Value
				break
			case "-cb":
				createdBefore = arg.Value
				break
			}
		}

		if len(fData.Locations) > 0 {
			for i, _ := range fData.Locations {
				fData.FetchOrders(i, createdAfter, createdBefore, pageSize)
			}
		} else {
			cl.Log("Can't build Orders before Locations")
		}

	})
}

func (fData *FData) FetchLocations() (table Tables.LocationsTable, err error) {

	fReq := FRequest.FRequest{}
	fReq.SetConfig(fData.Config)

	res := fReq.Request("/v0/clientsLocations")
	var fRes FResponse.FLocationResponse
	fRes.Parse(res.Body)
	fData.Locations = fRes.Data

	table, err = Tables.LocationTable(Tables.LocationsTableConfig{
		Pool: fData.DB,
	})

	if err != nil {
		err = errors.New(err.Error())
		return
	}

	for _, loc := range fData.Locations {
		fmt.Printf("Fetching Location %s\n", loc.LocationName)
		table.InsertLocation(Tables.LocationRow{
			LocationID:       loc.LocationID,
			LocationName:     loc.LocationName,
			ImportID:         loc.ImportID,
			Website:          loc.Website,
			HoursOfOperation: loc.HoursOfOperation,
			ClientId:         loc.ClientId,
			ClientName:       loc.ClientName,
			LocationLogoURL:  loc.LocationLogoURL,
			TimeZone:         loc.TimeZone,
			PhoneNumber:      loc.PhoneNumber,
			Email:            loc.Email,
		})
	}

	defer res.Body.Close()

	return
}

func (fData *FData) FetchInventory(locIndex int) {

	fReq := FRequest.FRequest{
		Config: fData.Config,
	}

	i := locIndex

	loc := fData.Locations[i]

	//fmt.Println(loc.ImportID)

	res := fReq.Request("/v0/locations/" + strconv.Itoa(loc.LocationID) + "/inventory")

	defer res.Body.Close()

	var invRes FResponse.FInventoryResponse

	locInv := invRes.Parse(res.Body)

	fData.Locations[i].Inventory = append(fData.Locations[i].Inventory, locInv.Data...)
	//fmt.Println("Inventory Data for " + strconv.Itoa(loc.LocationID) + ":")
	//fmt.Println(loc.Inventory)
	location := fData.Locations[i]

	table, err := Tables.InventoryTable(Tables.InventoriesTableConfig{
		Pool: fData.DB,
	})

	if err != nil {
		fmt.Printf("Inventory Table error: %s", err.Error())
		return
	}

	for _, product := range locInv.Data {
		_, err := table.InsertInventory(Tables.InventoryRow{
			ProductID:                               product.ProductID,
			ClientID:                                product.ClientID,
			ProductDescription:                      product.ProductDescription,
			ProductName:                             product.ProductName,
			PriceInMinorUnits:                       product.PriceInMinorUnits,
			SKU:                                     product.SKU,
			Nutrients:                               product.Nutrients,
			ProductPictureURL:                       product.ProductPictureURL,
			PurchaseCategory:                        product.PurchaseCategory,
			Category:                                product.Category,
			Type:                                    product.Type,
			Brand:                                   product.Brand,
			IsMixAndMatch:                           product.IsMixAndMatch,
			IsStackable:                             product.IsStackable,
			ProductUnitOfMeasure:                    product.ProductUnitOfMeasure,
			ProductUnitOfMeasureToGramsMultiplier:   product.ProductUnitOfMeasureToGramsMultiplier,
			ProductWeight:                           product.ProductWeight,
			Quantity:                                product.Quantity,
			InventoryUnitOfMeasure:                  product.InventoryUnitOfMeasure,
			InventoryUnitOfMeasureToGramsMultiplier: product.InventoryUnitOfMeasureToGramsMultiplier,
			LocationID:                              product.LocationID,
			LocationName:                            product.LocationName,
			CurrencyCode:                            product.CurrencyCode,
		})

		if err != nil {
			panic(fmt.Sprintf("Inventory Table Insert Error: %s", err.Error()))

		}

		cbdTable, err := Tables.CannabinoidTable(Tables.CannabinoidsTableConfig{
			Pool: fData.DB,
		})

		if err != nil {
			panic(fmt.Sprintf("cbdTable creation error: %s", err.Error()))
		}

		//pid := strconv.Itoa(product.ProductID) + "@" + strconv.Itoa(location.LocationID)

		for _, cannabinoid := range product.CannabinoidInformation {
			_, err := cbdTable.InsertCannabinoid(Tables.CannabinoidRow{
				ProductID:                      product.ProductID,
				Name:                           cannabinoid.Name,
				LowerRange:                     cannabinoid.LowerRange,
				UpperRange:                     cannabinoid.UpperRange,
				UnitOfMeasure:                  cannabinoid.UnitOfMeasure,
				UnitOfMeasureToGramsMultiplier: cannabinoid.UnitOfMeasureToGramsMultiplier,
			})

			if err != nil {
				panic(fmt.Sprintf("Cannabinoid Table Insert Error: %s", err.Error()))
			}

		}

		wtTable, err := Tables.WeightTierTable(Tables.WeightTiersTableConfig{
			Pool: fData.DB,
		})

		if err != nil {
			panic(fmt.Sprintf("wtTable creation error: %s", err.Error()))
		}

		for _, wt := range product.WeightTierInformation {
			_, err := wtTable.InsertWeightTier(Tables.WeightTierRow{
				ProductID:                product.ProductID,
				Name:                     wt.Name,
				GramAmount:               wt.GramAmount,
				PricePerUnitInMinorUnits: wt.PricePerUnitInMinorUnits,
			})

			if err != nil {
				panic(fmt.Sprintf("Weight Tier Table Insert Error: %s", err.Error()))
			}
		}

	}

	if fData.CL != nil {
		fData.CL.Log(fmt.Sprintf("%s inventory built\n", location.LocationName))
	} else {
		fmt.Printf("%s inventory built\n", location.LocationName)
	}

}

func SurroundingDates(createdAfter string, createdBefore string) (string, string) {

	tz, err := time.LoadLocation("America/Denver")
	if err != nil {
		panic(err)
	}

	after, err := time.ParseInLocation(time.RFC3339, createdAfter, tz)

	//fmt.Printf("Surrounding after 1: %s\n", after)

	var before time.Time

	if err != nil {
		panic(err)
	}

	if createdBefore != "" {
		before, err = time.Parse(time.RFC3339, createdBefore)
	} else {
		before = after.AddDate(0, 0, 2)
	}

	//after = after.AddDate(0, 0, -1)
	//fmt.Printf("Surrounding after 2: %s\n", after)
	//fmt.Printf("Surrounding before: %s\n", before)
	return after.Format(time.RFC3339), before.Format(time.RFC3339)

}

func fhTime(date string) string {
	//fmt.Printf("fhTime converting: %s\n", date)
	tz, err := time.LoadLocation("America/Denver")
	if err != nil {
		panic(err)
	}

	t, err := time.ParseInLocation(time.RFC3339, date, tz)

	if err != nil {
		panic(fmt.Sprintf("failed to format time: %s", date))
	}

	return t.Format("01-02-2006")
}

func (fData *FData) FetchOrdersLastYear(locIndex int) {
	//start := time.Now().AddDate(-1, 0, 0)
	now := time.Now()
	//end := start.AddDate(0, 1, 0)
	for createdAfter := time.Now().AddDate(-1, 0, 0); createdAfter.Before(now); createdAfter = createdAfter.AddDate(0, 1, 0) {
		createdBefore := createdAfter.AddDate(0, 1, 0)

		fData.FetchOrders(locIndex, createdAfter.Format(time.RFC3339), createdBefore.Format(time.RFC3339), 10000)
		fmt.Printf("Fetch from %s to %s\n", createdAfter.Format("01-02-2006 15:04:05"), createdBefore.Format("01-02-2006 15:04:05"))
		time.Sleep(10 * time.Second)

	}

}

func (fData *FData) FetchOrdersLastMonth(locIndex int) {
	lastMonth := time.Now().AddDate(0, -1, 0).Format(time.RFC3339)
	tomorrow := time.Now().AddDate(0, 0, 1).Format(time.RFC3339)
	fmt.Printf("Fetching last month: %s\n", lastMonth)
	fData.FetchOrders(locIndex, lastMonth, tomorrow, 10000)
}

func (fData *FData) FetchOrdersYesterday(locIndex int) {

	yesterday := time.Now().AddDate(0, 0, -1).Format(time.RFC3339)
	fmt.Printf("Fetching yesterday: %s\n", yesterday)

	fData.FetchOrders(locIndex, yesterday, "", 10000)
}

func (fData *FData) FetchOrdersToday(locIndex int) {
	today := time.Now().Format(time.RFC3339)

	fData.FetchOrders(locIndex, today, "", 10000)
}

func (fData *FData) FetchOrders(locIndex int, createdAfter string, createdBefore string, pageSize int) {
	fReq := FRequest.FRequest{}
	fReq.SetConfig(fData.Config)

	i := locIndex
	location := fData.Locations[i]
	importID := location.ImportID

	createdAfter, createdBefore = SurroundingDates(createdAfter, createdBefore)

	res := fReq.RequestOrders(importID, fhTime(createdAfter), fhTime(createdBefore), pageSize)
	var ordRes FResponse.FOrderResponse
	fRes := ordRes.Parse(res.Body)

	fData.Locations[i].Orders = fRes.Orders

	orders := fData.Locations[i].Orders

	ordTable, err := Tables.OrderTable(Tables.OrdersTableConfig{
		Pool: fData.DB,
	})

	if err != nil {
		panic(fmt.Sprintf("Failed to create order table: %s", err.Error()))
	}

	for _, order := range orders {
		oid := order.ID

		ordTable.InsertOrder(Tables.OrderRow{
			ID:            oid,
			ClientID:      order.ClientID,
			CreatedAt:     order.CreatedAt,
			CustomerID:    order.CustomerID,
			CurrentPoints: order.CurrentPoints,
			CustomerType:  order.CustomerType,
			CustomerName:  order.CustomerName,
			LocationID:    order.LocationID,
			LocationName:  order.LocationName,
			Voided:        order.Voided,
			CompletedOn:   order.CompletedOn,
			OrderStatus:   order.OrderStatus,
			OrderType:     order.OrderType,
		})

		cartItemTable, err := Tables.CartItemTable(Tables.CartItemsTableConfig{
			Pool: fData.DB,
		})

		if err != nil {
			panic(fmt.Sprintf("Error creating Cart Item Table: %s\n", err.Error()))
		}

		for _, item := range order.ItemsInCart {
			cartItemTable.InsertCartItem(Tables.CartItemRow{
				OrderID:      oid,
				ImportId:     order.LocationID,
				ID:           item.ID,
				Category:     item.Category,
				SKU:          item.SKU,
				Title1:       item.Title1,
				Title2:       item.Title2,
				Strain:       item.Strain,
				Quantity:     item.Quantity,
				UnitPrice:    item.UnitPrice,
				TotalPrice:   item.TotalPrice,
				UnitOfWeight: item.UnitOfWeight,
			})
		}

		//fmt.Printf("Outter Getting Order total for Order: %s\n", oid)

		totalsTable, err := Tables.OrderTotalTable(Tables.OrderTotalsTableConfig{
			Pool: fData.DB,
		})

		if err != nil {
			panic(fmt.Sprintf("Error creating totals table: %s\n", err.Error()))
		}

		//noinspection ALL

		//fmt.Println(order.Totals)
		//
		//for _, tot := range order.Totals {
		//	_, err = totalsTable.InsertOrderTotal(Tables.OrderTotalRow{
		//		OrderID:        oid,
		//		ImportId:       order.LocationID,
		//		FinalTotal:     tot.FinalTotal,
		//		SubTotal:       tot.SubTotal,
		//		TotalDiscounts: tot.TotalDiscounts,
		//		TotalFees:      tot.TotalFees,
		//		TotalTaxes:     tot.TotalTaxes,
		//	})
		//
		//	if err != nil {
		//		panic(fmt.Sprintf("Error creating totals row: %s\n", err.Error()))
		//	}
		//}
		_, err = totalsTable.InsertOrderTotal(Tables.OrderTotalRow{
			OrderID:        oid,
			ImportId:       order.LocationID,
			FinalTotal:     order.Totals.FinalTotal,
			SubTotal:       order.Totals.SubTotal,
			TotalDiscounts: order.Totals.TotalDiscounts,
			TotalFees:      order.Totals.TotalFees,
			TotalTaxes:     order.Totals.TotalTaxes,
		})

		if err != nil {
			panic(fmt.Sprintf("Error creating totals row: %s\n", err.Error()))
		}

	}

	fmt.Printf("%s orders built\n", fData.Locations[i].LocationName)

}

func (data *FData) CustomFetch(locIndex int, createdAfter string, createdBefore string) {
	ca, err := time.Parse("01-02-2006", createdAfter)
	cb, err := time.Parse("01-02-2006", createdBefore)
	lib.C(err)

	data.FetchOrders(locIndex, ca.Format(time.RFC3339), cb.Format(time.RFC3339), 10000)
}

func (data *FData) HasOrders(locImportId string, date string) (is bool) {

	ordersTable, err := Tables.OrderTable(Tables.OrdersTableConfig{
		Pool: data.DB,
	})

	if err != nil {
		panic(fmt.Sprintf("orders table creation failed: %s", err.Error()))
	}

	if date == "" {
		date = time.Now().Format(time.RFC3339)
	}

	fmt.Printf("Checking for orders for location %s on %s\n", locImportId, date)
	rows, err := ordersTable.GetOrdersByLocationIdAndDate(locImportId, date, 1)

	if len(rows) > 0 {
		is = true
	}

	return
}

//lastMonth := time.Now().AddDate(0, -1, 0).Format("01-02-2006")
//yesterday := time.Now().AddDate(0, 0, -1).Format("01-02-2006")
//dayAfterTomorrow := time.Now().AddDate(0, 0, 2).Format("01-02-2006")
//
//fmt.Printf("Last Month: %s\nYesterday: %s\n Tomorrow: %s\n", lastMonth, yesterday, dayAfterTomorrow)
//
//
//fData.FetchOrdersYesterday(loc.ImportID, func(orders []FOrders.FOrder) {
//	fData.Locations[i].Orders = orders
//
//	if i >= len(fData.Locations)-1 {
//		callback(*fData)
//	}
//})

func (fData *FData) String() string {
	data, err := json.Marshal(fData)

	if err != nil {
		panic(err)
	}

	return fmt.Sprintf("%+v\n", data)
}

func (fData *FData) Close() {
	fData.DB.Close()
}
