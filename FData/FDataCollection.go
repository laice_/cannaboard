package FData

import (
	"bitbucket.org/laice_/cannaboard/Console"
	"bitbucket.org/laice_/cannaboard/FConfig"
	"fmt"
)

type FDataCollection struct {
	Data        []FData           `json:"fdata"`
	Configs     []FConfig.FConfig `json:"configs"`
	SeverConfig FConfig.DBConfig  `json:"serverConfig"`
	CL          *Console.CommandList
}

func (fdc *FDataCollection) Add(fd FData) {
	fdc.Data = append(fdc.Data, fd)
}

func (fdc *FDataCollection) Remove(fd FData) {
	for i, d := range fdc.Data {
		if d.Config.ClientID == fd.Config.ClientID {
			fdc.Data = append(fdc.Data[:i], fdc.Data[i+1:]...)
		}
	}
}

func (fdc *FDataCollection) InitCommands() {
	cl := *fdc.CL
	cl.Log("FDC Init Commands")
	cl.NewCommand("cfg", func(args []Console.Arg) {
		console := false

		for _, arg := range args {
			if arg.Key == "-c" {

				console = true
			}

		}
		fdc.LoadAllConfigs(console)

	})
}

func NewCollection(configs []FConfig.FConfig, serverConfig FConfig.DBConfig, con bool) (fdc *FDataCollection) {

	if con {
		fmt.Printf("New Collection with Console\n")
		fdc = &FDataCollection{
			Configs:     configs,
			SeverConfig: serverConfig,
			CL:          Console.InitConsole(),
		}
		fdc.InitCommands()

		return fdc
	} else {
		fdc = &FDataCollection{
			Configs:     configs,
			SeverConfig: serverConfig,
			CL:          nil,
		}

		fdc.LoadAllConfigs(false)
		return fdc
	}

}

func (fdc *FDataCollection) LoadAllConfigs(console bool) {
	for i, _ := range fdc.Configs {
		fdc.Data = append(fdc.Data, *fdc.DataFromConfig(i, console))
	}
}

func (fdc *FDataCollection) DataFromConfig(confIndex int, console bool) *FData {

	if console {

		return NewFData(fdc.Configs[confIndex], fdc.SeverConfig, fdc.CL)
	} else {
		return NewFData(fdc.Configs[confIndex], fdc.SeverConfig, nil)
	}

}

func (fdc *FDataCollection) String() string {
	out := ""
	for _, dat := range fdc.Data {
		out = out + dat.String() + "\n"
	}
	return out
}
