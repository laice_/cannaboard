package FOrders

type FOrder struct {
	ID            string      `json:"_id"`
	ClientID      string      `json:"clientId"`
	CreatedAt     string      `json:"createdAt"`
	CustomerID    string      `json:"customerId"`
	CurrentPoints int         `json:"currentPoints"`
	CustomerType  string      `json:"customerType"`
	CustomerName  string      `json:"name"`
	LocationID    string      `json:"locationId"`
	LocationName  string      `json:"locationName"`
	ItemsInCart   []FCartItem `json:"itemsInCart"`
	Voided        bool        `json:"voided"`
	Totals        FOrderTotal `json:"totals"`
	CompletedOn   string      `json:"completedOn"`
	OrderStatus   string      `json:"orderStatus"`
	OrderType     string      `json:"orderType"`
}

type FCartItem struct {
	Category     string  `json:"category"`
	SKU          string  `json:"sku"`
	Title1       string  `json:"title1"`
	Title2       string  `json:"title2"`
	Strain       string  `json:"strainName"`
	Quantity     float32 `json:"quantity"`
	UnitPrice    float32 `json:"unitPrice"`
	TotalPrice   float32 `json:"totalPrice"`
	UnitOfWeight string  `json:"unitOfWeight"`
	ID           string  `json:"id"`
}

type FOrderTotal struct {
	FinalTotal     float32 `json:"finalTotal"`
	SubTotal       float32 `json:"subTotal"`
	TotalDiscounts float32 `json:"totalDiscounts"`
	TotalFees      float32 `json:"totalFees"`
	TotalTaxes     float32 `json:"totalTaxes"`
}

//func AssembleFromTable(order Tables.OrderRow, dbConfig FConfig.DBConfig) (fullOrder FOrder) {
//	fullOrder = FOrder{
//		ID:            order.ID,
//		ClientID:      order.ClientID,
//		CreatedAt:     order.CreatedAt,
//		CustomerID:    order.CustomerID,
//		CurrentPoints: order.CurrentPoints,
//		CustomerType:  order.CustomerType,
//		CustomerName:  order.CustomerName,
//		LocationID:    order.LocationID,
//		LocationName:  order.LocationName,
//		Voided:        order.Voided,
//		CompletedOn:   order.CompletedOn,
//		OrderStatus:   order.OrderStatus,
//		OrderType:     order.OrderType,
//	}
//
//	fullOrder.getItemsInCart(dbConfig)
//	fullOrder.getTotals(dbConfig)
//
//	return
//
//}

//func (order *FOrder) getItemsInCart(dbConfig FConfig.DBConfig) {
//
//	db, err := FDB.New(dbConfig)
//	if err != nil {
//		fmt.Printf("Cart Item DB Error:\n%s\n", err.Error())
//	}
//	defer db.Close()
//
//	cartTable, err := Tables.CartItemTable(Tables.CartItemsTableConfig{
//		Pool: db,
//	})
//
//	if err != nil {
//		fmt.Printf("Cart Item Table Error:\n%s\n", err.Error())
//	}
//
//	oid := order.ID
//
//	//ioid, err := strconv.Atoi(oid)
//	if err != nil {
//		fmt.Printf("Cart Item Error converting OID to int:\n%s\n", err.Error())
//	}
//
//	ci, err := cartTable.GetCartItemsByOrderId(oid)
//
//	if err != nil {
//		fmt.Printf("Cart Item Table Error:\n%s\n", err.Error())
//	}
//
//	order.ItemsInCart = ci
//
//}
//
//func (order *FOrder) getTotals(dbConfig FConfig.DBConfig) {
//	db, err := FDB.New(dbConfig)
//	if err != nil {
//		fmt.Printf("OrderTotal DB Error:\n%s\n", err.Error())
//	}
//	defer db.Close()
//
//	totalTable, err := Tables.OrderTotalTable(Tables.OrderTotalsTableConfig{
//		Pool: db,
//	})
//
//	if err != nil {
//		fmt.Printf("OrderTotal Table Error:\n%s\n", err.Error())
//	}
//
//	oid := order.ID
//
//	//ioid, err := strconv.Atoi(oid)
//	//if err != nil {
//	//	fmt.Printf("OrderTotal Error converting OID to int:\n%s\n", err.Error())
//	//}
//
//	tt, err := totalTable.GetOrderTotalsByOrderId(oid)
//
//	if err != nil {
//		fmt.Printf("OrderTotal Table Error:\n%s\n", err.Error())
//	}
//
//	order.Totals = tt
//}
