package FDB

import (
	"bitbucket.org/laice_/cannaboard/FConfig"
	"database/sql"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/pkg/errors"

	_ "github.com/lib/pq"
)

type DBPool struct {
	DB  *sql.DB
	cfg FConfig.DBConfig
}

func New(config FConfig.DBConfig) (pool *DBPool, err error) {

	if config.DBName == "" || config.DBUser == "" || config.DBHost == "" ||
		config.DBPass == "" || config.DBPort == "" || config.SSLMode == "" {
		err = errors.Errorf("All fields must be set (%s)", spew.Sdump(config))
		return
	}

	var db *sql.DB

	var p DBPool

	pool = &p

	pool.setCFG(config)

	connStr := fmt.Sprintf("user=%s password=%s dbname=%s host=%s sslmode=%s",
		config.DBUser, config.DBPass, config.DBName, config.DBHost, config.SSLMode)

	db, err = sql.Open("postgres", connStr)

	if err != nil {
		err = errors.Wrapf(err, "Couldn't open connection to postgres db (%s)",
			spew.Sdump(config))
		return
	}

	if err = db.Ping(); err != nil {
		err = errors.Wrapf(err, "Couldn't ping postgres db (%s)", spew.Sdump(config))
		return
	}

	pool.setDB(db)

	return

}

func (pool *DBPool) setCFG(cfg FConfig.DBConfig) {
	pool.cfg = cfg
}

func (pool *DBPool) setDB(db *sql.DB) {
	pool.DB = db
}

func (pool *DBPool) Close() (err error) {
	if pool.DB == nil {
		return
	}

	if err = pool.DB.Close(); err != nil {
		err = errors.Wrapf(err,
			"Error closing db connection",
			spew.Sdump(pool.cfg))
	}

	return
}
