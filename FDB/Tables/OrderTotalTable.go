package Tables

import (
	"bitbucket.org/laice_/cannaboard/FDB"
	"github.com/davecgh/go-spew/spew"
	"github.com/pkg/errors"
)

type OrderTotalsTable struct {
	Pool *FDB.DBPool
}

type OrderTotalsTableConfig struct {
	Pool *FDB.DBPool
}

type OrderTotalRow struct {
	OrderID        string  `json:"orderId"`
	ImportId       string  `json:"importId"`
	FinalTotal     float32 `json:"finalTotal"`
	SubTotal       float32 `json:"subTotal"`
	TotalDiscounts float32 `json:"totalDiscounts"`
	TotalFees      float32 `json:"totalFees"`
	TotalTaxes     float32 `json:"totalTaxes"`
}

func OrderTotalTable(cfg OrderTotalsTableConfig) (table OrderTotalsTable, err error) {
	if cfg.Pool == nil {
		err = errors.New("Can't create table without Pool instance")
		return
	}

	var t OrderTotalsTable
	t.setPool(cfg.Pool)

	table = t

	if err = table.createTable(); err != nil {
		err = errors.Wrapf(err, "Couldn't create table during init")
		return
	}

	return

}

func (table *OrderTotalsTable) setPool(pool *FDB.DBPool) {
	table.Pool = pool
}

func (table *OrderTotalsTable) createTable() (err error) {
	const qry = `
	CREATE TABLE IF NOT EXISTS orderTotals (		
		orderId 		text,
		importId		text,
		primary key(orderId, importId),
		finalTotal 		decimal(12,2),
		subTotal 		decimal(12,2),
		totalDiscounts 	decimal(12,2),
		totalFees 		decimal(12,2),
		totalTaxes 		decimal(12,2)
)
`
	_, err = table.Pool.DB.Exec(qry)

	if err != nil {
		err = errors.Wrapf(err, "OrderTotal table creation query failed (%s)", qry)
		return
	}

	return

}

func (table *OrderTotalsTable) InsertOrderTotal(row OrderTotalRow) (newRow OrderTotalRow, err error) {

	//fmt.Printf("Inserting Order Total for Order %s\n", row.OrderID)

	if row.OrderID == "" {
		err = errors.Errorf("Can't create inventory without order id (%s)", spew.Sdump(row))
		return
	}

	//fmt.Printf("Inserting product %s\n", row.ProductName)

	//const qry = `INSERT INTO orderTotals (
	//	productId,
	//	clientId,
	//	productDescription,
	//	productName, priceInMinorUnits, sku, nutrients, productPictureURL, purchaseCategory, category, typ, brand, isMixAndMatch, isStackable, productUnitOfMeasure, productUnitOfMeasureToGramsMultiplier, productWeight, quantity, inventoryUnitOfMeasure, inventoryUnitOfMeasureToGramsMultiplier, locationId, locationName, currencyCode ) VALUES ( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11
	//				$12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23 )  ON CONFLICT (productId) DO UPDATE SET ( clientId, productDescription, productName, priceInMinorUnits, sku, nutrients, productPictureURL, purchaseCategory, category, typ, brand, isMixAndMatch, isStackable, productUnitOfMeasure, productUnitOfMeasureToGramsMultiplier, productWeight, quantity, inventoryUnitOfMeasure, inventoryUnitOfMeasureToGramsMultiplier, locationId, locationName, currencyCode ) = ( EXCLUDED.clientId, EXCLUDED.productDescription, EXCLUDED.productName, EXCLUDED.priceInMinorUnits, EXCLUDED.sku, EXCLUDED.nutrients, EXCLUDED.productPictureURL, EXCLUDED.purchaseCategory, EXCLUDED.category, EXCLUDED.typ, EXCLUDED.brand, EXCLUDED.isMixAndMatch, EXCLUDED.isStackable, EXCLUDED.productUnitOfMeasure, EXCLUDED.productUnitOfMeasureToGramsMultiplier, EXCLUDED.productWeight, EXCLUDED.quantity, EXCLUDED.inventoryUnitOfMeasure, EXCLUDED.inventoryUnitOfMeasureToGramsMultiplier, EXCLUDED.locationId, EXCLUDED.locationName, EXCLUDED.currencyCode ) RETURNING productName;`
	const qry = `INSERT INTO orderTotals (
	orderId 	,
                         importId,
	finalTotal 	,
	subTotal 	,
	totalDiscounts ,
	totalFees 	,
	totalTaxes 	
) VALUES (
$1, $2, $3, $4, $5, $6, $7
)  ON CONFLICT (orderId, importId) DO UPDATE SET (
	orderId 	,  
                                        importId,
	finalTotal 	,            
	subTotal 	,            
	totalDiscounts ,         
	totalFees 	,            
	totalTaxes 	            
) = ( 
	EXCLUDED.orderId 	,   
     EXCLUDED.importId,
	EXCLUDED.finalTotal 	,            
	EXCLUDED.subTotal 	,            
	EXCLUDED.totalDiscounts ,         
	EXCLUDED.totalFees 	,            
	EXCLUDED.totalTaxes 	            
) RETURNING 
	orderId 	,           
  importId,
	finalTotal 	,            
	subTotal 	,            
	totalDiscounts ,         
	totalFees 	,            
	totalTaxes 	            
;`
	//fmt.Printf("Query: %s\n", qry)

	err = table.Pool.DB.QueryRow(qry,
		row.OrderID,
		row.ImportId,
		row.FinalTotal,
		row.SubTotal,
		row.TotalDiscounts,
		row.TotalFees,
		row.TotalTaxes,
	).Scan(
		&newRow.OrderID,
		&newRow.ImportId,
		&newRow.FinalTotal,
		&newRow.SubTotal,
		&newRow.TotalDiscounts,
		&newRow.TotalFees,
		&newRow.TotalTaxes,
	)

	if err != nil {
		err = errors.Wrapf(err, "Couldn't insert row into DB (%s)", spew.Sdump(row))
		return
	} else {
		//fmt.Printf("OrderTotal %s inserted\n", row.ProductName)
	}

	return
}

func (table *OrderTotalsTable) GetOrderTotalsByOrderId(orderId string) (rows []OrderTotalRow, err error) {
	if orderId == "" {
		err = errors.Errorf("Can't get order row with empty orderId")
		return
	}

	const qry = `SELECT  
	orderId 	,  
       importId,
	finalTotal 	,            
	subTotal 	,            
	totalDiscounts ,         
	totalFees 	,            
	totalTaxes 	        
	FROM orderTotals WHERE orderId = $1`

	iterator, err := table.Pool.DB.Query(qry, orderId)

	if err != nil {
		err = errors.Wrapf(err, "Failed to get OrderTotal (orderId = %s)", orderId)
		return
	}

	defer iterator.Close()

	for iterator.Next() {
		var row = OrderTotalRow{}

		err = iterator.Scan(
			&row.OrderID,
			&row.ImportId,
			&row.FinalTotal,
			&row.SubTotal,
			&row.TotalDiscounts,
			&row.TotalFees,
			&row.TotalTaxes,
		)

		if err != nil {
			err = errors.Wrapf(err, "OrderTotal row scanning failed for order: %s", orderId)
			return
		}

		rows = append(rows, row)
	}
	return
}
