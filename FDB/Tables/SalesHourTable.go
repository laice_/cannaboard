package Tables

import (
	"bitbucket.org/laice_/cannaboard/FDB"
	"database/sql"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"strconv"
	"time"
)

type SalesHourTable struct {
	Pool *FDB.DBPool
}

type SalesHourTableConfig struct {
	Pool *FDB.DBPool
}

type SalesHourRow struct {
	CreatedAt     string          `json:"createdAt"`
	SalesTotal    decimal.Decimal `json:"salesTotal"`
	ItemsSold     float64         `json:"itemsSold"`
	CustomerCount int             `json:"customerCount"`
	ImportId      string          `json:"importId"`
	SalesAvg      decimal.Decimal `json:"salesAvg"`
}

func SaleHourTable(cfg SalesHourTableConfig) (table SalesHourTable, err error) {
	if cfg.Pool == nil {
		err = errors.New("Can't create table without Pool instance")
		return
	}

	var t SalesHourTable
	t.setPool(cfg.Pool)

	table = t

	if err = table.createTable(); err != nil {
		err = errors.Wrapf(err, "Couldn't create table during init")
		return
	}

	return

}

func (table *SalesHourTable) setPool(pool *FDB.DBPool) {
	table.Pool = pool
}

func (table *SalesHourTable) createTable() (err error) {
	const qry = `
	CREATE TABLE IF NOT EXISTS salesHour (
		createdAt timestamp with time zone,
		importId		text,
		primary key(createdAt, importId),
		salesTotal		decimal(12,2),
		itemsSold		int,
		customerCount	int
	)
`
	_, err = table.Pool.DB.Exec(qry)

	if err != nil {
		err = errors.Wrapf(err, "Order stats table creation query failed (%s)", qry)
		return
	}

	return

}

func (table *SalesHourTable) InsertSaleHour(row SalesHourRow) (newRow SalesHourRow, err error) {
	if row.CreatedAt == "" {
		err = errors.Errorf("Can't create inventory without product id (%s)", spew.Sdump(row))
		return
	}

	const qry = `INSERT INTO salesHour (
createdAt,
salesTotal,
itemsSold,
customerCount,
importId
) VALUES (
$1, $2, $3, $4, $5
)  ON CONFLICT (createdAt, importId) DO UPDATE SET (
createdAt,
salesTotal,
itemsSold,
customerCount,
importId
) = ( 
EXCLUDED.createdAt,
EXCLUDED.salesTotal,
EXCLUDED.itemsSold,
EXCLUDED.customerCount, 
EXCLUDED.importId	
) RETURNING createdAt, salesTotal, itemsSold, customerCount, importId;`

	err = table.Pool.DB.QueryRow(qry,
		row.CreatedAt,
		row.SalesTotal,
		row.ItemsSold,
		row.CustomerCount,
		row.ImportId,
	).Scan(
		&newRow.CreatedAt,
		&newRow.SalesTotal,
		&newRow.ItemsSold,
		&newRow.CustomerCount,
		&newRow.ImportId,
	)

	if err != nil {
		err = errors.Wrapf(err, "Couldn't insert row into DB (%s)", spew.Sdump(row))
		return
	} else {
		//fmt.Printf("Order %s inserted\n", row.ProductName)
	}

	return
}

func (table *SalesHourTable) GetSalesHoursByDay(day string, importId string, limit int) (rows []SalesHourRow, err error) {

	if day == "" {
		err = errors.Errorf("Need day to continue, day: %s", day)
		return
	}

	qry := fmt.Sprintf(`SELECT  
	createdAt,
	sum(salesTotal),
	sum(itemsSold),
	sum(customerCount),
	importId	

	FROM salesHour 
	WHERE date_trunc('day',createdAt) = '%s' AND importId = '%s'
	group by createdAt, importId
	order by createdAt
	`, day, importId)

	if importId == "" || importId == "all" {
		qry = fmt.Sprintf(`SELECT  
	createdAt,
	sum(salesTotal),
	sum(itemsSold),
	sum(customerCount)
	FROM salesHour 
	WHERE date_trunc('day',createdAt) = '%s'
 	group by createdAt
	order by createdAt
`, day)
	}

	if limit != 0 {
		qry = qry + " LIMIT " + strconv.Itoa(limit)
	}
	//a := ca.Format("2006-02-01")
	//b := cb.Format("2006-02-01")
	//fmt.Printf("Location: %s\nQuery:%s\n", importId, qry)

	var iterator *sql.Rows

	iterator, err = table.Pool.DB.Query(qry)

	if err != nil {
		err = errors.Errorf("Error selecting by location id and date:\n%s\n", err.Error())
		return
	}

	for iterator.Next() {
		var row = SalesHourRow{}
		if importId != "all" {
			err = iterator.Scan(
				&row.CreatedAt,
				&row.SalesTotal,
				&row.ItemsSold,
				&row.CustomerCount,
				&row.ImportId,
			)
		} else {
			err = iterator.Scan(
				&row.CreatedAt,
				&row.SalesTotal,
				&row.ItemsSold,
				&row.CustomerCount,
			)
		}

		if err != nil {
			err = errors.Wrapf(err, "Sales hour row scanning failed for day %s", day)
			return
		}

		rows = append(rows, row)

	}

	return
}

func (table *SalesHourTable) GetSalesHoursByLocationIdBetweenDates(locationId string, createdAfter string, createdBefore string, limit int) (rows []SalesHourRow, err error) {

	tz, err := time.LoadLocation("America/Denver")

	if err != nil {
		err = errors.Errorf("Error creating timezone")
		return nil, err
	}

	if locationId == "" || createdAfter == "" {
		err = errors.Errorf("Need LocationId and start date to continue, locId: %d, date: %s", locationId, createdAfter)
		return nil, err
	}

	ca, err := time.ParseInLocation(time.RFC3339, createdAfter, tz)

	if err != nil {
		err = errors.Errorf("Failed to parse date as RFC3339: %s", createdAfter)
		return nil, err
	}

	var cb time.Time
	if createdBefore == "" {
		cb = time.Now().In(tz)
	} else {
		cb, err = time.ParseInLocation(time.RFC3339, createdBefore, tz)
		if err != nil {
			err = errors.Errorf("Failed to parse date as RFC3339: %s", createdAfter)
			return nil, err
		}
		cb = cb.AddDate(0, 0, 2)

	}

	//_, err = table.Pool.DB.Exec(`SET TIME ZONE 'America/Denver';`)

	if err != nil {
		err = errors.Errorf("Error setting DB time zone:\n%s\n", err.Error())
		return nil, err
	}

	qry := fmt.Sprintf(`SELECT  
	createdAt,
	salesTotal,
	itemsSold,
	customerCount,
	importId	

	FROM salesHour 	  

	
	WHERE importId = '%s' 
	AND createdAt between '%s' and '%s'
	group by importId, createdAt
	order by createdAt
	`, locationId, ca.Format("01-02-2006"), cb.Format("01-02-2006"))

	if limit != 0 {
		qry = qry + " LIMIT " + strconv.Itoa(limit)
	}
	//a := ca.Format("2006-02-01")
	//b := cb.Format("2006-02-01")
	//fmt.Printf("Date: %s\nLocation: %d\nQuery:%s\n", d, locationId, qry)

	iterator, err := table.Pool.DB.Query(qry)

	if err != nil {
		err = errors.Errorf("Error selecting by location id and date:\n%s\n", err.Error())
		return nil, err
	}

	defer iterator.Close()

	for iterator.Next() {
		var row = SalesHourRow{}

		err = iterator.Scan(
			&row.CreatedAt,
			&row.SalesTotal,
			&row.ItemsSold,
			&row.CustomerCount,
			&row.ImportId,
		)

		if err != nil {
			err = errors.Wrapf(err, "Order row scanning failed for location: %s and dates: %s - %s", locationId, ca, cb)
			return nil, err
		}

		//t, err2 := time.Parse(time.RFC3339, row.CreatedAt)
		//
		//if err2 != nil {
		//	err2 = errors.Wrapf(err, "Parsing createdAt failed for location: %s and dates: %s - %s", locationId, ca, cb)
		//	return nil, err
		//}

		//fmt.Printf("After: %s\nBefore: %s\nTime: %s\n",
		//	ca.Format(time.RFC3339), cb.Format(time.RFC3339), t.Format(time.RFC3339))
		// Add(-12*time.Hour)
		//if t.After(ca.Add(6*time.Hour)) && t.Before(cb) {
		rows = append(rows, row)
		//}

	}

	return
}
