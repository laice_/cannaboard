package Tables

import (
	"bitbucket.org/laice_/cannaboard/FDB"
	"bitbucket.org/laice_/cannaboard/lib"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"strconv"
	"time"
)

type SalesDayTable struct {
	Pool *FDB.DBPool
}

type SalesDayTableConfig struct {
	Pool *FDB.DBPool
}

type SalesDayRow struct {
	Hours         []SalesHourRow  `json:"hours"`
	CreatedAt     string          `json:"createdAt"`
	SalesTotal    decimal.Decimal `json:"salesTotal"`
	ItemsSold     int             `json:"itemsSold"`
	CustomerCount int             `json:"customerCount"`
	ImportId      string          `json:"importId"`
}

type SalesWeek struct {
	Day           string
	SalesTotal    decimal.Decimal `json:"salesTotal"`
	ItemsSold     int             `json:"itemsSold"`
	CustomerCount int             `json:"customerCount"`
	ImportId      string          `json:"importId"`
}

type SalesDay struct {
	Day           string          `json:"day"`
	SalesTotal    decimal.Decimal `json:"salesTotal"`
	ItemsSold     int             `json:"itemsSold"`
	CustomerCount int             `json:"customerCount"`
	ImportId      string          `json:"importId"`
}

func SaleDayTable(cfg SalesDayTableConfig) (table SalesDayTable, err error) {
	if cfg.Pool == nil {
		err = errors.New("Can't create table without Pool instance")
		return
	}

	var t SalesDayTable
	t.setPool(cfg.Pool)

	table = t

	if err = table.createTable(); err != nil {
		err = errors.Wrapf(err, "Couldn't create table during init")
		return
	}

	return

}

func (table *SalesDayTable) setPool(pool *FDB.DBPool) {
	table.Pool = pool
}

func (table *SalesDayTable) createTable() (err error) {
	const qry = `
	CREATE TABLE IF NOT EXISTS salesDay (		
		createdAt	timestamp with time zone,
		salesTotal	decimal(12,2),
		itemsSold	int,
		customerCount int,
		importId text,
		primary key(createdAt, importId)
	)
`
	_, err = table.Pool.DB.Exec(qry)

	if err != nil {
		err = errors.Wrapf(err, "Sales day table creation query failed (%s)", qry)
		return
	}

	return

}

func (table *SalesDayTable) InsertSalesDay(row SalesDayRow) (newRow SalesDayRow, err error) {
	if row.CreatedAt == "" {
		err = errors.Errorf("Can't create inventory without product day (%s)", spew.Sdump(row))
		return
	}

	const qry = `INSERT INTO salesDay (
createdAt,
salesTotal,
itemsSold,
customerCount,
importId
) VALUES (
$1, $2, $3, $4, $5
)  ON CONFLICT (createdAt, importId) DO UPDATE SET (
createdAt,
salesTotal,
itemsSold,
customerCount,
importId	
) = ( 
EXCLUDED.createdAt,	
EXCLUDED.salesTotal,	
EXCLUDED.itemsSold,	
EXCLUDED.customerCount,
EXCLUDED.importId
) RETURNING createdAt, salesTotal, itemsSold, customerCount, importId   ;`

	err = table.Pool.DB.QueryRow(qry,
		row.CreatedAt,
		row.SalesTotal,
		row.ItemsSold,
		row.CustomerCount,
		row.ImportId,
	).Scan(
		&newRow.CreatedAt,
		&newRow.SalesTotal,
		&newRow.ItemsSold,
		&newRow.CustomerCount,
		&newRow.ImportId,
	)

	if err != nil {
		err = errors.Wrapf(err, "Couldn't insert row into DB (%s)", spew.Sdump(row))
		return
	} else {
		//fmt.Printf("Order %s inserted\n", row.ProductName)
	}

	hourTable, err := SaleHourTable(SalesHourTableConfig{
		Pool: table.Pool,
	})

	if err != nil {
		err = errors.Wrapf(err, "Couldn't get hours table")
		return
	}

	for _, hour := range row.Hours {
		_, err = hourTable.InsertSaleHour(hour)
		if err != nil {
			err = errors.Wrapf(err, "Failed to insert hour %s", spew.Sdump(hour))
			return
		}
	}

	return
}

func (table *SalesDayTable) GetSalesDay(day string, importId string) (rows []SalesDayRow, err error) {

	if day == "" {
		err = errors.Errorf("Need day to continue, %s", day)
		return
	}
	qry := `SELECT  
	createdAt,
salesTotal,
itemsSold,
customerCount,
importId		  

	FROM salesDay 
	WHERE createdAt = $1 AND importId = $2
	`
	if importId == "" {
		qry = `SELECT  
	createdAt,
salesTotal,
itemsSold,
customerCount,
importId		  

	FROM salesDay 
	WHERE createdAt = $1 
	`
	}

	//a := ca.Format("2006-02-01")
	//b := cb.Format("2006-02-01")
	//fmt.Printf("Date: %s\nLocation: %d\nQuery:%s\n", d, locationId, qry)

	iterator, err := table.Pool.DB.Query(qry, day, importId)

	if err != nil {
		err = errors.Errorf("Error selecting by day:\n%s\n", err.Error())
		return nil, err
	}

	for iterator.Next() {
		var row = SalesDayRow{}

		err = iterator.Scan(
			&row.CreatedAt,
			&row.SalesTotal,
			&row.ItemsSold,
			&row.CustomerCount,
			&row.ImportId,
		)

		if err != nil {
			err = errors.Wrapf(err, "Sales day scanning failed for day: %s ", day)
			return nil, err
		}

		hourTable, err := SaleHourTable(SalesHourTableConfig{
			Pool: table.Pool,
		})

		if err != nil {
			err = errors.Wrapf(err, "Couldn't get hours table")
			return nil, err
		}

		row.Hours, err = hourTable.GetSalesHoursByDay(row.CreatedAt, importId, 0)

		if err != nil {
			err = errors.Wrapf(err, "Couldn't get hours")
			return nil, err
		}

		rows = append(rows, row)

	}

	return
}

func (table *SalesDayTable) GetOrdersByLocationIdBetweenDates(locationId string, createdAfter string, createdBefore string, limit int) (rows []SalesDayRow, err error) {

	tz, err := time.LoadLocation("America/Denver")

	if err != nil {
		err = errors.Errorf("Error creating timezone")
		return nil, err
	}

	if locationId == "" || createdAfter == "" {
		err = errors.Errorf("Need LocationId and start date to continue, locId: %d, date: %s", locationId, createdAfter)
		return nil, err
	}

	ca, err := time.ParseInLocation(time.RFC3339, createdAfter, tz)

	if err != nil {
		err = errors.Errorf("Failed to parse date as RFC3339: %s", createdAfter)
		return nil, err
	}

	var cb time.Time
	if createdBefore == "" {
		cb = time.Now().In(tz)
	} else {
		cb, err = time.ParseInLocation(time.RFC3339, createdBefore, tz)
		if err != nil {
			err = errors.Errorf("Failed to parse date as RFC3339: %s", createdAfter)
			return nil, err
		}
		cb = cb.AddDate(0, 0, 2)

	}

	//_, err = table.Pool.DB.Exec(`SET TIME ZONE 'America/Denver';`)

	if err != nil {
		err = errors.Errorf("Error setting DB time zone:\n%s\n", err.Error())
		return nil, err
	}
	var qry string
	if locationId != "all" {
		qry = fmt.Sprintf(`SELECT  
	createdAt,
salesTotal,
itemsSold,
customerCount,
importId
	FROM  salesDay 
	WHERE importId = '%s' 
	AND createdAt >= '%s' and createdAt <='%s'
	group by importId, createdAt
	order by createdAt;
	`, locationId, ca.Format("01-02-2006"), cb.Format("01-02-2006"))
	} else {
		qry = fmt.Sprintf(`SELECT  
	createdAt,
sum(salesTotal) as totalsales,
sum(itemsSold) as totalsold,
sum(customerCount) as totalcustomers
	FROM salesDay
WHERE createdAt >= '%s' and createdAt <='%s'
	group by createdAt
	order by createdAt;
	
	
	`, ca.Format("01-02-2006"), cb.Format("01-02-2006"))
	}
	//

	if limit != 0 {
		qry = qry + " LIMIT " + strconv.Itoa(limit)
	}
	//a := ca.Format("2006-02-01")
	//b := cb.Format("2006-02-01")
	fmt.Printf("Location: %s\nQuery:%s\n----\n", locationId, qry)

	iterator, err := table.Pool.DB.Query(qry)

	if err != nil {
		err = errors.Errorf("Error selecting by location id and date:\n%s\n", err.Error())
		return nil, err
	}

	//if !iterator.Next() {
	//	err = errors.Errorf("Iterator empty")
	//	return nil, err
	//}

	for iterator.Next() {
		var row = SalesDayRow{}

		if locationId != "all" {
			err = iterator.Scan(
				&row.CreatedAt,
				&row.SalesTotal,
				&row.ItemsSold,
				&row.CustomerCount,
				&row.ImportId,
			)
		} else {
			err = iterator.Scan(
				&row.CreatedAt,
				&row.SalesTotal,
				&row.ItemsSold,
				&row.CustomerCount,
			)
		}

		if err != nil {
			err = errors.Wrapf(err, "Order row scanning failed for location: %s and dates: %s - %s", locationId, ca, cb)
			return nil, err
		}

		//t, err2 := time.Parse(time.RFC3339, row.CreatedAt)

		//if err2 != nil {
		//	err2 = errors.Wrapf(err, "Parsing createdAt failed for location: %s and dates: %s - %s", locationId, ca, cb)
		//	return nil, err
		//}

		//fmt.Printf("After: %s\nBefore: %s\nTime: %s\n",
		//	ca.Format(time.RFC3339), cb.Format(time.RFC3339), t.Format(time.RFC3339))

		//if t.After(ca.Add(6*time.Hour)) && t.Before(cb.Add(-12*time.Hour)) {

		hourTable, err := SaleHourTable(SalesHourTableConfig{
			Pool: table.Pool,
		})

		if err != nil {
			err = errors.Wrapf(err, "Parsing createdAt failed for location: %s and dates: %s - %s\n", locationId, ca, cb)
			return nil, err
		}
		//fmt.Println(row.CreatedAt)
		t, err := time.Parse(time.RFC3339, row.CreatedAt)
		lib.C(err)
		//fmt.Println(t)
		tt := t.Format("01-02-2006")
		//fmt.Println(tt)
		row.Hours, err = hourTable.GetSalesHoursByDay(tt, locationId, 0)

		if err != nil {
			err = errors.Wrapf(err, "Fetching hours failed for location: %s and dates: %s - %s\n", locationId, ca, cb)
			return nil, err
		}

		rows = append(rows, row)
		//}

	}

	return
}

func (table *SalesDayTable) GetOrdersByLocationIdBetweenDatesDaily(locationId string, createdAfter string, createdBefore string, limit int) (rows []SalesDay, err error) {

	tz, err := time.LoadLocation("America/Denver")

	if err != nil {
		err = errors.Errorf("Error creating timezone")
		return nil, err
	}

	if locationId == "" || createdAfter == "" {
		err = errors.Errorf("Need LocationId and start date to continue, locId: %d, date: %s", locationId, createdAfter)
		return nil, err
	}

	ca, err := time.ParseInLocation(time.RFC3339, createdAfter, tz)

	if err != nil {
		err = errors.Errorf("Failed to parse date as RFC3339: %s", createdAfter)
		return nil, err
	}

	var cb time.Time
	if createdBefore == "" {
		cb = time.Now().In(tz)
	} else {
		cb, err = time.ParseInLocation(time.RFC3339, createdBefore, tz)
		if err != nil {
			err = errors.Errorf("Failed to parse date as RFC3339: %s", createdAfter)
			return nil, err
		}
		cb = cb.AddDate(0, 0, 2)

	}

	//_, err = table.Pool.DB.Exec(`SET TIME ZONE 'America/Denver';`)

	if err != nil {
		err = errors.Errorf("Error setting DB time zone:\n%s\n", err.Error())
		return nil, err
	}

	var qry string
	if locationId != "all" {
		qry = fmt.Sprintf(`
SELECT s.day, SUM(s.totalsales) as totalsales, SUM(s.itemssold) as itemssold, SUM(s.totalcustomers) as totalcustomers, s.importid FROM
(SELECT
       	date_trunc('day', createdAt::date) as day,        
SUM(salesTotal) as totalsales,
SUM(itemsSold) as itemssold,
SUM(customerCount) as totalcustomers,
importId
        FROM  salesDay as s
        WHERE importId = '%s'
		AND s.createdAt >= '%s' and s.createdAt <= '%s'
		
        GROUP BY s.createdAt, s.importid, date_trunc('day', createdAt::date)
        
) as s
group by s.day, s.importid
order by s.day
;
	`, locationId, ca.Format("01-02-2006"), cb.Format("01-02-2006"))
	} else {
		qry = fmt.Sprintf(`
SELECT s.day, SUM(s.totalsales) as totalsales, SUM(s.itemssold) as itemssold, SUM(s.totalcustomers) as totalcustomers FROM
(SELECT
       	date_trunc('day', createdAt::date) as day,        
SUM(salesTotal) as totalsales,
SUM(itemsSold) as itemssold,
SUM(customerCount) as totalcustomers

        FROM  salesDay as s
        
		WHERE s.createdAt >= '%s' and s.createdAt <= '%s'
		
        GROUP BY s.createdAt,  date_trunc('day', createdAt::date)
        
) as s
group by s.day
order by s.day
;
	`, ca.Format("01-02-2006"), cb.Format("01-02-2006"))
	}

	if limit != 0 {
		qry = qry + " LIMIT " + strconv.Itoa(limit)
	}
	//a := ca.Format("2006-02-01")
	//b := cb.Format("2006-02-01")
	//fmt.Printf("Location: %s\nQuery:%s\n----\n", locationId, qry)

	iterator, err := table.Pool.DB.Query(qry)

	if err != nil {
		err = errors.Errorf("Error selecting by location id and date:\n%s\n", err.Error())
		return nil, err
	}

	defer iterator.Close()

	for iterator.Next() {
		var row = SalesDay{}

		if locationId != "all" {
			err = iterator.Scan(
				&row.Day,
				&row.SalesTotal,
				&row.ItemsSold,
				&row.CustomerCount,
				&row.ImportId,
			)
		} else {
			err = iterator.Scan(
				&row.Day,
				&row.SalesTotal,
				&row.ItemsSold,
				&row.CustomerCount,
			)
		}

		if err != nil {
			err = errors.Wrapf(err, "Order row scanning failed for location: %s and dates: %s - %s", locationId, ca, cb)
			return nil, err
		}

		rows = append(rows, row)

	}

	return
}

func (table *SalesDayTable) GetOrdersByLocationIdBetweenDatesWeekly(locationId string, createdAfter string, createdBefore string, limit int) (rows []SalesDay, err error) {

	tz, err := time.LoadLocation("America/Denver")

	if err != nil {
		err = errors.Errorf("Error creating timezone")
		return nil, err
	}

	if locationId == "" || createdAfter == "" {
		err = errors.Errorf("Need LocationId and start date to continue, locId: %d, date: %s", locationId, createdAfter)
		return nil, err
	}

	ca, err := time.ParseInLocation(time.RFC3339, createdAfter, tz)

	if err != nil {
		err = errors.Errorf("Failed to parse date as RFC3339: %s", createdAfter)
		return nil, err
	}

	var cb time.Time
	if createdBefore == "" {
		cb = time.Now().In(tz)
	} else {
		cb, err = time.ParseInLocation(time.RFC3339, createdBefore, tz)
		if err != nil {
			err = errors.Errorf("Failed to parse date as RFC3339: %s", createdAfter)
			return nil, err
		}
		cb = cb.AddDate(0, 0, 2)

	}

	//_, err = table.Pool.DB.Exec(`SET TIME ZONE 'America/Denver';`)

	if err != nil {
		err = errors.Errorf("Error setting DB time zone:\n%s\n", err.Error())
		return nil, err
	}
	//cay, caw := ca.ISOWeek()
	//cby, cbw := cb.ISOWeek()
	//
	//if caw > cbw {
	//	cbw = 52
	//	// cbw = caw
	//	caw = 1
	//}

	//	qry := fmt.Sprintf(`
	//SELECT s.year, s.week, SUM(s.totalsales) as totalsales, SUM(s.itemssold) as itemssold, SUM(s.totalcustomers) as totalcustomers, s.importid FROM
	//(SELECT
	//       	date_part('week', createdAt::date) as week,
	//        date_part('year', createdAt::date) as year,
	//SUM(salesTotal) as totalsales,
	//SUM(itemsSold) as itemssold,
	//SUM(customerCount) as totalcustomers,
	//importId
	//        FROM  salesDay as s
	//        WHERE importId = '%s'
	//		AND NOT (
	//			date_part('day', createdAt) = '31' AND
	//			date_part('month', createdAt) = '12' AND
	//			date_part('hour', createdAt) = '00' AND
	//			date_part('minute', createdAt) = '00' AND
	//			date_part('second', createdAt) = '00'
	//		)
	//		AND s.createdAt between '%s' and '%s'
	//        AND date_part('week', createdAt::date) between '%d' and '%d'
	//        AND date_part('year', createdAt::date) between '%d' and '%d'
	//
	//        GROUP BY s.createdAt, s.importid, date_part('week', createdAt::date), date_part('year', createdAt::date)
	//
	//) as s
	//group by s.week, s.year, s.importid
	//order by s.year
	//;
	//
	//
	//	`, locationId, ca.Format("01-02-2006"), cb.Format("01-02-2006"), caw, cbw, cay, cby)

	var qry string

	if locationId != "all" {
		qry = fmt.Sprintf(`
select day, sum(totalsales), sum(itemssold), sum(totalcustomers) from
(select date_trunc('week',createdat::date) as day, sum(salestotal) as totalsales, sum(itemssold) as itemssold, sum(customercount) as totalcustomers
from salesday
where importid='%s'
and createdat >= '%s' and createdat <= '%s'
group by date_trunc('week',createdat::date), createdat
order by createdat desc) s
group by day
order by day

`, locationId, ca.Format("01-02-2006"), cb.Format("01-02-2006"))
	} else {
		qry = fmt.Sprintf(`
select day, sum(totalsales), sum(itemssold), sum(totalcustomers) from
(select date_trunc('week',createdat::date) as day, sum(salestotal) as totalsales, sum(itemssold) as itemssold, sum(customercount) as totalcustomers
from salesday
where createdat >= '%s' and createdat <= '%s'
group by date_trunc('week',createdat::date), createdat
order by createdat desc) s
group by day
order by day

`, ca.Format("01-02-2006"), cb.Format("01-02-2006"))
	}

	if limit != 0 {
		qry = qry + " LIMIT " + strconv.Itoa(limit)
	}
	//a := ca.Format("2006-02-01")
	//b := cb.Format("2006-02-01")
	//fmt.Printf("Location: %s\nQuery:%s\n----\n", locationId, qry)

	iterator, err := table.Pool.DB.Query(qry)

	if err != nil {
		err = errors.Errorf("Error selecting by location id and date:\n%s\n", err.Error())
		return nil, err
	}

	for iterator.Next() {
		var row = SalesDay{}

		err = iterator.Scan(
			&row.Day,
			&row.SalesTotal,
			&row.ItemsSold,
			&row.CustomerCount,
		)

		if err != nil {
			err = errors.Wrapf(err, "Order row scanning failed for location: %s and dates: %s - %s", locationId, ca, cb)
			return nil, err
		}

		rows = append(rows, row)

	}

	return
}

func (table *SalesDayTable) GetOrdersByLocationIdBetweenDatesMonthly(locationId string, createdAfter string, createdBefore string, limit int) (rows []SalesDay, err error) {

	tz, err := time.LoadLocation("America/Denver")

	if err != nil {
		err = errors.Errorf("Error creating timezone")
		return nil, err
	}

	if locationId == "" || createdAfter == "" {
		err = errors.Errorf("Need LocationId and start date to continue, locId: %d, date: %s", locationId, createdAfter)
		return nil, err
	}

	ca, err := time.ParseInLocation(time.RFC3339, createdAfter, tz)

	if err != nil {
		err = errors.Errorf("Failed to parse date as RFC3339: %s", createdAfter)
		return nil, err
	}

	var cb time.Time
	if createdBefore == "" {
		cb = time.Now().In(tz)
	} else {
		cb, err = time.ParseInLocation(time.RFC3339, createdBefore, tz)
		if err != nil {
			err = errors.Errorf("Failed to parse date as RFC3339: %s", createdAfter)
			return nil, err
		}
		cb = cb.AddDate(0, 0, 2)

	}

	//_, err = table.Pool.DB.Exec(`SET TIME ZONE 'America/Denver';`)

	if err != nil {
		err = errors.Errorf("Error setting DB time zone:\n%s\n", err.Error())
		return nil, err
	}
	var qry string
	if locationId != "all" {
		qry = fmt.Sprintf(`
SELECT s.month, SUM(s.totalsales) as totalsales, SUM(s.itemssold) as itemssold, SUM(s.totalcustomers) as totalcustomers, s.importid FROM
(SELECT
       	date_trunc('month', createdAt::date) as month,
SUM(salesTotal) as totalsales,
SUM(itemsSold) as itemssold,
SUM(customerCount) as totalcustomers,
importId
        FROM  salesDay as s
        WHERE importId = '%s'
		AND s.createdAt between '%s' and '%s'		
        GROUP BY s.createdAt, s.importid, date_trunc('month', createdAt::date)
        
) as s
group by s.month, s.importid
order by s.month;

	`, locationId, ca.Format("01-02-2006"), cb.Format("01-02-2006"))
	} else {
		qry = fmt.Sprintf(`
SELECT s.month, SUM(s.totalsales) as totalsales, SUM(s.itemssold) as itemssold, SUM(s.totalcustomers) as totalcustomers FROM
(SELECT
       	date_trunc('month', createdAt::date) as month,
SUM(salesTotal) as totalsales,
SUM(itemsSold) as itemssold,
SUM(customerCount) as totalcustomers
        FROM  salesDay as s
        WHERE s.createdAt between '%s' and '%s'		
        GROUP BY s.createdAt,  date_trunc('month', createdAt::date)
        
) as s
group by s.month
order by s.month;

	`, ca.Format("01-02-2006"), cb.Format("01-02-2006"))
	}

	if limit != 0 {
		qry = qry + " LIMIT " + strconv.Itoa(limit)
	}
	//a := ca.Format("2006-02-01")
	//b := cb.Format("2006-02-01")
	//fmt.Printf("Location: %s\nQuery:%s\n----\n", locationId, qry)

	iterator, err := table.Pool.DB.Query(qry)

	if err != nil {
		err = errors.Errorf("Error selecting by location id and date:\n%s\n", err.Error())
		return nil, err
	}

	for iterator.Next() {
		var row = SalesDay{}
		if locationId != "all" {
			err = iterator.Scan(
				&row.Day,
				&row.SalesTotal,
				&row.ItemsSold,
				&row.CustomerCount,
				&row.ImportId,
			)
		} else {
			err = iterator.Scan(
				&row.Day,
				&row.SalesTotal,
				&row.ItemsSold,
				&row.CustomerCount,
			)
		}

		if err != nil {
			err = errors.Wrapf(err, "Order row scanning failed for location: %s and dates: %s - %s", locationId, ca, cb)
			return nil, err
		}

		rows = append(rows, row)

	}

	return
}

func (table *SalesDayTable) GetOrdersByLocationIdBetweenDatesYearly(locationId string, createdAfter string, createdBefore string, limit int) (rows []SalesDay, err error) {

	tz, err := time.LoadLocation("America/Denver")

	if err != nil {
		err = errors.Errorf("Error creating timezone")
		return nil, err
	}

	if locationId == "" || createdAfter == "" {
		err = errors.Errorf("Need LocationId and start date to continue, locId: %d, date: %s", locationId, createdAfter)
		return nil, err
	}

	ca, err := time.ParseInLocation(time.RFC3339, createdAfter, tz)

	if err != nil {
		err = errors.Errorf("Failed to parse date as RFC3339: %s", createdAfter)
		return nil, err
	}

	var cb time.Time
	if createdBefore == "" {
		cb = time.Now().In(tz)
	} else {
		cb, err = time.ParseInLocation(time.RFC3339, createdBefore, tz)
		if err != nil {
			err = errors.Errorf("Failed to parse date as RFC3339: %s", createdAfter)
			return nil, err
		}
		cb = cb.AddDate(0, 0, 2)

	}

	//_, err = table.Pool.DB.Exec(`SET TIME ZONE 'America/Denver';`)

	if err != nil {
		err = errors.Errorf("Error setting DB time zone:\n%s\n", err.Error())
		return nil, err
	}
	var qry string
	if locationId != "all" {
		qry = fmt.Sprintf(`
SELECT s.year, SUM(s.totalsales) as totalsales, SUM(s.itemssold) as itemssold, SUM(s.totalcustomers) as totalcustomers, s.importid FROM
(SELECT
       	date_part('year', createdAt::date) as year,
SUM(salesTotal) as totalsales,
SUM(itemsSold) as itemssold,
SUM(customerCount) as totalcustomers,
importId
        FROM  salesDay as s
        WHERE importId = '%s'
		AND s.createdAt between '%s' and '%s'		
        GROUP BY s.createdAt, s.importid, date_part('year', createdAt::date)
        
) as s
group by s.year, s.importid
order by s.year;

	`, locationId, ca.Format("01-02-2006"), cb.Format("01-02-2006"))
	} else {
		qry = fmt.Sprintf(`
SELECT s.year, SUM(s.totalsales) as totalsales, SUM(s.itemssold) as itemssold, SUM(s.totalcustomers) as totalcustomers FROM
(SELECT
       	date_part('year', createdAt::date) as year,
SUM(salesTotal) as totalsales,
SUM(itemsSold) as itemssold,
SUM(customerCount) as totalcustomers
        FROM  salesDay as s
        WHERE s.createdAt between '%s' and '%s'		
        GROUP BY s.createdAt, date_part('year', createdAt::date)
        
) as s
group by s.year
order by s.year;

	`, ca.Format("01-02-2006"), cb.Format("01-02-2006"))
	}

	if limit != 0 {
		qry = qry + " LIMIT " + strconv.Itoa(limit)
	}
	//a := ca.Format("2006-02-01")
	//b := cb.Format("2006-02-01")
	//fmt.Printf("Location: %s\nQuery:%s\n----\n", locationId, qry)

	iterator, err := table.Pool.DB.Query(qry)

	if err != nil {
		err = errors.Errorf("Error selecting by location id and date:\n%s\n", err.Error())
		return nil, err
	}

	for iterator.Next() {
		var row = SalesDay{}

		if locationId != "all" {
			err = iterator.Scan(
				&row.Day,
				&row.SalesTotal,
				&row.ItemsSold,
				&row.CustomerCount,
				&row.ImportId,
			)
		} else {
			err = iterator.Scan(
				&row.Day,
				&row.SalesTotal,
				&row.ItemsSold,
				&row.CustomerCount,
			)
		}

		if err != nil {
			err = errors.Wrapf(err, "Order row scanning failed for location: %s and dates: %s - %s", locationId, ca, cb)
			return nil, err
		}

		rows = append(rows, row)

	}

	return
}
