package Tables

import (
	"bitbucket.org/laice_/cannaboard/FDB"
	"bitbucket.org/laice_/cannaboard/lib"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
	"strconv"
	"time"
)

type CartItemsTable struct {
	Pool *FDB.DBPool
}

type CartItemsTableConfig struct {
	Pool *FDB.DBPool
}

type CartItemRow struct {
	OrderID      string  `json:"orderId"`
	ImportId     string  `json:"importId"`
	ID           string  `json:"id"`
	Category     string  `json:"category"`
	SKU          string  `json:"sku"`
	Title1       string  `json:"title1"`
	Title2       string  `json:"title2"`
	Strain       string  `json:"strainName"`
	Quantity     float32 `json:"quantity"`
	UnitPrice    float32 `json:"unitPrice"`
	TotalPrice   float32 `json:"totalPrice"`
	UnitOfWeight string  `json:"unitOfWeight"`
}

type CartStatistic struct {
	CreatedAt    string  `json:"createdAt"`
	ImportId     string  `json:"importId"`
	Name         string  `json:"name"`
	Strain       string  `json:"strain"`
	Unit         string  `json:"unit"`
	TotalSold    float32 `json:"totalSold"`
	TotalSales   float32 `json:"totalSales"`
	PricePerUnit float32 `json:"pricePerUnit"`
}

type CartStatTransport struct {
	Names      []string  `json:"names"`
	Strains    []string  `json:"strains"`
	Units      []string  `json:"units"`
	Sold       []float32 `json:"sold"`
	Sales      []string  `json:"sales"`
	TotalSold  float32   `json:"totalSold"`
	TotalSales string    `json:"totalSales"`

	CategoryNames      []string  `json:"categoryNames"`
	CategoryTotalSold  []float32 `json:"categoryTotalSold"`
	CategoryTotalSales []float32 `json:"categoryTotalSales"`
	CategoryDates      []string  `json:"categoryDates"`
}

type CategoryStatTransport struct {
	Stats []CategoryStat `json:"stats"`
}

type CategoryStat struct {
	Name       string  `json:"name"`
	TotalSold  float32 `json:"totalSold"`
	TotalSales float32 `json:"totalSales"`
	CreatedAt  string  `json:"createdAt"`
	SalesAvg   float32 `json:"salesAvg"`
}

type CatStat struct {
	Name       string    `json:"name"`
	TotalSold  []float32 `json:"totalSold"`
	TotalSales []float32 `json:"totalSales"`
	CreatedAt  []string  `json:"createdAt"`
}

type CatStats struct {
	Stats []CatStat `json:"stats"`
}

func CartItemTable(cfg CartItemsTableConfig) (table CartItemsTable, err error) {
	if cfg.Pool == nil {
		err = errors.New("Can't create table without Pool instance")
		return
	}

	var t CartItemsTable
	t.setPool(cfg.Pool)

	table = t

	if err = table.createTable(); err != nil {
		err = errors.Wrapf(err, "Couldn't create table during init")
		return
	}

	return

}

func (table *CartItemsTable) setPool(pool *FDB.DBPool) {
	table.Pool = pool
}

func (table *CartItemsTable) createTable() (err error) {
	const qry = `
	CREATE TABLE IF NOT EXISTS cartItems (		
		orderId 		text,
		importId		text,
		id 				text,
		primary key(orderId, id, importId),
		category 		text,
		sku 			text,
		title1 			text,
		title2 			text,
		strainName 		text,
		quantity 		real,
		unitPrice 		decimal(12,2),
		totalPrice		decimal(12,2),
		unitOfWeight 	text
)
`
	_, err = table.Pool.DB.Exec(qry)

	if err != nil {
		err = errors.Wrapf(err, "CartItem table creation query failed (%s)", qry)
		return
	}

	return

}

func (table *CartItemsTable) InsertCartItem(row CartItemRow) (newRow CartItemRow, err error) {
	if row.OrderID == "" {
		err = errors.Errorf("Can't create inventory without product id (%s)", spew.Sdump(row))
		return
	}

	//fmt.Printf("Inserting product %s\n", row.ProductName)

	//const qry = `INSERT INTO cartItems (
	//	productId,
	//	clientId,
	//	productDescription,
	//	productName, priceInMinorUnits, sku, nutrients, productPictureURL, purchaseCategory, category, typ, brand, isMixAndMatch, isStackable, productUnitOfMeasure, productUnitOfMeasureToGramsMultiplier, productWeight, quantity, inventoryUnitOfMeasure, inventoryUnitOfMeasureToGramsMultiplier, locationId, locationName, currencyCode ) VALUES ( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11
	//				$12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23 )  ON CONFLICT (productId) DO UPDATE SET ( clientId, productDescription, productName, priceInMinorUnits, sku, nutrients, productPictureURL, purchaseCategory, category, typ, brand, isMixAndMatch, isStackable, productUnitOfMeasure, productUnitOfMeasureToGramsMultiplier, productWeight, quantity, inventoryUnitOfMeasure, inventoryUnitOfMeasureToGramsMultiplier, locationId, locationName, currencyCode ) = ( EXCLUDED.clientId, EXCLUDED.productDescription, EXCLUDED.productName, EXCLUDED.priceInMinorUnits, EXCLUDED.sku, EXCLUDED.nutrients, EXCLUDED.productPictureURL, EXCLUDED.purchaseCategory, EXCLUDED.category, EXCLUDED.typ, EXCLUDED.brand, EXCLUDED.isMixAndMatch, EXCLUDED.isStackable, EXCLUDED.productUnitOfMeasure, EXCLUDED.productUnitOfMeasureToGramsMultiplier, EXCLUDED.productWeight, EXCLUDED.quantity, EXCLUDED.inventoryUnitOfMeasure, EXCLUDED.inventoryUnitOfMeasureToGramsMultiplier, EXCLUDED.locationId, EXCLUDED.locationName, EXCLUDED.currencyCode ) RETURNING productName;`
	const qry = `INSERT INTO cartItems (
orderId 	   ,
                       importId,
id 			   ,
"category" 	   ,
sku 		   ,
title1 		   ,
title2 		   ,
strainName 	   ,
quantity 	   ,
unitPrice 	   ,
totalPrice	   ,
unitOfWeight   
) VALUES (     
$1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12
)  ON CONFLICT (orderId, id, importId) DO UPDATE SET (
orderId 	   ,   
importId		,
id 			   ,   
"category" 	   ,   
sku 		   ,   
title1 		   ,   
title2 		   ,   
strainName 	   ,   
quantity 	   ,   
unitPrice 	   ,   
totalPrice	   ,   
unitOfWeight
) = ( 
EXCLUDED.orderId 	   ,   
     EXCLUDED.importId,
EXCLUDED.id 			   ,   
EXCLUDED.category 	   ,   
EXCLUDED.sku 		   ,   
EXCLUDED.title1 		   ,   
EXCLUDED.title2 		   ,   
EXCLUDED.strainName 	   ,   
EXCLUDED.quantity 	   ,   
EXCLUDED.unitPrice 	   ,   
EXCLUDED.totalPrice	   ,   
EXCLUDED.unitOfWeight  

) RETURNING 
orderId 	   , 
  importId,
id 			   ,   
"category" 	   ,   
sku 		   ,   
title1 		   ,   
title2 		   ,   
strainName 	   ,   
quantity 	   ,   
unitPrice 	   ,   
totalPrice	   ,   
unitOfWeight       
;`

	err = table.Pool.DB.QueryRow(qry,
		row.OrderID,
		row.ImportId,
		row.ID,
		row.Category,
		row.SKU,
		row.Title1,
		row.Title2,
		row.Strain,
		row.Quantity,
		row.UnitPrice,
		row.TotalPrice,
		row.UnitOfWeight,
	).Scan(
		&newRow.OrderID,
		&newRow.ImportId,
		&newRow.ID,
		&newRow.Category,
		&newRow.SKU,
		&newRow.Title1,
		&newRow.Title2,
		&newRow.Strain,
		&newRow.Quantity,
		&newRow.UnitPrice,
		&newRow.TotalPrice,
		&newRow.UnitOfWeight,
	)

	if err != nil {
		err = errors.Wrapf(err, "Couldn't insert row into DB (%s)", spew.Sdump(row))
		return
	} else {
		//fmt.Printf("CartItem %s inserted\n", row.ProductName)
	}

	return
}

func (table *CartItemsTable) GetCartItemsByOrderId(orderId string) (rows []CartItemRow, err error) {
	if orderId == "" {
		err = errors.Errorf("Can't get product row with empty productId")
		return
	}

	const qry = `SELECT  
	 orderId 	   ,   
       importId,
     id 			   ,   
     category 	   ,   
     sku 		   ,   
     title1 		   ,   
     title2 		   ,   
	 strainName 	   ,   
     quantity 	   ,   
     unitPrice 	   ,   
     totalPrice	   ,   
     unitOfWeight 
	FROM cartItems WHERE orderId = $1`

	iterator, err := table.Pool.DB.Query(qry, orderId)

	if err != nil {
		err = errors.Wrapf(err, "Failed to get CartItem (orderId = %s)", orderId)
		return
	}

	defer lib.C(iterator.Close())

	for iterator.Next() {
		var row = CartItemRow{}

		err = iterator.Scan(
			&row.OrderID,
			&row.ImportId,
			&row.ID,
			&row.Category,
			&row.SKU,
			&row.Title1,
			&row.Title2,
			&row.Strain,
			&row.Quantity,
			&row.UnitPrice,
			&row.TotalPrice,
			&row.UnitOfWeight,
		)

		if err != nil {
			err = errors.Wrapf(err, "CartItem row scanning failed for order: %s", orderId)
			return
		}

		rows = append(rows, row)
	}
	return
}

//func (table *CartItemsTable) GetCategoryStatistics(importId string, createdAfter string, createdBefore string, period string) (Stats []CatStat) {
//	var qry string
//
//	_, err := table.Pool.DB.Exec(`SET TIME ZONE 'America/Denver';`)
//
//	lib.C(err)
//
//	if importId != "all" {
//		qry = fmt.Sprintf(`
//SELECT s.category, sum(s.totalsold) as totalsold, sum(s.totalsales) as totalsales, created
//FROM(
//		SELECT category, sum(quantity) as totalsold, sum(totalprice) as totalsales, date_trunc('%s', o.createdAt) as created
//FROM cartitems as c
//INNER JOIN orders as o
//ON o._id = c.orderid
//WHERE o.createdat >= '%s' and o.createdat <= '%s'
//AND o.locationid = '%s'
//group by category, o.createdAt
//order by category ) as s
//group by category, created
//;
//
//`, period, createdAfter, createdBefore, importId)
//	} else {
//		qry = fmt.Sprintf(`
//SELECT s.category, sum(s.totalsold) as totalsold, sum(s.totalsales) as totalsales, created
//FROM
//(
//		SELECT category, sum(quantity) as totalsold, sum(totalprice) as totalsales, date_trunc('%s', o.createdAt) as created
//FROM cartitems as c
//INNER JOIN orders as o
//ON o._id = c.orderid
//WHERE o.createdat >= '%s' and o.createdat <= '%s'
//group by category, o.createdAt
//order by category) as s
//group by category, created
//;
//
//`, period, createdAfter, createdBefore)
//	}
//
//	fmt.Println(qry)
//
//	iterator, err := table.Pool.DB.Query(qry)
//	lib.C(err)
//	var st []CatStat
//
//	for iterator.Next() {
//
//		var stat CategoryStat
//
//		err = iterator.Scan(
//			&stat.Name,
//			&stat.TotalSold,
//			&stat.TotalSales,
//			&stat.CreatedAt,
//		)
//
//		fmt.Println(stat)
//
//		nameFound := false
//		for _, v := range st {
//			if v.Name == stat.Name {
//				ac := false
//				for j, k := range v.CreatedAt {
//					if k == v.CreatedAt[j] {
//						v.TotalSold[j] = v.TotalSold[j] + stat.TotalSold
//						v.TotalSales[j] = v.TotalSales[j] + stat.TotalSales
//						ac = true
//					}
//				}
//				if ac == false {
//					v.CreatedAt = append(v.CreatedAt, stat.CreatedAt)
//					v.TotalSales = append(v.TotalSales, stat.TotalSales)
//					v.TotalSold = append(v.TotalSold, stat.TotalSold)
//
//				}
//
//				nameFound = true
//			}
//		}
//
//		if nameFound == false {
//
//			var ts []float32
//			var tsa []float32
//			var ca []string
//			ts = append(ts, stat.TotalSold)
//			tsa = append(tsa, stat.TotalSales)
//			ca = append(ca, stat.CreatedAt)
//
//			st = append(st, CatStat{
//				Name:       stat.Name,
//				TotalSold:  ts,
//				TotalSales: tsa,
//				CreatedAt:  ca,
//			})
//		}
//
//	}
//
//	fmt.Println(st)
//
//	return st
//}

func (table *CartItemsTable) GetCategoryStatistics(importId string, createdAfter string, createdBefore string, period string) (Stats []CategoryStat) {
	var qry string

	_, err := table.Pool.DB.Exec(`SET TIME ZONE 'America/Denver';`)

	lib.C(err)

	if importId != "all" {
		qry = fmt.Sprintf(`
SELECT s.category, sum(s.totalsold) as totalsold, sum(s.totalsales) as totalsales, created, avg(avgsale) as avgsale
FROM(
		SELECT category, sum(quantity) as totalsold, sum(totalprice) as totalsales, 
			date_trunc('%s', o.createdAt) as created, avg(totalprice) as avgsale
FROM cartitems as c
INNER JOIN orders as o
ON o._id = c.orderid
WHERE o.createdat >= '%s' and o.createdat <= '%s'
AND o.locationid = '%s'
group by category, o.createdAt
order by category ) as s
group by category, created
;

`, period, createdAfter, createdBefore, importId)
	} else {
		qry = fmt.Sprintf(`
SELECT s.category, sum(s.totalsold) as totalsold, sum(s.totalsales) as totalsales, created, avg(avgsale) as avgsale
FROM
(
		SELECT category, sum(quantity) as totalsold, sum(totalprice) as totalsales, 
			date_trunc('%s', o.createdAt) as created, avg(totalprice) as avgsale
FROM cartitems as c
INNER JOIN orders as o
ON o._id = c.orderid
WHERE o.createdat >= '%s' and o.createdat <= '%s'
group by category, o.createdAt
order by category) as s
group by category, created
;

`, period, createdAfter, createdBefore)
	}

	//fmt.Println(qry)

	iterator, err := table.Pool.DB.Query(qry)
	lib.C(err)

	for iterator.Next() {

		var stat CategoryStat

		err = iterator.Scan(
			&stat.Name,
			&stat.TotalSold,
			&stat.TotalSales,
			&stat.CreatedAt,
			&stat.SalesAvg,
		)

		//fmt.Println(stat)

		Stats = append(Stats, stat)

	}

	//fmt.Println(Stats)

	return
}

//func (table *CartItemsTable) GetCategoryStatistics(importId string, createdAfter string, createdBefore string, period string) (Names []string, TotalSold []float32, TotalSales []float32, Dates []string) {
//	var qry string
//
//	_, err := table.Pool.DB.Exec(`SET TIME ZONE 'America/Denver';`)
//
//	lib.C(err)
//
//	if importId != "all" {
//		qry = fmt.Sprintf(`
//SELECT s.category, sum(s.totalsold) as totalsold, sum(s.totalsales) as totalsales, created
//FROM(
//		SELECT category, sum(quantity) as totalsold, sum(totalprice) as totalsales, date_trunc('%s', o.createdAt)
//FROM cartitems as c
//INNER JOIN orders as o
//ON o._id = c.orderid
//WHERE o.createdat >= '%s' and o.createdat <= '%s'
//AND o.locationid = '%s'
//group by category, o.createdAt
//orderby category ) as s
//group by category, created
//;
//
//`, period, createdAfter, createdBefore, importId)
//	} else {
//		qry = fmt.Sprintf(`
//SELECT s.category, sum(s.totalsold) as totalsold, sum(s.totalsales) as totalsales, created
//FROM
//(
//		SELECT category, sum(quantity) as totalsold, sum(totalprice) as totalsales, date_trunc('%s', o.createdAt) as created
//FROM cartitems as c
//INNER JOIN orders as o
//ON o._id = c.orderid
//WHERE o.createdat >= '%s' and o.createdat <= '%s'
//group by category, o.createdAt
//order by category) as s
//group by category, created
//;
//
//`, period, createdAfter, createdBefore)
//	}
//
//	fmt.Println(qry)
//
//	iterator, err := table.Pool.DB.Query(qry)
//	lib.C(err)
//
//	for iterator.Next() {
//
//		var stat CategoryStat
//
//		err = iterator.Scan(
//			&stat.Name,
//			&stat.TotalSold,
//			&stat.TotalSales,
//			&stat.CreatedAt,
//		)
//
//		fmt.Println(stat)
//
//		Names = append(Names, stat.Name)
//		TotalSold = append(TotalSold, stat.TotalSold)
//		TotalSales = append(TotalSales, stat.TotalSales)
//		Dates = append(Dates, stat.CreatedAt)
//
//	}
//
//	fmt.Println(Names, TotalSold, TotalSales, Dates)
//
//	return
//}

func (table *CartItemsTable) GetCartStatisticYearly(importId string, createdAfter string, createdBefore string) (rows []SalesHourRow, err error) {

	ca, err := time.Parse(time.RFC3339, createdAfter)
	cb, err := time.Parse(time.RFC3339, createdBefore)

	lib.C(err)

	_, err = table.Pool.DB.Exec(`SET TIME ZONE 'America/Denver';`)

	lib.C(err)

	var qry string
	if importId != "all" {
		qry = fmt.Sprintf(` 
		select sum(customercount), sum(totalquantity), sum(totalsales), created, importId, avg(avgsales) from (
		select count(o.createdAt) as customercount, sum(quantity) as totalquantity, sum(totalprice) as totalsales, date_trunc('year', o.createdAt) as created, o.locationid as importId, avg(totalprice) as avgsales
		from cartitems as c
		inner join orders as o
		on c.orderid = o._id
		where o.locationid = '%s' 
		and o.createdat >= '%s 05:00:00' and o.createdat <= '%s'		
		group by date_trunc('year', o.createdAt), o.locationid, o.createdAt
		order by o.createdAt ) as s
		group by created, importId
		order by created ;
		
		
`, importId, ca.Format("01-02-2006"), cb.Format("01-02-2006"))

	} else {
		qry = fmt.Sprintf(` 
	
		select sum(customercount), sum(totalquantity), sum(totalsales), created, avg(avgsales) from (
		select count(o.createdAt) as customercount, sum(quantity) as totalquantity, sum(totalprice) as totalsales, date_trunc('year', o.createdAt) as created, avg(totalprice) as avgsales
		from cartitems as c
		inner join orders as o
		on c.orderid = o._id

		where o.createdat >= '%s 05:00:00' and o.createdat <= '%s'		
		group by date_trunc('year', o.createdAt), o.locationid, o.createdAt
		order by o.createdAt ) as s
		group by created
		order by created ;
		
		
`, ca.Format("01-02-2006"), cb.Format("01-02-2006"))

	}

	fmt.Println(qry)

	iterator, err := table.Pool.DB.Query(qry)
	lib.C(err)
	//fmt.Println(iterator)

	defer iterator.Close()

	for iterator.Next() {
		var row = SalesHourRow{}
		//fmt.Println("hello")
		if importId != "all" {
			err = iterator.Scan(
				&row.CustomerCount,
				&row.ItemsSold,
				&row.SalesTotal,
				&row.CreatedAt,
				&row.ImportId,
				&row.SalesAvg,
			)
		} else {
			err = iterator.Scan(
				&row.CustomerCount,
				&row.ItemsSold,
				&row.SalesTotal,
				&row.CreatedAt,
				&row.SalesAvg,
			)
		}

		lib.C(err)

		rows = append(rows, row)

	}

	//fmt.Println(stat)

	return

}

func (table *CartItemsTable) GetCartStatisticMonthly(importId string, createdAfter string, createdBefore string) (rows []SalesHourRow, err error) {

	ca, err := time.Parse(time.RFC3339, createdAfter)
	cb, err := time.Parse(time.RFC3339, createdBefore)

	lib.C(err)

	_, err = table.Pool.DB.Exec(`SET TIME ZONE 'America/Denver';`)

	lib.C(err)

	var qry string
	if importId != "all" {
		qry = fmt.Sprintf(` 
		select sum(customercount), sum(totalquantity), sum(totalsales), created, importId, avg(avgsale) as avgsale from (
		select count(o.createdAt) as customercount, sum(quantity) as totalquantity, sum(totalprice) as totalsales, date_trunc('month', o.createdAt) as created, o.locationid as importId, avg(totalprice) as avgsale
		from cartitems as c
		inner join orders as o
		on c.orderid = o._id
		where o.locationid = '%s' 
		and o.createdat >= '%s 05:00:00' and o.createdat <= '%s'		
		group by date_trunc('month', o.createdAt), o.locationid, o.createdAt
		order by o.createdAt ) as s
		group by created, importId
		order by created ;
		
		
`, importId, ca.Format("01-02-2006"), cb.Format("01-02-2006"))

	} else {
		qry = fmt.Sprintf(` 
	
		select sum(customercount), sum(totalquantity), sum(totalsales), created, avg(avgsale) as avgsale from (
		select count(o.createdAt) as customercount, sum(quantity) as totalquantity, sum(totalprice) as totalsales, date_trunc('month', o.createdAt) as created, avg(totalprice) as avgsale
		from cartitems as c
		inner join orders as o
		on c.orderid = o._id

		where o.createdat >= '%s 05:00:00' and o.createdat <= '%s'		
		group by date_trunc('month', o.createdAt), o.locationid, o.createdAt
		order by o.createdAt ) as s
		group by created
		order by created ;
		
		
`, ca.Format("01-02-2006"), cb.Format("01-02-2006"))

	}

	fmt.Println(qry)

	iterator, err := table.Pool.DB.Query(qry)
	lib.C(err)
	//fmt.Println(iterator)

	defer iterator.Close()

	for iterator.Next() {
		var row = SalesHourRow{}
		//fmt.Println("hello")
		if importId != "all" {
			err = iterator.Scan(
				&row.CustomerCount,
				&row.ItemsSold,
				&row.SalesTotal,
				&row.CreatedAt,
				&row.ImportId,
				&row.SalesAvg,
			)
		} else {
			err = iterator.Scan(
				&row.CustomerCount,
				&row.ItemsSold,
				&row.SalesTotal,
				&row.CreatedAt,
				&row.SalesAvg,
			)
		}

		lib.C(err)

		rows = append(rows, row)

	}

	//fmt.Println(stat)

	return

}

func (table *CartItemsTable) GetCartStatisticWeekly(importId string, createdAfter string, createdBefore string) (rows []SalesHourRow, err error) {

	ca, err := time.Parse(time.RFC3339, createdAfter)
	cb, err := time.Parse(time.RFC3339, createdBefore)

	lib.C(err)

	_, err = table.Pool.DB.Exec(`SET TIME ZONE 'America/Denver';`)

	lib.C(err)

	var qry string
	if importId != "all" {
		qry = fmt.Sprintf(` 
		select sum(customercount), sum(totalquantity), sum(totalsales), created, importId, avg(avgsale) as avgsale from (
		select count(o.createdAt) as customercount, sum(quantity) as totalquantity, sum(totalprice) as totalsales, date_trunc('week', o.createdAt) as created, o.locationid as importId, avg(totalprice) as avgsale
		from cartitems as c
		inner join orders as o
		on c.orderid = o._id
		where o.locationid = '%s' 
		and o.createdat >= '%s 05:00:00' and o.createdat <= '%s'		
		group by date_trunc('week', o.createdAt), o.locationid, o.createdAt
		order by o.createdAt ) as s
		group by created, importId
		order by created ;
		
		
`, importId, ca.Format("01-02-2006"), cb.Format("01-02-2006"))

	} else {
		qry = fmt.Sprintf(` 
	
		select sum(customercount), sum(totalquantity), sum(totalsales), created, avg(avgsale) as avgsale from (
		select count(o.createdAt) as customercount, sum(quantity) as totalquantity, sum(totalprice) as totalsales, date_trunc('week', o.createdAt) as created, avg(totalprice) as avgsale
		from cartitems as c
		inner join orders as o
		on c.orderid = o._id

		where o.createdat >= '%s 05:00:00' and o.createdat <= '%s'		
		group by date_trunc('week', o.createdAt), o.locationid, o.createdAt
		order by o.createdAt ) as s
		group by created
		order by created ;
		
		
`, ca.Format("01-02-2006"), cb.Format("01-02-2006"))

	}

	fmt.Println(qry)

	iterator, err := table.Pool.DB.Query(qry)
	lib.C(err)
	//fmt.Println(iterator)

	defer iterator.Close()

	for iterator.Next() {
		var row = SalesHourRow{}
		//fmt.Println("hello")
		if importId != "all" {
			err = iterator.Scan(
				&row.CustomerCount,
				&row.ItemsSold,
				&row.SalesTotal,
				&row.CreatedAt,
				&row.ImportId,
				&row.SalesAvg,
			)
		} else {
			err = iterator.Scan(
				&row.CustomerCount,
				&row.ItemsSold,
				&row.SalesTotal,
				&row.CreatedAt,
				&row.SalesAvg,
			)
		}

		lib.C(err)

		rows = append(rows, row)

	}

	//fmt.Println(stat)

	return

}

func (table *CartItemsTable) GetCartStatisticDaily(importId string, createdAfter string, createdBefore string) (rows []SalesHourRow, err error) {

	ca, err := time.Parse(time.RFC3339, createdAfter)
	cb, err := time.Parse(time.RFC3339, createdBefore)

	lib.C(err)

	_, err = table.Pool.DB.Exec(`SET TIME ZONE 'America/Denver';`)

	if err != nil {
		err = errors.Errorf("Error setting DB time zone:\n%s\n", err.Error())
		return nil, err
	}

	var qry string
	if importId != "all" {
		qry = fmt.Sprintf(` 
		select sum(customercount), sum(totalquantity), sum(totalsales), created, importId, avg(avgsale) as avgsale from (
		select count(o.createdAt) as customercount, sum(quantity) as totalquantity, sum(totalprice) as totalsales, date_trunc('day', o.createdAt) as created, o.locationid as importId, avg(totalprice) as avgsale
		from cartitems as c
		inner join orders as o
		on c.orderid = o._id
		where o.locationid = '%s' 
		and o.createdat >= '%s 05:00:00' and o.createdat <= '%s'		
		group by date_trunc('day', o.createdAt), o.locationid, o.createdAt
		order by o.createdAt ) as s
		group by created, importId
		order by created ;
		
		
`, importId, ca.Format("01-02-2006"), cb.Format("01-02-2006"))

	} else {
		qry = fmt.Sprintf(` 
	
		select sum(customercount), sum(totalquantity), sum(totalsales), created, avg(avgsale) as avgsale from (
		select count(o.createdAt) as customercount, sum(quantity) as totalquantity, sum(totalprice) as totalsales, date_trunc('day', o.createdAt) as created, avg(totalprice) as avgsale
		from cartitems as c
		inner join orders as o
		on c.orderid = o._id

		where o.createdat >= '%s 05:00:00' and o.createdat <= '%s'		
		group by date_trunc('day', o.createdAt), o.locationid, o.createdAt
		order by o.createdAt ) as s
		group by created
		order by created ;
		
		
`, ca.Format("01-02-2006"), cb.Format("01-02-2006"))

	}

	fmt.Println(qry)

	iterator, err := table.Pool.DB.Query(qry)
	lib.C(err)
	//fmt.Println(iterator)

	defer iterator.Close()

	for iterator.Next() {
		var row = SalesHourRow{}
		//fmt.Println("hello")
		if importId != "all" {
			err = iterator.Scan(
				&row.CustomerCount,
				&row.ItemsSold,
				&row.SalesTotal,
				&row.CreatedAt,
				&row.ImportId,
				&row.SalesAvg,
			)
		} else {
			err = iterator.Scan(
				&row.CustomerCount,
				&row.ItemsSold,
				&row.SalesTotal,
				&row.CreatedAt,
				&row.SalesAvg,
			)
		}

		lib.C(err)

		rows = append(rows, row)

	}

	//fmt.Println(stat)

	return

}

func (table *CartItemsTable) GetCartStatisticHourly(importId string, createdAfter string, createdBefore string) (rows []SalesHourRow, err error) {

	ca, err := time.Parse(time.RFC3339, createdAfter)
	cb, err := time.Parse(time.RFC3339, createdBefore)

	lib.C(err)

	_, err = table.Pool.DB.Exec(`SET TIME ZONE 'America/Denver';`)

	lib.C(err)

	var qry string
	if importId != "all" {
		qry = fmt.Sprintf(` 
		select sum(customercount), sum(totalquantity), sum(totalsales), created, importId, avg(avgsale) as avgsale from (
		select count(o.createdAt) as customercount, sum(quantity) as totalquantity, sum(totalprice) as totalsales, date_trunc('hour', o.createdAt) as created, o.locationid as importId, avg(totalprice) as avgsale
		from cartitems as c
		inner join orders as o
		on c.orderid = o._id
		where o.locationid = '%s' 
		and o.createdat >= '%s 05:00:00' and o.createdat <= '%s'		
		group by date_trunc('hour', o.createdAt), o.locationid, o.createdAt
		order by o.createdAt ) as s
		group by created, importId
		order by created ;
		
		
`, importId, ca.Format("01-02-2006"), cb.Format("01-02-2006"))

	} else {
		qry = fmt.Sprintf(` 
	
		select sum(customercount), sum(totalquantity), sum(totalsales), created, avg(avgsale) as avgsale from (
		select count(o.createdAt) as customercount, sum(quantity) as totalquantity, sum(totalprice) as totalsales, date_trunc('hour', o.createdAt) as created, avg(totalprice) as avgsale
		from cartitems as c
		inner join orders as o
		on c.orderid = o._id

		where o.createdat >= '%s 05:00:00' and o.createdat <= '%s'		
		group by date_trunc('hour', o.createdAt), o.locationid, o.createdAt
		order by o.createdAt ) as s
		group by created
		order by created ;
		
		
`, ca.Format("01-02-2006"), cb.Format("01-02-2006"))

	}

	//fmt.Println(qry)

	iterator, err := table.Pool.DB.Query(qry)
	lib.C(err)
	//fmt.Println(iterator)

	defer iterator.Close()

	for iterator.Next() {
		var row = SalesHourRow{}
		//fmt.Println("hello")
		if importId != "all" {
			err = iterator.Scan(
				&row.CustomerCount,
				&row.ItemsSold,
				&row.SalesTotal,
				&row.CreatedAt,
				&row.ImportId,
				&row.SalesAvg,
			)
		} else {
			err = iterator.Scan(
				&row.CustomerCount,
				&row.ItemsSold,
				&row.SalesTotal,
				&row.CreatedAt,
				&row.SalesAvg,
			)
		}

		lib.C(err)

		rows = append(rows, row)

	}

	//stat.CategoryNames, stat.CategoryTotalSold, stat.CategoryTotalSales = table.GetCategoryStatistics(importId, ca.Format("01-02-2006"), cb.Format("01-02-2006"))

	//fmt.Println(stat)

	return

}

type DOWStat struct {
	CreatedAt          string          `json:"createdat"`
	TotalSales         decimal.Decimal `json:"totalSales"`
	AvgSales           decimal.Decimal `json:"avgSales"`
	MaxSale            decimal.Decimal `json:"maxSale"`
	MinSale            decimal.Decimal `json:"minSale"`
	UnitPerTransaction decimal.Decimal `json:"unitPerTransaction"`
	DayOfWeek          string          `json:"dayOfWeek"`
}

func (table *CartItemsTable) GetCartStatisticDOW(importId string, day string) (rows []DOWStat, err error) {

	_, err = table.Pool.DB.Exec(`SET TIME ZONE 'America/Denver';`)

	lib.C(err)

	var dow int
	switch day {
	case "All":
		fallthrough
	case "all":
		dow = -1
		break
	case "Sunday":
	case "sunday":
		dow = 0
		break
	case "Monday":
		fallthrough
	case "monday":
		dow = 1
		break
	case "Tuesday":
		fallthrough
	case "tuesday":
		dow = 2
		break
	case "Wednesday":
		fallthrough
	case "wednesday":
		dow = 3
		break
	case "Thursday":
		fallthrough
	case "thursday":
		dow = 4
		break
	case "Friday":
		fallthrough
	case "friday":
		dow = 5
		break
	case "Saturday":
		fallthrough
	case "saturday":
		dow = 6
		break
	default:
		dow = -1
		break
	}

	var qry string
	if importId != "all" {
		if dow == -1 {
			qry = fmt.Sprintf(` 
		select s.createdat, round(sum(s.totalsales),2) as total_sales,round(avg(s.avgsale), 2) as avg_sale, max(s.maxsale) as max_sale, min(s.minsale) as min_sale, round(avg(s.units_per_transaction)::numeric, 2) as avg_unit_per_transaction, extract(dow from s.createdAt) as dayOfWeek
from
(
select date_trunc('day',o.createdat) as createdat, sum(c.totalprice) as totalsales, avg(c.totalprice) as avgsale, max(c.totalprice) as maxsale, min(c.totalprice) as minsale, sum(c.quantity)/count(c.orderid) as units_per_transaction
from cartitems as c
inner join orders as o
on o._id = c.orderid

where o.locationid = '%s'
group by date_trunc('day', o.createdat)
order by createdat
) as s
group by s.createdat
;	
		
`, importId)

		} else {
			qry = fmt.Sprintf(` 
		select s.createdat, round(sum(s.totalsales),2) as total_sales,round(avg(s.avgsale), 2) as avg_sale, max(s.maxsale) as max_sale, min(s.minsale) as min_sale, round(avg(s.units_per_transaction)::numeric, 2) as avg_unit_per_transaction, extract(dow from s.createdAt) as dayOfWeek
from
(
select date_trunc('day',o.createdat) as createdat, sum(c.totalprice) as totalsales, avg(c.totalprice) as avgsale, max(c.totalprice) as maxsale, min(c.totalprice) as minsale, sum(c.quantity)/count(c.orderid) as units_per_transaction
from cartitems as c
inner join orders as o
on o._id = c.orderid
where extract(dow from date_trunc('day', o.createdat)) = '%d'
and o.locationid = '%s'
group by date_trunc('day', o.createdat)
order by createdat
) as s
group by s.createdat
;	
		
`, dow, importId)

		}

	} else {
		if dow == -1 {
			qry = fmt.Sprintf(` 
	
		select s.createdat, round(sum(s.totalsales),2) as total_sales,round(avg(s.avgsale), 2) as avg_sale, max(s.maxsale) as max_sale, min(s.minsale) as min_sale, round(avg(s.units_per_transaction)::numeric, 2) as avg_unit_per_transaction, extract(dow from s.createdAt) as dayOfWeek
from
(
select date_trunc('day',o.createdat) as createdat, sum(c.totalprice) as totalsales, avg(c.totalprice) as avgsale, max(c.totalprice) as maxsale, min(c.totalprice) as minsale, sum(c.quantity)/count(c.orderid) as units_per_transaction
from cartitems as c
inner join orders as o
on o._id = c.orderid
group by date_trunc('day', o.createdat)
order by createdat
) as s
group by s.createdat
;

		
		
`)
		} else {
			qry = fmt.Sprintf(` 
	
		select s.createdat, round(sum(s.totalsales),2) as total_sales,round(avg(s.avgsale), 2) as avg_sale, max(s.maxsale) as max_sale, min(s.minsale) as min_sale, round(avg(s.units_per_transaction)::numeric, 2) as avg_unit_per_transaction, extract(dow from s.createdAt) as dayOfWeek
from
(
select date_trunc('day',o.createdat) as createdat, sum(c.totalprice) as totalsales, avg(c.totalprice) as avgsale, max(c.totalprice) as maxsale, min(c.totalprice) as minsale, sum(c.quantity)/count(c.orderid) as units_per_transaction
from cartitems as c
inner join orders as o
on o._id = c.orderid
where extract(dow from date_trunc('day', o.createdat)) = '%d'
group by date_trunc('day', o.createdat)
order by createdat
) as s
group by s.createdat
;

		
		
`, dow)
		}

	}

	//fmt.Println(qry)

	iterator, err := table.Pool.DB.Query(qry)
	lib.C(err)
	//fmt.Println(iterator)

	defer func() {
		lib.C(iterator.Close())
	}()

	for iterator.Next() {
		var row = DOWStat{}
		//fmt.Println("hello")

		err = iterator.Scan(
			&row.CreatedAt,
			&row.TotalSales,
			&row.AvgSales,
			&row.MaxSale,
			&row.MinSale,
			&row.UnitPerTransaction,
			&row.DayOfWeek,
		)

		lib.C(err)

		switch row.DayOfWeek {
		case "0":
			row.DayOfWeek = "Sunday"
			break
		case "1":
			row.DayOfWeek = "Monday"
			break
		case "2":
			row.DayOfWeek = "Tuesday"
			break
		case "3":
			row.DayOfWeek = "Wednesday"
			break
		case "4":
			row.DayOfWeek = "Thursday"
			break
		case "5":
			row.DayOfWeek = "Friday"
			break
		case "6":
			row.DayOfWeek = "Saturday"
			break
		}
		//		row.DayOfWeek = day

		rows = append(rows, row)

	}

	//stat.CategoryNames, stat.CategoryTotalSold, stat.CategoryTotalSales = table.GetCategoryStatistics(importId, ca.Format("01-02-2006"), cb.Format("01-02-2006"))

	//fmt.Println(stat)

	return

}

func (table *CartItemsTable) GetCartCounts(importId string, createdAfter string, createdBefore string) (stat CartStatTransport, err error) {

	ca, err := time.Parse(time.RFC3339, createdAfter)
	cb, err := time.Parse(time.RFC3339, createdBefore)
	// --and (o.createdat::date >= date '%s' and o.createdat::date <= date '%s'))
	lib.C(err)
	//	qry := fmt.Sprintf(`
	//	select c.title1, c.strainname, sum(c.quantity) as quantity, sum(c.totalprice) as totalprice, c.unitofweight
	//from cartitems as c
	//inner join orders as o
	//on c.orderid = o._id
	//group by c.title1, c.strainname, c.unitofweight
	//`) //, importId) //, ca.Format("2006-01-02"), cb.Format("2006-01-02"))

	//	qry := fmt.Sprintf(`
	//	select title1, strainname, sum(quantity) as quantity, sum(totalprice) as totalprice, unitofweight
	//from cartitems as c
	//group by title1, strainname, unitofweight
	//order by title1 limit 10
	//`)
	var qry string
	if importId != "all" {
		qry = fmt.Sprintf(` 
		select title1, sum(quantity) as totalquantity, sum(totalprice) as totalsales, unitofweight 
		from cartitems as c
		inner join orders as o
		on c.orderid = o._id
		where o.locationid = '%s' 
		and o.createdat >= '%s 05:00:00' and o.createdat <= '%s'		
		group by title1, unitofweight
		order by sum(totalprice) desc
		
		
`, importId, ca.Format("01-02-2006"), cb.Format("01-02-2006"))

	} else {
		qry = fmt.Sprintf(` 
		select title1, sum(quantity) as totalquantity, sum(totalprice) as totalsales, unitofweight 
		from cartitems as c
		inner join orders as o
		on c.orderid = o._id
		where o.createdat >= '%s 05:00:00' and o.createdat <= '%s'		
		group by title1, unitofweight
		order by sum(totalprice) desc
		
		
`, ca.Format("01-02-2006"), cb.Format("01-02-2006"))

	}

	//fmt.Println(qry)

	iterator, err := table.Pool.DB.Query(qry)
	lib.C(err)
	//fmt.Println(iterator)

	defer iterator.Close()

	var allsales float32

	p := message.NewPrinter(language.English)

	for iterator.Next() {
		var row = CartStatistic{}
		//fmt.Println("hello")
		err = iterator.Scan(
			&row.Name,
			&row.TotalSold,
			&row.TotalSales,
			&row.Unit,
		)
		lib.C(err)

		//fmt.Println(row)

		stat.Names = append(stat.Names, row.Name)
		//stat.Strains = append(stat.Strains, row.Strain)
		stat.Sold = append(stat.Sold, row.TotalSold)
		stat.TotalSold = stat.TotalSold + row.TotalSold
		//ts := strconv.FormatFloat(float64(row.TotalSales), 'f', 2, 64)
		allsales = allsales + row.TotalSales
		fl := strconv.FormatFloat(float64(row.TotalSales), 'f', 2, 32)
		f, err := strconv.ParseFloat(fl, 32)
		lib.C(err)
		stat.Sales = append(stat.Sales, p.Sprintf("$%.2f", f))
		stat.Units = append(stat.Units, row.Unit)

	}

	fl := strconv.FormatFloat(float64(allsales), 'f', 2, 32)
	f, err := strconv.ParseFloat(fl, 32)
	lib.C(err)

	stat.TotalSales = p.Sprintf(`$%.2f`, f)

	//stat.CategoryNames, stat.CategoryTotalSold, stat.CategoryTotalSales = table.GetCategoryStatistics(importId, ca.Format("01-02-2006"), cb.Format("01-02-2006"))

	//fmt.Println(stat)

	return

}
