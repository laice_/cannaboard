package Tables

import (
	"bitbucket.org/laice_/cannaboard/FDB"
	"bitbucket.org/laice_/cannaboard/lib"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/pkg/errors"
	"strconv"
	"strings"
	"time"
)

type WeeklyItemCountTable struct {
	Pool *FDB.DBPool
}

type WeeklyItemCountTableConfig struct {
	Pool *FDB.DBPool
}

//type ItemCountRow struct {
//	CreatedAt  string  `json:"createdAt"`
//	ImportId   string  `json:"importId"`
//	ItemType   string  `json:"itemType"`
//	ItemName   string  `json:"itemName"`
//	TotalItems float32 `json:"totalItems"`
//}

func NewWeeklyItemCountTable(cfg WeeklyItemCountTableConfig) (table WeeklyItemCountTable, err error) {
	if cfg.Pool == nil {
		err = errors.New("Can't create table without Pool instance")
		return
	}

	var t WeeklyItemCountTable
	t.setPool(cfg.Pool)

	table = t

	if err = table.createTable(); err != nil {
		err = errors.Wrapf(err, "Couldn't create table during init")
		return
	}

	return

}

func (table *WeeklyItemCountTable) setPool(pool *FDB.DBPool) {
	table.Pool = pool
}

func (table *WeeklyItemCountTable) createTable() (err error) {
	const qry = `
	CREATE TABLE IF NOT EXISTS weeklyItemCounts (		
		createdAt text,
		importId text,
		itemType text,
		itemName text,
		totalItems float,
		primary key(createdAt, importId, itemType, itemName)
		
	)
`
	fmt.Println("Creating Table")
	_, err = table.Pool.DB.Exec(qry)

	if err != nil {
		err = errors.Wrapf(err, "Sales day table creation query failed (%s)", qry)
		return
	}

	fmt.Println("Table Created")

	return

}

func (table *WeeklyItemCountTable) InsertItemCount(row ItemCountRow) (newRow ItemCountRow, err error) {
	if row.CreatedAt == "" {
		err = errors.Errorf("Can't create inventory without product day (%s)", spew.Sdump(row))
		return
	}

	const qry = `INSERT INTO weeklyItemCounts (
createdAt,
importId,
itemType,
itemName,
totalItems
) VALUES (
$1, $2, $3, $4, $5
)  ON CONFLICT (createdAt, importId, itemType, itemName) DO UPDATE SET 
totalItems = EXCLUDED.totalItems
WHERE weeklyItemCounts.createdAt=EXCLUDED.createdAt AND weeklyItemCounts.importId=EXCLUDED.importId 
        AND weeklyItemCounts.itemType=EXCLUDED.itemType
      AND weeklyItemCounts.itemName=EXCLUDED.itemName      
RETURNING createdAt, importId, itemType, itemName, totalItems   ;`

	/*
			= (
		EXCLUDED.createdAt,
		EXCLUDED.importId,
		EXCLUDED.type,
		EXCLUDED.name,
		EXCLUDED.count
		)
	*/

	err = table.Pool.DB.QueryRow(qry,
		row.CreatedAt,
		row.ImportId,
		row.ItemType,
		row.ItemName,
		row.TotalItems,
	).Scan(
		&newRow.CreatedAt,
		&newRow.ImportId,
		&newRow.ItemType,
		&newRow.ItemName,
		&newRow.TotalItems,
	)

	if err != nil {
		dump := spew.Sdump(row)
		err = errors.Wrapf(err, "Couldn't insert row into DB (%s)", spew.Sdump(row))
		if strings.Contains(dump, "does not exist") {
			fmt.Println("Retrying table creation")
			lib.C(table.createTable())
			return table.InsertItemCount(row)
		} else {
			return
		}

	}

	return
}

func (table *WeeklyItemCountTable) GetWeeklyItemCount(yearweek string, importId string) (rows []ItemCountRow, err error) {

	if yearweek == "" {
		err = errors.Errorf("Need yearweek (2006-1) to continue, %s", yearweek)
		return
	}
	qry := `SELECT  
	createdAt,
	importId,
	itemType,
	itemName,
	totalItems
	FROM weeklyItemCounts 
	WHERE createdAt = $1 AND importId = $2
	`
	if importId == "" {
		qry = `SELECT  
	createdAt,
	importId,
	itemType,
	itemName,
	totalItems

	FROM weeklyItemCounts 
	WHERE createdAt = $1 
	`
	}

	//a := ca.Format("2006-02-01")
	//b := cb.Format("2006-02-01")
	//fmt.Printf("Date: %s\nLocation: %d\nQuery:%s\n", d, locationId, qry)

	iterator, err := table.Pool.DB.Query(qry, yearweek, importId)

	if err != nil {
		err = errors.Errorf("Error selecting by day:\n%s\n", err.Error())
		return nil, err
	}

	for iterator.Next() {
		var row = ItemCountRow{}

		err = iterator.Scan(
			&row.CreatedAt,
			&row.ImportId,
			&row.ItemType,
			&row.ItemName,
			&row.TotalItems,
		)

		if err != nil {
			err = errors.Wrapf(err, "weekly item counts scanning failed: %s ", yearweek)
			return nil, err
		}

		rows = append(rows, row)

	}

	return
}

func (table *WeeklyItemCountTable) GetOrdersByLocationIdBetweenDates(locationId string, createdAfter string, createdBefore string, limit int) (rows []ItemCountRow, err error) {

	tz, err := time.LoadLocation("America/Denver")

	if err != nil {
		err = errors.Errorf("Error creating timezone")
		return nil, err
	}

	if locationId == "" || createdAfter == "" {
		err = errors.Errorf("Need LocationId and start date to continue, locId: %d, date: %s", locationId, createdAfter)
		return nil, err
	}

	ca, err := time.ParseInLocation(time.RFC3339, createdAfter, tz)

	if err != nil {
		err = errors.Errorf("Failed to parse date as RFC3339: %s", createdAfter)
		return nil, err
	}

	var cb time.Time
	if createdBefore == "" {
		cb = time.Now().In(tz)
	} else {
		cb, err = time.ParseInLocation(time.RFC3339, createdBefore, tz)
		if err != nil {
			err = errors.Errorf("Failed to parse date as RFC3339: %s", createdAfter)
			return nil, err
		}
		cb = cb.AddDate(0, 0, 2)

	}

	_, err = table.Pool.DB.Exec(`SET TIME ZONE 'America/Denver';`)

	if err != nil {
		err = errors.Errorf("Error setting DB time zone:\n%s\n", err.Error())
		return nil, err
	}
	qry := `
	select itemName, SUM(totalItems) as totalItems
from itemCounts as i
where importId = $1
and itemType = 'ProductName'
and createdAt >= $2
and createdAt <= $3
group by itemName, itemType
having SUM(totalItems) > 1
order by SUM(totalItems) desc, itemType
;
	`

	if limit != 0 {
		qry = qry + " LIMIT " + strconv.Itoa(limit)
	}
	//a := ca.Format("2006-02-01")
	//b := cb.Format("2006-02-01")
	//fmt.Printf("Date: %s\nLocation: %d\nQuery:%s\n", d, locationId, qry)

	iterator, err := table.Pool.DB.Query(qry, locationId, ca, cb)

	if err != nil {
		err = errors.Errorf("Error selecting by location id and date:\n%s\n", err.Error())
		return nil, err
	}

	for iterator.Next() {
		var row = ItemCountRow{}

		err = iterator.Scan(
			&row.ItemName,
			&row.TotalItems,
		)

		if err != nil {
			err = errors.Wrapf(err, "Order row scanning failed for location: %s and dates: %s - %s", locationId, ca, cb)
			return nil, err
		}

		//t, err2 := time.Parse(time.RFC3339, row.CreatedAt)
		//
		//if err2 != nil {
		//	err2 = errors.Wrapf(err, "Parsing createdAt failed for location: %s and dates: %s - %s", locationId, ca, cb)
		//	return nil, err
		//}
		//
		//fmt.Printf("After: %s\nBefore: %s\nTime: %s\n",
		//	ca.Format(time.RFC3339), cb.Format(time.RFC3339), t.Format(time.RFC3339))
		//
		//if t.After(ca.Add(6*time.Hour)) && t.Before(cb.Add(-12*time.Hour)) {

		//qry = `
		//SELECT
		//createdAt,
		//importId,
		//salesTotal,
		//itemsSold,
		//customerCount
		//FROM salesHour
		//WHERE importId = $1
		//AND createdAt >= $2
		//AND createdAt <= $3
		//	`
		//
		//hourTable, err := SaleHourTable(SalesHourTableConfig{
		//	Pool: table.Pool,
		//})
		//
		//if err != nil {
		//	err = errors.Wrapf(err, "Parsing createdAt failed for location: %s and dates: %s - %s", locationId, ca, cb)
		//	return nil, err
		//}
		//
		//row. err = hourTable.GetSalesHoursByLocationIdBetweenDates(locationId, createdAfter, createdBefore, 0)
		//
		//if err != nil {
		//	err = errors.Wrapf(err, "Fetching hours failed for location: %s and dates: %s - %s", locationId, ca, cb)
		//	return nil, err
		//}

		rows = append(rows, row)
		//}

	}

	return
}
