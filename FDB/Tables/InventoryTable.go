package Tables

import (
	"bitbucket.org/laice_/cannaboard/FDB"
	"github.com/davecgh/go-spew/spew"
	"github.com/pkg/errors"
)

type InventoriesTable struct {
	Pool *FDB.DBPool
}

type InventoriesTableConfig struct {
	Pool *FDB.DBPool
}

type InventoryRow struct {
	ProductID                               int     `json:"productId"`
	ClientID                                int     `json:"clientId"`
	ProductDescription                      string  `json:"productDescription"`
	ProductName                             string  `json:"productName"`
	PriceInMinorUnits                       int     `json:"priceInMinorUnits"`
	SKU                                     string  `json:"sku"`
	Nutrients                               string  `json:"nutrients"`
	ProductPictureURL                       string  `json:"productPictureURL"`
	PurchaseCategory                        string  `json:"purchaseCategory"`
	Category                                string  `json:"category"`
	Type                                    string  `json:"type"`
	Brand                                   string  `json:"brand"`
	IsMixAndMatch                           bool    `json:"isMixAndMatch"`
	IsStackable                             bool    `json:"isStackable"`
	ProductUnitOfMeasure                    string  `json:"productUnitOfMeasure"`
	ProductUnitOfMeasureToGramsMultiplier   string  `json:"productUnitOfMeasureToGramsMultiplier"`
	ProductWeight                           float32 `json:"productWeight"`
	Quantity                                float32 `json:"quantity"`
	InventoryUnitOfMeasure                  string  `json:"inventoryUnitOfMeasure"`
	InventoryUnitOfMeasureToGramsMultiplier float32 `json:"inventoryUnitOfMeasureToGramsMultiplier"`
	LocationID                              int     `json:"locationId"`
	LocationName                            string  `json:"locationName"`
	CurrencyCode                            string  `json:"currencyCode"`
}

func InventoryTable(cfg InventoriesTableConfig) (table InventoriesTable, err error) {
	if cfg.Pool == nil {
		err = errors.New("Can't create table without Pool instance")
		return
	}

	var t InventoriesTable
	t.setPool(cfg.Pool)

	table = t

	if err = table.createTable(); err != nil {
		err = errors.Wrapf(err, "Couldn't create table during init")
		return
	}

	return

}

func (table *InventoriesTable) setPool(pool *FDB.DBPool) {
	table.Pool = pool
}

func (table *InventoriesTable) createTable() (err error) {
	const qry = `
	CREATE TABLE IF NOT EXISTS inventories (
		productId int PRIMARY KEY,
		clientId int,
		productDescription text,
		productName text,
		priceInMinorUnits int,
		sku text,
		nutrients text,
		productPictureURL text,
		purchaseCategory text,
		category text,
		typ text,
		brand text,
		isMixAndMatch boolean,
		isStackable boolean,
		productUnitOfMeasure text,
		productUnitOfMeasureToGramsMultiplier text,
		productWeight real,
		quantity real,
		inventoryUnitOfMeasure text,
		inventoryUnitOfMeasureToGramsMultiplier real,
		locationId int,
		locationName text,
		currencyCode text)
`
	_, err = table.Pool.DB.Exec(qry)

	if err != nil {
		err = errors.Wrapf(err, "Inventory table creation query failed (%s)", qry)
		return
	}

	return

}

func (table *InventoriesTable) InsertInventory(row InventoryRow) (newRow InventoryRow, err error) {
	if row.ProductID == 0 {
		err = errors.Errorf("Can't create inventory without product id (%s)", spew.Sdump(row))
		return
	}

	//fmt.Printf("Inserting product %s\n", row.ProductName)

	//const qry = `INSERT INTO inventories (
	//	productId,
	//	clientId,
	//	productDescription,
	//	productName, priceInMinorUnits, sku, nutrients, productPictureURL, purchaseCategory, category, typ, brand, isMixAndMatch, isStackable, productUnitOfMeasure, productUnitOfMeasureToGramsMultiplier, productWeight, quantity, inventoryUnitOfMeasure, inventoryUnitOfMeasureToGramsMultiplier, locationId, locationName, currencyCode ) VALUES ( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11
	//				$12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23 )  ON CONFLICT (productId) DO UPDATE SET ( clientId, productDescription, productName, priceInMinorUnits, sku, nutrients, productPictureURL, purchaseCategory, category, typ, brand, isMixAndMatch, isStackable, productUnitOfMeasure, productUnitOfMeasureToGramsMultiplier, productWeight, quantity, inventoryUnitOfMeasure, inventoryUnitOfMeasureToGramsMultiplier, locationId, locationName, currencyCode ) = ( EXCLUDED.clientId, EXCLUDED.productDescription, EXCLUDED.productName, EXCLUDED.priceInMinorUnits, EXCLUDED.sku, EXCLUDED.nutrients, EXCLUDED.productPictureURL, EXCLUDED.purchaseCategory, EXCLUDED.category, EXCLUDED.typ, EXCLUDED.brand, EXCLUDED.isMixAndMatch, EXCLUDED.isStackable, EXCLUDED.productUnitOfMeasure, EXCLUDED.productUnitOfMeasureToGramsMultiplier, EXCLUDED.productWeight, EXCLUDED.quantity, EXCLUDED.inventoryUnitOfMeasure, EXCLUDED.inventoryUnitOfMeasureToGramsMultiplier, EXCLUDED.locationId, EXCLUDED.locationName, EXCLUDED.currencyCode ) RETURNING productName;`
	const qry = `INSERT INTO inventories (
productId,
clientId,
productDescription,
productName,
priceInMinorUnits,
sku,
nutrients,
productPictureURL,
purchaseCategory,
category,
typ,
brand,
isMixAndMatch,
isStackable,
productUnitOfMeasure,
productUnitOfMeasureToGramsMultiplier,
productWeight,
quantity,
inventoryUnitOfMeasure,
inventoryUnitOfMeasureToGramsMultiplier,
locationId,
locationName,
currencyCode
) VALUES (
$1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11,
							$12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23
)  ON CONFLICT (productId) DO UPDATE SET ( 
clientId, 
productDescription, 
productName, 
priceInMinorUnits,
sku, 
nutrients, 
productPictureURL, 
purchaseCategory, 
category, 
typ, 
brand, 
isMixAndMatch, 
isStackable, 
productUnitOfMeasure, 
productUnitOfMeasureToGramsMultiplier,
productWeight, 
quantity, 
inventoryUnitOfMeasure, 
inventoryUnitOfMeasureToGramsMultiplier, 
locationId, 
locationName, 
currencyCode ) = ( 
EXCLUDED.clientId, 
EXCLUDED.productDescription, 
EXCLUDED.productName, 
EXCLUDED.priceInMinorUnits, 
EXCLUDED.sku, 
EXCLUDED.nutrients, 
EXCLUDED.productPictureURL, 
EXCLUDED.purchaseCategory, 
EXCLUDED.category, 
EXCLUDED.typ, 
EXCLUDED.brand, 
EXCLUDED.isMixAndMatch, 
EXCLUDED.isStackable, 
EXCLUDED.productUnitOfMeasure, 
EXCLUDED.productUnitOfMeasureToGramsMultiplier, 
EXCLUDED.productWeight, 
EXCLUDED.quantity, 
EXCLUDED.inventoryUnitOfMeasure, 
EXCLUDED.inventoryUnitOfMeasureToGramsMultiplier, 
EXCLUDED.locationId, 
EXCLUDED.locationName,
EXCLUDED.currencyCode ) RETURNING productName;`

	//	const qry = `
	//	INSERT INTO inventories ( productId,
	//		clientId,
	//		productDescription,
	//		productName,
	//		priceInMinorUnits,
	//		sku,
	//		nutrients,
	//		productPictureURL,
	//		purchaseCategory,
	//		category,
	//		typ,
	//		brand,
	//		isMixAndMatch,
	//		isStackable,
	//		productUnitOfMeasure,
	//		productUnitOfMeasureToGramsMultiplier,
	//		productWeight,
	//		quantity,
	//		inventoryUnitOfMeasure,
	//		inventoryUnitOfMeasureToGramsMultiplier,
	//		locationId,
	//		locationName,
	//		currencyCode ) VALUES (
	//							$1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11
	//							$12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23)
	//							ON CONFLICT (productId) DO UPDATE SET (
	//		clientId,
	//		productDescription,
	//		productName,
	//		priceInMinorUnits,
	//		sku,
	//		nutrients,
	//		productPictureURL,
	//		purchaseCategory,
	//		category,
	//		typ,
	//		brand,
	//		isMixAndMatch,
	//		isStackable,
	//		productUnitOfMeasure,
	//		productUnitOfMeasureToGramsMultiplier,
	//		productWeight,
	//		quantity,
	//		inventoryUnitOfMeasure,
	//		inventoryUnitOfMeasureToGramsMultiplier,
	//		locationId,
	//		locationName,
	//		currencyCode ) =
	//							(
	//EXCLUDED.clientId,
	//EXCLUDED.productDescription,
	//EXCLUDED.productName,
	//EXCLUDED.priceInMinorUnits,
	//EXCLUDED.sku,
	//EXCLUDED.nutrients,
	//EXCLUDED.productPictureURL,
	//EXCLUDED.purchaseCategory,
	//EXCLUDED.category,
	//EXCLUDED.typ,
	//EXCLUDED.brand,
	//EXCLUDED.isMixAndMatch,
	//EXCLUDED.isStackable,
	//EXCLUDED.productUnitOfMeasure,
	//EXCLUDED.productUnitOfMeasureToGramsMultiplier
	//EXCLUDED.productWeight,
	//EXCLUDED.quantity,
	//EXCLUDED.inventoryUnitOfMeasure,
	//EXCLUDED.inventoryUnitOfMeasureToGramsMultiplier,
	//EXCLUDED.locationId,
	//EXCLUDED.locationName,
	//EXCLUDED.currencyCode
	//) RETURNING productName;
	//`
	//fmt.Printf("%s\n", qry)
	err = table.Pool.DB.QueryRow(qry,
		row.ProductID,
		row.ClientID,
		row.ProductDescription,
		row.ProductName,
		row.PriceInMinorUnits,
		row.SKU,
		row.Nutrients,
		row.ProductPictureURL,
		row.PurchaseCategory,
		row.Category,
		row.Type,
		row.Brand,
		row.IsMixAndMatch,
		row.IsStackable,
		row.ProductUnitOfMeasure,
		row.ProductUnitOfMeasureToGramsMultiplier,
		row.ProductWeight,
		row.Quantity,
		row.InventoryUnitOfMeasure,
		row.InventoryUnitOfMeasureToGramsMultiplier,
		row.LocationID,
		row.LocationName,
		row.CurrencyCode).Scan(&newRow.ProductName)

	if err != nil {
		err = errors.Wrapf(err, "Couldn't insert row into DB (%s)", spew.Sdump(row))
		return
	} else {
		//fmt.Printf("Inventory %s inserted\n", row.ProductName)
	}

	return
}

func (table *InventoriesTable) GetInventoryByLocationId(locationId int) (rows []InventoryRow, err error) {
	if locationId == 0 {
		err = errors.Errorf("Can't get location row with empty locationId")
		return
	}

	const qry = `SELECT  
	productId,
clientId,
productDescription,
productName,
priceInMinorUnits,
sku,
nutrients,
productPictureURL,
purchaseCategory,
category,
typ,
brand,
isMixAndMatch,
isStackable,
productUnitOfMeasure,
productUnitOfMeasureToGramsMultiplier,
productWeight,
quantity,
inventoryUnitOfMeasure,
inventoryUnitOfMeasureToGramsMultiplier,
locationId,
locationName,
currencyCode
	FROM inventories WHERE locationId = $1`

	iterator, err := table.Pool.DB.Query(qry, locationId)

	if err != nil {
		err = errors.Wrapf(err, "Failed to get Inventory (id = %s)", locationId)
		return
	}

	defer iterator.Close()

	for iterator.Next() {
		var row = InventoryRow{}

		err = iterator.Scan(
			&row.ProductID,
			&row.ClientID,
			&row.ProductDescription,
			&row.ProductName,
			&row.PriceInMinorUnits,
			&row.SKU,
			&row.Nutrients,
			&row.ProductPictureURL,
			&row.PurchaseCategory,
			&row.Category,
			&row.Type,
			&row.Brand,
			&row.IsMixAndMatch,
			&row.IsStackable,
			&row.ProductUnitOfMeasure,
			&row.ProductUnitOfMeasureToGramsMultiplier,
			&row.ProductWeight,
			&row.Quantity,
			&row.InventoryUnitOfMeasure,
			&row.InventoryUnitOfMeasureToGramsMultiplier,
			&row.LocationID,
			&row.LocationName,
			&row.CurrencyCode)

		if err != nil {
			err = errors.Wrapf(err, "Inventory row scanning failed for location: %s", locationId)
			return
		}

		rows = append(rows, row)
	}
	return
}
