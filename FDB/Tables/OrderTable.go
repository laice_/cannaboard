package Tables

import (
	"bitbucket.org/laice_/cannaboard/FDB"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/pkg/errors"
	"strconv"
	"time"
)

type OrdersTable struct {
	Pool *FDB.DBPool
}

type OrdersTableConfig struct {
	Pool *FDB.DBPool
}

type OrderRow struct {
	ID            string          `json:"_id"`
	ClientID      string          `json:"clientId"`
	CreatedAt     string          `json:"createdAt"`
	CustomerID    string          `json:"customerId"`
	CurrentPoints int             `json:"currentPoints"`
	CustomerType  string          `json:"customerType"`
	CustomerName  string          `json:"name"`
	LocationID    string          `json:"locationId"`
	LocationName  string          `json:"locationName"`
	Voided        bool            `json:"voided"`
	CompletedOn   string          `json:"completedOn"`
	OrderStatus   string          `json:"orderStatus"`
	OrderType     string          `json:"orderType"`
	Totals        []OrderTotalRow `json:"totals"`
	ItemsInCart   []CartItemRow   `json:"itemsInCart"`
}

func OrderTable(cfg OrdersTableConfig) (table OrdersTable, err error) {
	if cfg.Pool == nil {
		err = errors.New("Can't create table without Pool instance")
		return
	}

	var t OrdersTable
	t.setPool(cfg.Pool)

	table = t

	if err = table.createTable(); err != nil {
		err = errors.Wrapf(err, "Couldn't create table during init")
		return
	}

	return

}

func (table *OrdersTable) setPool(pool *FDB.DBPool) {

	table.Pool = pool
}

func (table *OrdersTable) createTable() (err error) {
	const qry = `
	CREATE TABLE IF NOT EXISTS orders (		
		_id 			text PRIMARY KEY,
		clientId 		text,
		createdAt 		timestamp with time zone,
		customerId 		text,
		currentPoints 	int,
		customerType 	text,
		customerName 	text,
		locationId 		text,
		locationName 	text,
		voided 			boolean,
		completedOn 	timestamp with time zone,
		orderStatus 	text,
		orderType 		text
	)
`
	_, err = table.Pool.DB.Exec(qry)

	if err != nil {
		err = errors.Wrapf(err, "Order table creation query failed (%s)", qry)
		return
	}

	return

}

func (table *OrdersTable) InsertOrder(row OrderRow) (newRow OrderRow, err error) {
	if row.ID == "" {
		err = errors.Errorf("Can't create inventory without product id (%s)", spew.Sdump(row))
		return
	}

	const qry = `INSERT INTO orders (
_id 		,	
clientId 	,	
createdAt 	,	
customerId 	,
currentPoints,
customerType ,
customerName ,
locationId 	,
locationName ,
voided 		,
completedOn ,
orderStatus ,
orderType 	
) VALUES (
$1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13
)  ON CONFLICT (_id) DO UPDATE SET (
_id 		,	
clientId 	,	
createdAt 	,	
customerId 	,
currentPoints,
customerType ,
customerName ,
locationId 	,
locationName ,
voided 		,
completedOn ,
orderStatus ,
orderType 	
) = ( 
EXCLUDED._id 		,	
EXCLUDED.clientId 	,	
EXCLUDED.createdAt 	,	
EXCLUDED.customerId 	,
EXCLUDED.currentPoints,
EXCLUDED.customerType ,
EXCLUDED.customerName ,
EXCLUDED.locationId 	,
EXCLUDED.locationName ,
EXCLUDED.voided 		,
EXCLUDED.completedOn ,
EXCLUDED.orderStatus ,
EXCLUDED.orderType 	
) RETURNING _id 		,
clientId 	,
createdAt 	,
customerId 	,  
currentPoints, 
customerType , 
customerName , 
locationId 	,  
locationName , 
voided 		,  
completedOn ,  
orderStatus ,  
orderType 	   ;`

	err = table.Pool.DB.QueryRow(qry,
		row.ID,
		row.ClientID,
		row.CreatedAt,
		row.CustomerID,
		row.CurrentPoints,
		row.CustomerType,
		row.CustomerName,
		row.LocationID,
		row.LocationName,
		row.Voided,
		row.CompletedOn,
		row.OrderStatus,
		row.OrderType,
	).Scan(
		&newRow.ID,
		&newRow.ClientID,
		&newRow.CreatedAt,
		&newRow.CustomerID,
		&newRow.CurrentPoints,
		&newRow.CustomerType,
		&newRow.CustomerName,
		&newRow.LocationID,
		&newRow.LocationName,
		&newRow.Voided,
		&newRow.CompletedOn,
		&newRow.OrderStatus,
		&newRow.OrderType,
	)

	if err != nil {
		err = errors.Wrapf(err, "Couldn't insert row into DB (%s)", spew.Sdump(row))
		return
	} else {
		//fmt.Printf("Order %s inserted\n", row.ProductName)
	}

	return
}

func (table *OrdersTable) GetOrdersByLocationIdBetweenDates(locationId string, createdAfter string, createdBefore string, limit int) (rows []OrderRow, err error) {

	tz, err := time.LoadLocation("America/Denver")

	if err != nil {
		err = errors.Errorf("Error creating timezone")
		return
	}

	if locationId == "" || createdAfter == "" {
		err = errors.Errorf("Need LocationId and start date to continue, locId: %d, date: %s", locationId, createdAfter)
		return
	}

	ca, err := time.ParseInLocation(time.RFC3339, createdAfter, tz)

	if err != nil {
		err = errors.Errorf("Failed to parse date as RFC3339: %s", createdAfter)
		return
	}

	var cb time.Time
	if createdBefore == "" {
		cb = time.Now().In(tz)
	} else {
		cb, err = time.ParseInLocation(time.RFC3339, createdBefore, tz)
		if err != nil {
			err = errors.Errorf("Failed to parse date as RFC3339: %s", createdAfter)
			return
		}
		cb = cb.AddDate(0, 0, 2)

	}

	_, err = table.Pool.DB.Exec(`SET TIME ZONE 'America/Denver';`)

	if err != nil {
		err = errors.Errorf("Error setting DB time zone:\n%s\n", err.Error())
		return
	}

	//qry := `SELECT
	//*
	//
	//FROM orders
	//WHERE locationid = '$1'
	//AND createdat between '$2 14:00:00' and '$3 05:00:00'
	//`

	qry := fmt.Sprintf(`SELECT  
	* 

	FROM orders 
	WHERE locationid = '%s' 
	AND createdat between '%s 14:00:00' and '%s 05:00:00'
	`, locationId, ca.Format("01-02-2006"), cb.Format("01-02-2006"))

	if limit != 0 {
		qry = qry + " LIMIT " + strconv.Itoa(limit)
	}
	//a := ca.Format("2006-02-01")
	//b := cb.Format("2006-02-01")
	fmt.Printf("Date: %s-%s\nLocation: %s\nQuery:%s\n", ca, cb, locationId, qry)

	iterator, err := table.Pool.DB.Query(qry)

	if err != nil {
		err = errors.Errorf("Error selecting by location id and date:\n%s\n", err.Error())
		return
	}

	for iterator.Next() {
		var row = OrderRow{}

		err = iterator.Scan(
			&row.ID,
			&row.ClientID,
			&row.CreatedAt,
			&row.CustomerID,
			&row.CurrentPoints,
			&row.CustomerType,
			&row.CustomerName,
			&row.LocationID,
			&row.LocationName,
			&row.Voided,
			&row.CompletedOn,
			&row.OrderStatus,
			&row.OrderType,
		)

		if err != nil {
			err = errors.Wrapf(err, "Order row scanning failed for location: %s and dates: %s - %s", locationId, ca, cb)
			return
		}

		//t, err2 := time.Parse(time.RFC3339, row.CreatedAt)

		//if err2 != nil {
		//	err2 = errors.Wrapf(err, "Parsing createdAt failed for location: %s and dates: %s - %s", locationId, ca, cb)
		//	return
		//}

		//fmt.Printf("After: %s\nBefore: %s\nTime: %s\n",
		//	ca.Format(time.RFC3339), cb.Format(time.RFC3339), t.Format(time.RFC3339))

		fmt.Println(row)
		//if t.After(ca.Add(6*time.Hour)) && t.Before(cb.Add(-12*time.Hour)) {
		rows = append(rows, row)
		//}

	}

	return
}

func (table *OrdersTable) GetOrdersByLocationIdBetweenDates2(locationId string, createdAfter string, createdBefore string, limit int) (rows []OrderRow, err error) {

	tz, err := time.LoadLocation("America/Denver")

	if err != nil {
		err = errors.Errorf("Error creating timezone")
		return
	}

	if locationId == "" || createdAfter == "" {
		err = errors.Errorf("Need LocationId and start date to continue, locId: %d, date: %s", locationId, createdAfter)
		return
	}

	ca, err := time.ParseInLocation(time.RFC3339, createdAfter, tz)

	if err != nil {
		err = errors.Errorf("Failed to parse date as RFC3339: %s", createdAfter)
		return
	}

	var cb time.Time
	if createdBefore == "" {
		cb = time.Now().In(tz)
	} else {
		cb, err = time.ParseInLocation(time.RFC3339, createdBefore, tz)
		if err != nil {
			err = errors.Errorf("Failed to parse date as RFC3339: %s", createdAfter)
			return
		}
		//cb = cb.AddDate(0, 0, 2)

	}

	_, err = table.Pool.DB.Exec(`SET TIME ZONE 'America/Denver';`)

	if err != nil {
		err = errors.Errorf("Error setting DB time zone:\n%s\n", err.Error())
		return
	}

	//qry := `SELECT
	//*
	//
	//FROM orders
	//WHERE locationid = '$1'
	//AND createdat between '$2 14:00:00' and '$3 05:00:00'
	//`

	qry := fmt.Sprintf(`SELECT  
	* 

	FROM orders 
	WHERE locationid = '%s' 
	AND createdat between '%s 14:00:00' and '%s 05:00:00'
	`, locationId, ca.Format("01-02-2006"), cb.Format("01-02-2006"))

	if limit != 0 {
		qry = qry + " LIMIT " + strconv.Itoa(limit)
	}
	//a := ca.Format("2006-02-01")
	//b := cb.Format("2006-02-01")
	fmt.Printf("Date: %s-%s\nLocation: %s\nQuery:%s\n", ca, cb, locationId, qry)

	iterator, err := table.Pool.DB.Query(qry)

	if err != nil {
		err = errors.Errorf("Error selecting by location id and date:\n%s\n", err.Error())
		return
	}

	for iterator.Next() {
		var row = OrderRow{}

		err = iterator.Scan(
			&row.ID,
			&row.ClientID,
			&row.CreatedAt,
			&row.CustomerID,
			&row.CurrentPoints,
			&row.CustomerType,
			&row.CustomerName,
			&row.LocationID,
			&row.LocationName,
			&row.Voided,
			&row.CompletedOn,
			&row.OrderStatus,
			&row.OrderType,
		)

		if err != nil {
			err = errors.Wrapf(err, "Order row scanning failed for location: %s and dates: %s - %s", locationId, ca, cb)
			return
		}

		//t, err2 := time.Parse(time.RFC3339, row.CreatedAt)

		//if err2 != nil {
		//	err2 = errors.Wrapf(err, "Parsing createdAt failed for location: %s and dates: %s - %s", locationId, ca, cb)
		//	return
		//}

		//fmt.Printf("After: %s\nBefore: %s\nTime: %s\n",
		//	ca.Format(time.RFC3339), cb.Format(time.RFC3339), t.Format(time.RFC3339))

		fmt.Println(row)
		//if t.After(ca.Add(6*time.Hour)) && t.Before(cb.Add(-12*time.Hour)) {
		rows = append(rows, row)
		//}

	}

	return
}

func (table *OrdersTable) GetOrdersByLocationIdAndDate(locationId string, date string, limit int) (rows []OrderRow, err error) {

	if locationId == "" || date == "" {
		err = errors.Errorf("Need LocationId and Date to continue, locId: %d, date: %s", locationId, date)
		return
	}

	day, err := time.Parse(time.RFC3339, date)

	//dayBefore := dayAfter.AddDate(0, 0, 2).Format(time.RFC3339)
	//
	//after := dayAfter.AddDate(0, 0, -1).Format(time.RFC3339)

	if err != nil {
		err = errors.Errorf("Failed to parse date as RFC3339: %s", date)
	}

	qry := `SELECT  
	_id 		,
	clientId 	,
	createdAt 	,
	customerId 	, 
	currentPoints,
	customerType ,
	customerName ,
	locationId 	, 
	locationName ,
	voided 		, 
	completedOn , 
	orderStatus , 
	orderType 	  

	FROM orders 
	WHERE locationId = $1 
	AND date_trunc('day',createdAt) = $2
	`

	if limit != 0 {
		qry = qry + " LIMIT " + strconv.Itoa(limit)
	}
	d := day.Format("2006-02-01")
	//fmt.Printf("Date: %s\nLocation: %d\nQuery:%s\n", d, locationId, qry)

	iterator, err := table.Pool.DB.Query(qry, locationId, d)

	if err != nil {
		err = errors.Errorf("Error selecting by location id and date: %s", err.Error())
		return
	}

	for iterator.Next() {
		var row = OrderRow{}

		err = iterator.Scan(
			&row.ID,
			&row.ClientID,
			&row.CreatedAt,
			&row.CustomerID,
			&row.CurrentPoints,
			&row.CustomerType,
			&row.CustomerName,
			&row.LocationID,
			&row.LocationName,
			&row.Voided,
			&row.CompletedOn,
			&row.OrderStatus,
			&row.OrderType,
		)

		if err != nil {
			err = errors.Wrapf(err, "Order row scanning failed for location: %s and date: %s", locationId, date)
			return
		}

		rows = append(rows, row)
	}

	return
}

func (table *OrdersTable) GetOrdersByLocationId(locationId string) (rows []OrderRow, err error) {
	if locationId == "" {
		err = errors.Errorf("Can't get location row with empty locationId")
		return
	}

	const qry = `SELECT  
	_id 		,
	clientId 	,
	createdAt 	,
	customerId 	, 
	currentPoints,
	customerType ,
	customerName ,
	locationId 	, 
	locationName ,
	voided 		, 
	completedOn , 
	orderStatus , 
	orderType 	  

	FROM orders WHERE locationId = $1`

	iterator, err := table.Pool.DB.Query(qry, locationId)

	if err != nil {
		err = errors.Wrapf(err, "Failed to get Order (locationId = %s)", locationId)
		return
	}

	defer iterator.Close()

	for iterator.Next() {
		var row = OrderRow{}

		err = iterator.Scan(
			&row.ID,
			&row.ClientID,
			&row.CreatedAt,
			&row.CustomerID,
			&row.CurrentPoints,
			&row.CustomerType,
			&row.CustomerName,
			&row.LocationID,
			&row.LocationName,
			&row.Voided,
			&row.CompletedOn,
			&row.OrderStatus,
			&row.OrderType,
		)

		if err != nil {
			err = errors.Wrapf(err, "Order row scanning failed for location: %s", locationId)
			return
		}

		rows = append(rows, row)
	}
	return
}

func (table *OrdersTable) GetOrders() (rows []OrderRow, err error) {

	const qry = `SELECT  
	_id 		,
	clientId 	,
	createdAt 	,
	customerId 	, 
	currentPoints,
	customerType ,
	customerName ,
	locationId 	, 
	locationName ,
	voided 		, 
	completedOn , 
	orderStatus , 
	orderType 	  

	FROM orders`

	iterator, err := table.Pool.DB.Query(qry)

	if err != nil {
		err = errors.Wrapf(err, "Failed to get all Orders")
		return
	}

	defer iterator.Close()

	for iterator.Next() {
		var row = OrderRow{}

		err = iterator.Scan(
			&row.ID,
			&row.ClientID,
			&row.CreatedAt,
			&row.CustomerID,
			&row.CurrentPoints,
			&row.CustomerType,
			&row.CustomerName,
			&row.LocationID,
			&row.LocationName,
			&row.Voided,
			&row.CompletedOn,
			&row.OrderStatus,
			&row.OrderType,
		)

		if err != nil {
			err = errors.Wrapf(err, "Order row scanning failed for all orders")
			return
		}

		rows = append(rows, row)
	}
	return
}
