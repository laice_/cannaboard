package Tables

import (
	"bitbucket.org/laice_/cannaboard/FDB"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/pkg/errors"
)

type LocationsTable struct {
	Pool *FDB.DBPool
}

type LocationsTableConfig struct {
	Pool *FDB.DBPool
}

type LocationRow struct {
	LocationID       int    `json:"locationId"`
	LocationName     string `json:"locationName"`
	ImportID         string `json:"importId"`
	Website          string `json:"website"`
	HoursOfOperation string `json:"hoursOfOperation"`
	ClientId         int    `json:"clientId"`
	ClientName       string `json:"clientName"`
	LocationLogoURL  string `json:"locationLogoURL"`
	TimeZone         string `json:"timeZone"`
	PhoneNumber      string `json:"phoneNumber"`
	Email            string `json:"email"`
}

func LocationTable(cfg LocationsTableConfig) (table LocationsTable, err error) {
	if cfg.Pool == nil {
		err = errors.New("Can't create table without Pool instance")
		return
	}

	var t LocationsTable
	t.setPool(cfg.Pool)

	table = t

	if err = table.createTable(); err != nil {
		err = errors.Wrapf(err, "Couldn't create table during init")
		return
	}

	return

}

func (table *LocationsTable) setPool(pool *FDB.DBPool) {
	table.Pool = pool
}

func (table *LocationsTable) createTable() (err error) {
	const qry = `
	CREATE TABLE IF NOT EXISTS locations (
		locationId int PRIMARY KEY,
		locationName text,
		importId text NOT NULL,
		website text,
		hoursOfOperation text,
		clientId int,
		clientName text,
		locationLogoURL text,
		timeZone text,
		phoneNumber text,
		email text)
`
	_, err = table.Pool.DB.Exec(qry)

	if err != nil {
		err = errors.Wrapf(err, "Locations table creation query failed (%s)", qry)
		return
	}

	return

}

func (table *LocationsTable) InsertLocation(row LocationRow) (newRow LocationRow, err error) {
	if row.LocationID == 0 {
		err = errors.Errorf("Can't create location without LocationID (%s)", spew.Sdump(row))
		return
	}

	fmt.Printf("Inserting location %s\n", row.LocationName)

	const qry = `
	INSERT INTO locations ( locationId, locationName, importId, 
							website, hoursOfOperation, clientId, 
							clientName, locationLogoURL, timeZone, 
							phoneNumber, email ) VALUES ( 
							$1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)  
							ON CONFLICT (locationId) DO UPDATE SET (  
							locationName, importId, website, 
							hoursOfOperation, clientId, clientName, 
							locationLogoURL, timeZone, phoneNumber, email ) = 
							( EXCLUDED.locationName, EXCLUDED.importId, EXCLUDED.website, 
							EXCLUDED.hoursOfOperation, EXCLUDED.clientId, EXCLUDED.clientName, 
							EXCLUDED.locationLogoURL, EXCLUDED.timeZone, EXCLUDED.phoneNumber, 
							EXCLUDED.email ) RETURNING locationId, importId, locationName;
`
	//fmt.Printf("%s\n", qry)
	err = table.Pool.DB.QueryRow(qry, row.LocationID,
		row.LocationName, row.ImportID, row.Website, row.HoursOfOperation, row.ClientId, row.ClientName,
		row.LocationLogoURL, row.TimeZone, row.PhoneNumber, row.Email).Scan(&newRow.LocationID,
		&newRow.ImportID, &newRow.LocationName)

	if err != nil {
		err = errors.Wrapf(err, "Couldn't insert row into DB (%s)", spew.Sdump(row))
		return
	} else {
		fmt.Printf("Location %s inserted\n", row.LocationName)
	}

	return
}

func (table *LocationsTable) GetLocationById(locationId int) (row LocationRow, err error) {
	if locationId == 0 {
		err = errors.Errorf("Can't get location row with empty locationId")
		return
	}

	const qry = `SELECT  
	locationId, locationName, importId, website, hoursOfOperation, clientId, clientName, 
	locationLogoURL, timeZone, phoneNumber, email
	FROM locations WHERE locationId = $1`

	err = table.Pool.DB.QueryRow(qry, locationId).Scan(&row.LocationID, &row.LocationName, &row.ImportID,
		&row.Website, &row.HoursOfOperation, &row.ClientId, &row.ClientName, &row.LocationLogoURL,
		&row.TimeZone, &row.PhoneNumber, &row.Email)

	if err != nil {
		err = errors.Wrapf(err, "Failed to get Location (id = %s)", locationId)
		return
	}

	return
}

func (table *LocationsTable) GetAllLocations(filter []int) (rows []LocationRow, err error) {

	const qry = `SELECT  
	locationId, locationName, importId, website, hoursOfOperation, clientId, clientName, 
	locationLogoURL, timeZone, phoneNumber, email
	FROM locations `

	iterator, err := table.Pool.DB.Query(qry)

	if err != nil {
		err = errors.Wrapf(err, "Failed to get All Locations")
		return
	}

	for iterator.Next() {
		var row = LocationRow{}
		err = iterator.Scan(&row.LocationID, &row.LocationName, &row.ImportID,
			&row.Website, &row.HoursOfOperation, &row.ClientId, &row.ClientName, &row.LocationLogoURL,
			&row.TimeZone, &row.PhoneNumber, &row.Email)
		skip := false
		for _, filt := range filter {
			//fmt.Printf("Checking filter %d against loc %d", filt, row.LocationID)

			if filt == row.LocationID {
				//fmt.Printf("Match found %d == %d", filt, row.LocationID)
				skip = true
			}
		}

		if skip == false {
			rows = append(rows, row)
		}

	}

	return
}
