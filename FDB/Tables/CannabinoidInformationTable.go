package Tables

import (
	"bitbucket.org/laice_/cannaboard/FDB"
	"github.com/davecgh/go-spew/spew"
	"github.com/pkg/errors"
)

type CannabinoidsTable struct {
	Pool *FDB.DBPool
}

type CannabinoidsTableConfig struct {
	Pool *FDB.DBPool
}

type CannabinoidRow struct {
	ProductID                      int     `json:"productId"`
	Name                           string  `json:"name"`
	LowerRange                     float32 `json:"lowerRange"`
	UpperRange                     float32 `json:"upperRange"`
	UnitOfMeasure                  string  `json:"unitOfMeasure"`
	UnitOfMeasureToGramsMultiplier float32 `json:"unitOfMeasureToGramsMultiplier"`
}

func CannabinoidTable(cfg CannabinoidsTableConfig) (table CannabinoidsTable, err error) {
	if cfg.Pool == nil {
		err = errors.New("Can't create table without Pool instance")
		return
	}

	var t CannabinoidsTable
	t.setPool(cfg.Pool)

	table = t

	if err = table.createTable(); err != nil {
		err = errors.Wrapf(err, "Couldn't create table during init")
		return
	}

	return

}

func (table *CannabinoidsTable) setPool(pool *FDB.DBPool) {
	table.Pool = pool
}

func (table *CannabinoidsTable) createTable() (err error) {
	const qry = `
	CREATE TABLE IF NOT EXISTS cannabinoids (		
		productId int NOT NULL,
		name	text NOT NULL,
		primary key (productId, name),
		lowerRange real,
		upperRange real,
		unitOfMeasure text,
		unitOfMeasureToGramsMultiplier real
)
`
	_, err = table.Pool.DB.Exec(qry)

	if err != nil {
		err = errors.Wrapf(err, "Cannabinoid table creation query failed (%s)", qry)
		return
	}

	return

}

func (table *CannabinoidsTable) InsertCannabinoid(row CannabinoidRow) (newRow CannabinoidRow, err error) {
	if row.ProductID == 0 {
		err = errors.Errorf("Can't create inventory without product id (%s)", spew.Sdump(row))
		return
	}

	//fmt.Printf("Inserting product %s\n", row.ProductName)

	//const qry = `INSERT INTO cannabinoids (
	//	productId,
	//	clientId,
	//	productDescription,
	//	productName, priceInMinorUnits, sku, nutrients, productPictureURL, purchaseCategory, category, typ, brand, isMixAndMatch, isStackable, productUnitOfMeasure, productUnitOfMeasureToGramsMultiplier, productWeight, quantity, inventoryUnitOfMeasure, inventoryUnitOfMeasureToGramsMultiplier, locationId, locationName, currencyCode ) VALUES ( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11
	//				$12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23 )  ON CONFLICT (productId) DO UPDATE SET ( clientId, productDescription, productName, priceInMinorUnits, sku, nutrients, productPictureURL, purchaseCategory, category, typ, brand, isMixAndMatch, isStackable, productUnitOfMeasure, productUnitOfMeasureToGramsMultiplier, productWeight, quantity, inventoryUnitOfMeasure, inventoryUnitOfMeasureToGramsMultiplier, locationId, locationName, currencyCode ) = ( EXCLUDED.clientId, EXCLUDED.productDescription, EXCLUDED.productName, EXCLUDED.priceInMinorUnits, EXCLUDED.sku, EXCLUDED.nutrients, EXCLUDED.productPictureURL, EXCLUDED.purchaseCategory, EXCLUDED.category, EXCLUDED.typ, EXCLUDED.brand, EXCLUDED.isMixAndMatch, EXCLUDED.isStackable, EXCLUDED.productUnitOfMeasure, EXCLUDED.productUnitOfMeasureToGramsMultiplier, EXCLUDED.productWeight, EXCLUDED.quantity, EXCLUDED.inventoryUnitOfMeasure, EXCLUDED.inventoryUnitOfMeasureToGramsMultiplier, EXCLUDED.locationId, EXCLUDED.locationName, EXCLUDED.currencyCode ) RETURNING productName;`
	const qry = `INSERT INTO cannabinoids (
productId,
name,
lowerRange,
upperRange,
unitOfMeasure,
unitOfMeasureToGramsMultiplier
) VALUES (
$1, $2, $3, $4, $5, $6
)  ON CONFLICT (productId, name) DO UPDATE SET (
lowerRange,
upperRange,
unitOfMeasure,
unitOfMeasureToGramsMultiplier ) = ( 
EXCLUDED.lowerRange,
EXCLUDED.upperRange,
EXCLUDED.unitOfMeasure,
EXCLUDED.unitOfMeasureToGramsMultiplier
) RETURNING productId, name, lowerRange, upperRange, unitOfMeasure, unitOfMeasureToGramsMultiplier;`

	err = table.Pool.DB.QueryRow(qry,
		row.ProductID,
		row.Name,
		row.LowerRange,
		row.UpperRange,
		row.UnitOfMeasure,
		row.UnitOfMeasureToGramsMultiplier,
	).Scan(&newRow.ProductID, &newRow.Name, &newRow.LowerRange,
		&newRow.UpperRange, &newRow.UnitOfMeasure, &newRow.UnitOfMeasureToGramsMultiplier)

	if err != nil {
		err = errors.Wrapf(err, "Couldn't insert row into DB (%s)", spew.Sdump(row))
		return
	} else {
		//fmt.Printf("Cannabinoid %s inserted\n", row.ProductName)
	}

	return
}

func (table *CannabinoidsTable) GetCannabinoidByProductId(productId int) (rows []CannabinoidRow, err error) {
	if productId == 0 {
		err = errors.Errorf("Can't get product row with empty productId")
		return
	}

	const qry = `SELECT  
	productId,
	name,
	lowerRange,
	upperRange,
	unitOfMeasure,
	unitOfMeasureToGramsMultiplier
	FROM cannabinoids WHERE productId = $1`

	iterator, err := table.Pool.DB.Query(qry, productId)

	if err != nil {
		err = errors.Wrapf(err, "Failed to get Cannabinoid (productId = %s)", productId)
		return
	}

	defer iterator.Close()

	for iterator.Next() {
		var row = CannabinoidRow{}

		err = iterator.Scan(
			&row.ProductID,
			&row.Name,
			&row.LowerRange,
			&row.UpperRange,
			&row.UnitOfMeasure,
			&row.UnitOfMeasureToGramsMultiplier,
		)

		if err != nil {
			err = errors.Wrapf(err, "Cannabinoid row scanning failed for product: %s", productId)
			return
		}

		rows = append(rows, row)
	}
	return
}
