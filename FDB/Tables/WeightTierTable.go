package Tables

import (
	"bitbucket.org/laice_/cannaboard/FDB"
	"github.com/davecgh/go-spew/spew"
	"github.com/pkg/errors"
)

type WeightTiersTable struct {
	Pool *FDB.DBPool
}

type WeightTiersTableConfig struct {
	Pool *FDB.DBPool
}

type WeightTierRow struct {
	ProductID                int     `json:"productId"`
	Name                     string  `json:"name"`
	GramAmount               float32 `json:"gramAmount"`
	PricePerUnitInMinorUnits int     `json:"pricePerUnitInMinorUnits"`
}

func WeightTierTable(cfg WeightTiersTableConfig) (table WeightTiersTable, err error) {
	if cfg.Pool == nil {
		err = errors.New("Can't create table without Pool instance")
		return
	}

	var t WeightTiersTable
	t.setPool(cfg.Pool)

	table = t

	if err = table.createTable(); err != nil {
		err = errors.Wrapf(err, "Couldn't create table during init")
		return
	}

	return

}

func (table *WeightTiersTable) setPool(pool *FDB.DBPool) {
	table.Pool = pool
}

func (table *WeightTiersTable) createTable() (err error) {
	const qry = `
	CREATE TABLE IF NOT EXISTS weightTiers (		
		productId int NOT NULL,
		name	text NOT NULL,
		primary key (productId, name),
		gramAmount real,
		pricePerUnitInMinorUnits int
)
`
	_, err = table.Pool.DB.Exec(qry)

	if err != nil {
		err = errors.Wrapf(err, "WeightTier table creation query failed (%s)", qry)
		return
	}

	return

}

func (table *WeightTiersTable) InsertWeightTier(row WeightTierRow) (newRow WeightTierRow, err error) {
	if row.ProductID == 0 {
		err = errors.Errorf("Can't create inventory without product id (%s)", spew.Sdump(row))
		return
	}

	//fmt.Printf("Inserting product %s\n", row.ProductName)

	//const qry = `INSERT INTO weightTiers (
	//	productId,
	//	clientId,
	//	productDescription,
	//	productName, priceInMinorUnits, sku, nutrients, productPictureURL, purchaseCategory, category, typ, brand, isMixAndMatch, isStackable, productUnitOfMeasure, productUnitOfMeasureToGramsMultiplier, productWeight, quantity, inventoryUnitOfMeasure, inventoryUnitOfMeasureToGramsMultiplier, locationId, locationName, currencyCode ) VALUES ( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11
	//				$12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23 )  ON CONFLICT (productId) DO UPDATE SET ( clientId, productDescription, productName, priceInMinorUnits, sku, nutrients, productPictureURL, purchaseCategory, category, typ, brand, isMixAndMatch, isStackable, productUnitOfMeasure, productUnitOfMeasureToGramsMultiplier, productWeight, quantity, inventoryUnitOfMeasure, inventoryUnitOfMeasureToGramsMultiplier, locationId, locationName, currencyCode ) = ( EXCLUDED.clientId, EXCLUDED.productDescription, EXCLUDED.productName, EXCLUDED.priceInMinorUnits, EXCLUDED.sku, EXCLUDED.nutrients, EXCLUDED.productPictureURL, EXCLUDED.purchaseCategory, EXCLUDED.category, EXCLUDED.typ, EXCLUDED.brand, EXCLUDED.isMixAndMatch, EXCLUDED.isStackable, EXCLUDED.productUnitOfMeasure, EXCLUDED.productUnitOfMeasureToGramsMultiplier, EXCLUDED.productWeight, EXCLUDED.quantity, EXCLUDED.inventoryUnitOfMeasure, EXCLUDED.inventoryUnitOfMeasureToGramsMultiplier, EXCLUDED.locationId, EXCLUDED.locationName, EXCLUDED.currencyCode ) RETURNING productName;`
	const qry = `INSERT INTO weightTiers (
productId,
name,
gramAmount,
pricePerUnitInMinorUnits
) VALUES (
$1, $2, $3, $4
)  ON CONFLICT (productId, name) DO UPDATE SET (
gramAmount,
pricePerUnitInMinorUnits
) = ( 
EXCLUDED.gramAmount,
EXCLUDED.pricePerUnitInMinorUnits
) RETURNING productId, name, gramAmount, pricePerUnitInMinorUnits;`

	err = table.Pool.DB.QueryRow(qry,
		row.ProductID,
		row.Name,
		row.GramAmount,
		row.PricePerUnitInMinorUnits,
	).Scan(
		&newRow.ProductID,
		&newRow.Name,
		&newRow.GramAmount,
		&newRow.PricePerUnitInMinorUnits,
	)

	if err != nil {
		err = errors.Wrapf(err, "Couldn't insert row into DB (%s)", spew.Sdump(row))
		return
	} else {
		//fmt.Printf("WeightTier %s inserted\n", row.ProductName)
	}

	return
}

func (table *WeightTiersTable) GetWeightTiersByProductId(productId int) (rows []WeightTierRow, err error) {
	if productId == 0 {
		err = errors.Errorf("Can't get product row with empty productId")
		return
	}

	const qry = `SELECT  
	productId,
	name,
	gramAmount,
	pricePerUnitInMinorUnits
	FROM weightTiers WHERE productId = $1`

	iterator, err := table.Pool.DB.Query(qry, productId)

	if err != nil {
		err = errors.Wrapf(err, "Failed to get WeightTier (productId = %s)", productId)
		return
	}

	defer iterator.Close()

	for iterator.Next() {
		var row = WeightTierRow{}

		err = iterator.Scan(
			&row.ProductID,
			&row.Name,
			&row.GramAmount,
			&row.PricePerUnitInMinorUnits,
		)

		if err != nil {
			err = errors.Wrapf(err, "WeightTier row scanning failed for product: %s", productId)
			return
		}

		rows = append(rows, row)
	}
	return
}
