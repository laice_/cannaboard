package Console

import (
	"errors"
	"fmt"
	"github.com/marcusolsson/tui-go"
	"strconv"
	"strings"
	"time"
)

type CommandList struct {
	Commands []Command  `json:"commands"`
	History  *tui.Box   `json:"history"`
	Input    *tui.Entry `json:"input"`
}

type Command struct {
	Verb   string      `json:"verb"`
	Action func([]Arg) `json:"action"`
}

type Arg struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

func InitConsole() *CommandList {
	history := tui.NewVBox()
	history.SetSizePolicy(tui.Expanding, tui.Maximum)

	historyScroll := tui.NewScrollArea(history)
	historyScroll.SetAutoscrollToBottom(true)

	historyBox := tui.NewVBox(historyScroll)
	historyBox.SetBorder(true)

	input := tui.NewEntry()
	input.SetFocused(true)
	input.SetSizePolicy(tui.Expanding, tui.Maximum)

	inputBox := tui.NewHBox(input)
	inputBox.SetBorder(true)
	inputBox.SetSizePolicy(tui.Expanding, tui.Maximum)

	box := tui.NewVBox(
		tui.NewLabel("flowhub fetch"),
		historyBox,
		inputBox,
	)

	cl := NewCommandList()
	cl.History = history
	cl.Input = input
	//cl.InitCommands()

	input.OnSubmit(cl.HandleCommand)

	ui, err := tui.New(box)
	if err != nil {
		panic(err)
	}
	ui.SetKeybinding("Esc", func() { ui.Quit() })

	if err := ui.Run(); err != nil {
		panic(err)
	}

	return cl
}

func (cl *CommandList) HandleArgs(rcv string) (cmd string, args []Arg) {

	txt := strings.Split(rcv, " ")

	//fmt.Printf("Handle Args: %s\n", txt)

	cmd, a := txt[0], txt[1:]

	//fmt.Printf("Handle Args2: %s\n", cmd)

	for i := 0; i < len(a); i = i + 2 {
		key := a[i]
		val := a[i+1]
		args = append(args, Arg{
			Key:   key,
			Value: val,
		})
	}

	return cmd, args
}

func (cl *CommandList) HandleCommand(e *tui.Entry) {
	txt := e.Text()

	cl.AppendHistory(txt)

	cmd, args := cl.HandleArgs(txt)
	//fmt.Printf("Handle command: %v", cmd)
	cl.Execute(cmd, args)

	cl.Input.SetText("")
}

func (cl *CommandList) AppendHistory(text string) {
	cl.History.Append(tui.NewHBox(
		tui.NewLabel(time.Now().Format("15:04")),
		tui.NewPadder(1, 0, tui.NewLabel("> ")),
		tui.NewLabel(text),
		tui.NewSpacer(),
	))
}

func (cl *CommandList) Log(text string) {
	cl.AppendHistory(text)
}

//func (cl *CommandList)InitCommands() {
//	cl.NewCommand("locations",  func(args []string){
//
//	})
//}

func NewCommandList() *CommandList {
	return &CommandList{}
}

func (cl *CommandList) NewCommand(verb string, action func([]Arg)) *Command {
	//fmt.Printf("Creating command %s\n", verb)
	cl.Log(fmt.Sprintf("Creating command %s", verb))

	cmd := &Command{
		Verb:   verb,
		Action: action,
	}
	cl.Commands = append(cl.Commands, *cmd)

	return cmd
}

func (cl *CommandList) findCommand(verb string) (cmd Command, err error) {
	cl.Log(strconv.Itoa(len(cl.Commands)))
	for i, cmd := range cl.Commands {
		//fmt.Printf(cmd.Verb)
		cl.Log(cmd.Verb)
		if cmd.Verb == verb {
			err = nil
			return cl.Commands[i], err
		}
	}

	return Command{}, errors.New(fmt.Sprintf("command %s not found", verb))
}

func (cl *CommandList) Execute(verb string, args []Arg) {
	//fmt.Printf("Execute %s command list\n", verb)
	cmd, err := cl.findCommand(verb)
	if err != nil {
		cl.AppendHistory(err.Error())
		return
	}
	cmd.Execute(args)

}

func (c *Command) Execute(args []Arg) {
	//fmt.Printf("Execute command: %s\n", c.Verb)
	c.Action(args)
}
