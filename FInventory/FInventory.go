package FInventory

import (
	"bitbucket.org/laice_/cannaboard/FRequest"
	"encoding/json"
)

//######################
//
//	Inventory
//
//**********************

type FWeightTierInformation struct {
	Name                     string  `json:"name"`
	GramAmount               float32 `json:"gramAmount"`
	PricePerUnitInMinorUnits int     `json:"pricePerUnitInMinorUnits"`
}

type FCannabinoidInformation struct {
	Name                           string  `json:"name"`
	LowerRange                     float32 `json:"lowerRange"`
	UpperRange                     float32 `json:"upperRange"`
	UnitOfMeasure                  string  `json:"unitOfMeasure"`
	UnitOfMeasureToGramsMultiplier float32 `json:"unitOfMeasureToGramsMultiplier"`
}

type FProduct struct {
	ProductID                               int                       `json:"productId"`
	ClientID                                int                       `json:"clientId"`
	ProductDescription                      string                    `json:"productDescription"`
	ProductName                             string                    `json:"productName"`
	PriceInMinorUnits                       int                       `json:"priceInMinorUnits"`
	SKU                                     string                    `json:"sku"`
	Nutrients                               string                    `json:"nutrients"`
	ProductPictureURL                       string                    `json:"productPictureURL"`
	PurchaseCategory                        string                    `json:"purchaseCategory"`
	Category                                string                    `json:"category"`
	Type                                    string                    `json:"type"`
	Brand                                   string                    `json:"brand"`
	IsMixAndMatch                           bool                      `json:"isMixAndMatch"`
	IsStackable                             bool                      `json:"isStackable"`
	ProductUnitOfMeasure                    string                    `json:"productUnitOfMeasure"`
	ProductUnitOfMeasureToGramsMultiplier   string                    `json:"productUnitOfMeasureToGramsMultiplier"`
	ProductWeight                           float32                   `json:"productWeight"`
	WeightTierInformation                   []FWeightTierInformation  `json:"weightTierInformation"`
	CannabinoidInformation                  []FCannabinoidInformation `json:"cannabinoidInformation"`
	Quantity                                float32                   `json:"quantity"`
	InventoryUnitOfMeasure                  string                    `json:"inventoryUnitOfMeasure"`
	InventoryUnitOfMeasureToGramsMultiplier float32                   `json:"inventoryUnitOfMeasureToGramsMultiplier"`
	LocationID                              int                       `json:"locationId"`
	LocationName                            string                    `json:"locationName"`
	CurrencyCode                            string                    `json:"currencyCode"`
}

type FLocationInventory struct {
	Products []FProduct `json:"products"`
}

func (loc *FProduct) String() string {

	location, err := json.Marshal(loc)

	if err != nil {
		panic(err)
	}

	return string(location)
}

func (ci *FCannabinoidInformation) String() string {
	c, err := json.Marshal(ci)
	if err != nil {
		panic(err)
	}

	return string(c)
}

func (wi *FWeightTierInformation) String() string {
	w, err := json.Marshal(wi)
	if err != nil {
		panic(err)
	}
	return string(w)
}

func (loc *FProduct) Update(fReq *FRequest.FRequest) {

}

/*
func (loc *FInventory) getSales(fReq *FRequest.FRequest) {

	fReq.Request("/v0/locations/"+string(loc.InventoryID)+"/sales", func(res http.Response) {
		var fRes FResponse.FSalesResponse
		fRes.Parse(res.Body, func(fRes *FResponse.FSalesResponse) {
			loc.Sales = fRes.Data

			fmt.Println("Sales built")

		})

	})

}
*/
