package Stats

import (
	"bitbucket.org/laice_/cannaboard/FConfig"
	"bitbucket.org/laice_/cannaboard/FDB"
	"bitbucket.org/laice_/cannaboard/FDB/Tables"
	"bitbucket.org/laice_/cannaboard/lib"
	"encoding/json"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"sync"
	"time"
)

func ProcessLocation(loc Tables.LocationRow, db *FDB.DBPool) {
	fmt.Printf("Processing location: %d\n", loc.LocationID)
	orderTable, err := Tables.OrderTable(Tables.OrdersTableConfig{
		Pool: db,
	})

	if err != nil {
		panic(fmt.Sprintf("Failed to load order table:\n%s\n", err.Error()))
	}

	_, err = orderTable.Pool.DB.Exec(`SET TIME ZONE 'America/Denver';`)

	if err != nil {
		err = errors.Errorf("Error setting DB time zone:\n%s\n", err.Error())
		return
	}

	//orders, err := orderTable.GetOrdersByLocationId(order.LocationID)

	//yesterday := time.Now().AddDate(0, 0, -1).Format(time.RFC3339)
	//tomorrow := time.Now().AddDate(0, 0, 1).Format(time.RFC3339)
	//
	//var orders []Tables.OrderRow
	//if len(os.Args) > 1 {
	//
	//	switch os.Args[1] {
	//	case "all":
	//		orders, err = orderTable.GetOrdersByLocationId(loc.ImportID)
	//		break
	//	default:
	//		orders, err = orderTable.GetOrdersByLocationIdBetweenDates(loc.ImportID, yesterday, tomorrow, 0)
	//		break
	//	}
	//} else {
	//	orders, err = orderTable.GetOrdersByLocationIdBetweenDates(loc.ImportID, yesterday, tomorrow, 0)
	//}

	if err != nil {
		panic(fmt.Sprintf("Failed to get all orders:\n%s\n", err.Error()))
	}

	//fmt.Printf("Got orders: %v\n", orders)

	lib.C(err)

	//var itemCounts []Tables.ItemCountRow

}

func ProcessOrders(orders []Tables.OrderRow, db *FDB.DBPool) {

	cartTable, err := Tables.CartItemTable(Tables.CartItemsTableConfig{
		Pool: db,
	})

	if err != nil {
		panic(fmt.Sprintf("Failed to load order table:\n%s\n", err.Error()))
	}

	totalTable, err := Tables.OrderTotalTable(Tables.OrderTotalsTableConfig{
		Pool: db,
	})

	if err != nil {
		panic(fmt.Sprintf("Failed to load order table:\n%s\n", err.Error()))
	}

	tz, err := time.LoadLocation("America/Denver")

	if err != nil {
		err = errors.Errorf("Error creating timezone")
		return
	}

	var saleDays []Tables.SalesDayRow

	var categoryCount []Tables.ItemCountRow
	var skuCount []Tables.ItemCountRow
	var productNameCount []Tables.ItemCountRow
	var vendorCount []Tables.ItemCountRow
	var strainCount []Tables.ItemCountRow
	var unitCount []Tables.ItemCountRow

	itemCountTable, err := Tables.NewItemCountTable(Tables.ItemCountTableConfig{
		Pool: db,
	})

	lib.C(err)
	monthlyItemCountTable, err := Tables.NewMonthlyItemCountTable(Tables.MonthlyItemCountTableConfig{
		Pool: db,
	})

	lib.C(err)

	weeklyItemCountTable, err := Tables.NewWeeklyItemCountTable(Tables.WeeklyItemCountTableConfig{
		Pool: db,
	})

	lib.C(err)

	dailyItemCountTable, err := Tables.NewDailyItemCountTable(Tables.DailyItemCountTableConfig{
		Pool: db,
	})

	lib.C(err)

	yearlyItemCountTable, err := Tables.NewYearlyItemCountTable(Tables.YearlyItemCountTableConfig{
		Pool: db,
	})

	for _, order := range orders {
		created, err := time.ParseInLocation(time.RFC3339, order.CreatedAt, tz)
		created = created.Add(-7 * time.Hour)

		if err != nil {
			err = errors.Errorf("Failed to parse date as RFC3339: %s", order.CreatedAt)
			return
		}

		//day := created.Format("01-02-2006")
		//hour := created.Format("15:00:00")

		//fmt.Printf("Day: %s Hour: %s\n", day, hour)
		exists := -1

		for i, d := range saleDays {
			if d.CreatedAt == created.Format("2006-01-02T00:00:00Z07:00") {
				saleDays[i].CustomerCount++
				exists = i

			}
		}

		if exists == -1 {
			saleDays = append(saleDays, Tables.SalesDayRow{
				CreatedAt:     created.Format("2006-01-02T00:00:00Z07:00"),
				CustomerCount: 1,
				ImportId:      order.LocationID,
				SalesTotal:    decimal.NewFromFloat(0),
			})
			exists = len(saleDays) - 1
		}

		items, err := cartTable.GetCartItemsByOrderId(order.ID)

		if err != nil {
			panic(fmt.Sprintf("Failed to get cart items:\n%s\n", err.Error()))
		}

		totals, err := totalTable.GetOrderTotalsByOrderId(order.ID)

		if err != nil {
			panic(fmt.Sprintf("Failed to get totals:\n%s\n", err.Error()))
		}

		itemCount := len(items)

		hTime := created.Format("2006-01-02T15:00:00Z07:00")
		year, week := created.ISOWeek()
		day := created.Format("01-02-2006")
		month := created.Format("01")

		hourlyItemCounts := make(map[string]Tables.ItemCountRow)
		dailyItemCounts := make(map[string]Tables.ItemCountRow)
		weeklyItemCounts := make(map[string]Tables.ItemCountRow)
		monthlyItemCounts := make(map[string]Tables.ItemCountRow)
		yearlyItemCounts := make(map[string]Tables.ItemCountRow)

		for _, item := range items {

			// hourly item counts

			catUID := fmt.Sprintf("%s-%s-%s-%s", "Category", item.Category, hTime, order.LocationID)

			if val, ok := hourlyItemCounts[catUID]; ok {
				hourlyItemCounts[catUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				hourlyItemCounts[catUID] = Tables.ItemCountRow{
					ItemType:   "Category",
					ItemName:   item.Category,
					CreatedAt:  hTime,
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			skuUID := fmt.Sprintf("%s-%s-%s-%s", "SKU", item.SKU, hTime, order.LocationID)
			if val, ok := hourlyItemCounts[skuUID]; ok {
				hourlyItemCounts[skuUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				hourlyItemCounts[skuUID] = Tables.ItemCountRow{
					ItemType:   "SKU",
					ItemName:   item.SKU,
					CreatedAt:  hTime,
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			pnUID := fmt.Sprintf("%s-%s-%s-%s", "ProductName", item.Title1, hTime, order.LocationID)

			if val, ok := hourlyItemCounts[pnUID]; ok {
				hourlyItemCounts[pnUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				hourlyItemCounts[pnUID] = Tables.ItemCountRow{
					ItemType:   "ProductName",
					ItemName:   item.Title1,
					CreatedAt:  hTime,
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			vUID := fmt.Sprintf("%s-%s-%s-%s", "Vendor", item.Title2, hTime, order.LocationID)
			if val, ok := hourlyItemCounts[vUID]; ok {
				hourlyItemCounts[vUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				hourlyItemCounts[vUID] = Tables.ItemCountRow{
					ItemType:   "Vendor",
					ItemName:   item.Title2,
					CreatedAt:  hTime,
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			sUID := fmt.Sprintf("%s-%s-%s-%s", "Strain", item.Strain, hTime, order.LocationID)

			if val, ok := hourlyItemCounts[sUID]; ok {
				hourlyItemCounts[sUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				hourlyItemCounts[sUID] = Tables.ItemCountRow{
					ItemType:   "Strain",
					ItemName:   item.Strain,
					CreatedAt:  hTime,
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			uUID := fmt.Sprintf("%s-%s-%s-%s", "Unit", item.UnitOfWeight, hTime, order.LocationID)
			if val, ok := hourlyItemCounts[uUID]; ok {
				hourlyItemCounts[uUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				hourlyItemCounts[uUID] = Tables.ItemCountRow{
					ItemType:   "Unit",
					ItemName:   item.UnitOfWeight,
					CreatedAt:  hTime,
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			// daily item counts

			if val, ok := dailyItemCounts[catUID]; ok {
				dailyItemCounts[catUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				dailyItemCounts[catUID] = Tables.ItemCountRow{
					ItemType:   "Category",
					ItemName:   item.Category,
					CreatedAt:  day,
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := dailyItemCounts[skuUID]; ok {
				dailyItemCounts[skuUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				dailyItemCounts[skuUID] = Tables.ItemCountRow{
					ItemType:   "SKU",
					ItemName:   item.SKU,
					CreatedAt:  day,
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := dailyItemCounts[pnUID]; ok {
				dailyItemCounts[pnUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				dailyItemCounts[pnUID] = Tables.ItemCountRow{
					ItemType:   "ProductName",
					ItemName:   item.Title1,
					CreatedAt:  day,
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := dailyItemCounts[vUID]; ok {
				dailyItemCounts[vUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				dailyItemCounts[vUID] = Tables.ItemCountRow{
					ItemType:   "Vendor",
					ItemName:   item.Title2,
					CreatedAt:  day,
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := dailyItemCounts[sUID]; ok {
				dailyItemCounts[sUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				dailyItemCounts[sUID] = Tables.ItemCountRow{
					ItemType:   "Strain",
					ItemName:   item.Strain,
					CreatedAt:  day,
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := dailyItemCounts[uUID]; ok {
				dailyItemCounts[uUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				dailyItemCounts[uUID] = Tables.ItemCountRow{
					ItemType:   "Unit",
					ItemName:   item.UnitOfWeight,
					CreatedAt:  day,
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			// weekly item counts

			if val, ok := weeklyItemCounts[catUID]; ok {
				weeklyItemCounts[catUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				weeklyItemCounts[catUID] = Tables.ItemCountRow{
					ItemType:   "Category",
					ItemName:   item.Category,
					CreatedAt:  fmt.Sprintf("%d-%d", year, week),
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := weeklyItemCounts[skuUID]; ok {
				weeklyItemCounts[skuUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				weeklyItemCounts[skuUID] = Tables.ItemCountRow{
					ItemType:   "SKU",
					ItemName:   item.SKU,
					CreatedAt:  fmt.Sprintf("%d-%d", year, week),
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := weeklyItemCounts[pnUID]; ok {
				weeklyItemCounts[pnUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				weeklyItemCounts[pnUID] = Tables.ItemCountRow{
					ItemType:   "ProductName",
					ItemName:   item.Title1,
					CreatedAt:  fmt.Sprintf("%d-%d", year, week),
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := weeklyItemCounts[vUID]; ok {
				weeklyItemCounts[vUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				weeklyItemCounts[vUID] = Tables.ItemCountRow{
					ItemType:   "Vendor",
					ItemName:   item.Title2,
					CreatedAt:  fmt.Sprintf("%d-%d", year, week),
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := weeklyItemCounts[sUID]; ok {
				weeklyItemCounts[sUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				weeklyItemCounts[sUID] = Tables.ItemCountRow{
					ItemType:   "Strain",
					ItemName:   item.Strain,
					CreatedAt:  fmt.Sprintf("%d-%d", year, week),
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := weeklyItemCounts[uUID]; ok {
				weeklyItemCounts[uUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				weeklyItemCounts[uUID] = Tables.ItemCountRow{
					ItemType:   "Unit",
					ItemName:   item.UnitOfWeight,
					CreatedAt:  fmt.Sprintf("%d-%d", year, week),
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			// monthly item counts

			if val, ok := monthlyItemCounts[catUID]; ok {
				monthlyItemCounts[catUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				monthlyItemCounts[catUID] = Tables.ItemCountRow{
					ItemType:   "Category",
					ItemName:   item.Category,
					CreatedAt:  fmt.Sprintf("%d-%s", year, month),
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := monthlyItemCounts[skuUID]; ok {
				monthlyItemCounts[skuUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				monthlyItemCounts[skuUID] = Tables.ItemCountRow{
					ItemType:   "SKU",
					ItemName:   item.SKU,
					CreatedAt:  fmt.Sprintf("%d-%s", year, month),
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := monthlyItemCounts[pnUID]; ok {
				monthlyItemCounts[pnUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				monthlyItemCounts[pnUID] = Tables.ItemCountRow{
					ItemType:   "ProductName",
					ItemName:   item.Title1,
					CreatedAt:  fmt.Sprintf("%d-%s", year, month),
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := monthlyItemCounts[vUID]; ok {
				monthlyItemCounts[vUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				monthlyItemCounts[vUID] = Tables.ItemCountRow{
					ItemType:   "Vendor",
					ItemName:   item.Title2,
					CreatedAt:  fmt.Sprintf("%d-%s", year, month),
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := monthlyItemCounts[sUID]; ok {
				monthlyItemCounts[sUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				monthlyItemCounts[sUID] = Tables.ItemCountRow{
					ItemType:   "Strain",
					ItemName:   item.Strain,
					CreatedAt:  fmt.Sprintf("%d-%s", year, month),
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := monthlyItemCounts[uUID]; ok {
				monthlyItemCounts[uUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				monthlyItemCounts[uUID] = Tables.ItemCountRow{
					ItemType:   "Unit",
					ItemName:   item.UnitOfWeight,
					CreatedAt:  fmt.Sprintf("%d-%s", year, month),
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			// yearly item counts

			if val, ok := yearlyItemCounts[catUID]; ok {
				yearlyItemCounts[catUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				yearlyItemCounts[catUID] = Tables.ItemCountRow{
					ItemType:   "Category",
					ItemName:   item.Category,
					CreatedAt:  fmt.Sprintf("%d", year),
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := yearlyItemCounts[skuUID]; ok {
				yearlyItemCounts[skuUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				yearlyItemCounts[skuUID] = Tables.ItemCountRow{
					ItemType:   "SKU",
					ItemName:   item.SKU,
					CreatedAt:  fmt.Sprintf("%d", year),
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := yearlyItemCounts[pnUID]; ok {
				yearlyItemCounts[pnUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				yearlyItemCounts[pnUID] = Tables.ItemCountRow{
					ItemType:   "ProductName",
					ItemName:   item.Title1,
					CreatedAt:  fmt.Sprintf("%d", year),
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := yearlyItemCounts[vUID]; ok {
				yearlyItemCounts[vUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				yearlyItemCounts[vUID] = Tables.ItemCountRow{
					ItemType:   "Vendor",
					ItemName:   item.Title2,
					CreatedAt:  fmt.Sprintf("%d", year),
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := yearlyItemCounts[sUID]; ok {
				yearlyItemCounts[sUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				yearlyItemCounts[sUID] = Tables.ItemCountRow{
					ItemType:   "Strain",
					ItemName:   item.Strain,
					CreatedAt:  fmt.Sprintf("%d", year),
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			if val, ok := yearlyItemCounts[uUID]; ok {
				yearlyItemCounts[uUID] = Tables.ItemCountRow{
					ItemType:   val.ItemType,
					ItemName:   val.ItemName,
					CreatedAt:  val.CreatedAt,
					ImportId:   val.ImportId,
					TotalItems: val.TotalItems + 1,
				}
			} else {
				yearlyItemCounts[uUID] = Tables.ItemCountRow{
					ItemType:   "Unit",
					ItemName:   item.UnitOfWeight,
					CreatedAt:  fmt.Sprintf("%d", year),
					ImportId:   order.LocationID,
					TotalItems: 1,
				}
			}

			//// daily item counts
			//
			//lib.CE(dailyItemCountTable.InsertDailyItemCount(Tables.DailyItemCountRow{
			//	ItemType:   "Category",
			//	ItemName:   item.Category,
			//	CreatedAt:  day,
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(dailyItemCountTable.InsertDailyItemCount(Tables.DailyItemCountRow{
			//	ItemType:   "SKU",
			//	ItemName:   item.SKU,
			//	CreatedAt:  day,
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(dailyItemCountTable.InsertDailyItemCount(Tables.DailyItemCountRow{
			//	ItemType:   "ProductName",
			//	ItemName:   item.Title1,
			//	CreatedAt:  day,
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(dailyItemCountTable.InsertDailyItemCount(Tables.DailyItemCountRow{
			//	ItemType:   "Vendor",
			//	ItemName:   item.Title2,
			//	CreatedAt:  day,
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(dailyItemCountTable.InsertDailyItemCount(Tables.DailyItemCountRow{
			//	ItemType:   "Strain",
			//	ItemName:   item.Strain,
			//	CreatedAt:  day,
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(dailyItemCountTable.InsertDailyItemCount(Tables.DailyItemCountRow{
			//	ItemType:   "Unit",
			//	ItemName:   item.UnitOfWeight,
			//	CreatedAt:  day,
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//// weekly item counts
			//
			//lib.CE(weeklyItemCountTable.InsertWeeklyItemCount(Tables.WeeklyItemCountRow{
			//	ItemType:   "Category",
			//	ItemName:   item.Category,
			//	CreatedAt:  fmt.Sprintf("%d-%d", year, week),
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(weeklyItemCountTable.InsertWeeklyItemCount(Tables.WeeklyItemCountRow{
			//	ItemType:   "SKU",
			//	ItemName:   item.SKU,
			//	CreatedAt:  fmt.Sprintf("%d-%d", year, week),
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(weeklyItemCountTable.InsertWeeklyItemCount(Tables.WeeklyItemCountRow{
			//	ItemType:   "ProductName",
			//	ItemName:   item.Title1,
			//	CreatedAt:  fmt.Sprintf("%d-%d", year, week),
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(weeklyItemCountTable.InsertWeeklyItemCount(Tables.WeeklyItemCountRow{
			//	ItemType:   "Vendor",
			//	ItemName:   item.Title2,
			//	CreatedAt:  fmt.Sprintf("%d-%d", year, week),
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(weeklyItemCountTable.InsertWeeklyItemCount(Tables.WeeklyItemCountRow{
			//	ItemType:   "Strain",
			//	ItemName:   item.Strain,
			//	CreatedAt:  fmt.Sprintf("%d-%d", year, week),
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(weeklyItemCountTable.InsertWeeklyItemCount(Tables.WeeklyItemCountRow{
			//	ItemType:   "Unit",
			//	ItemName:   item.UnitOfWeight,
			//	CreatedAt:  fmt.Sprintf("%d-%d", year, week),
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//// monthly item count
			//
			//lib.CE(monthlyItemCountTable.InsertMonthlyItemCount(Tables.MonthlyItemCountRow{
			//	ItemType:   "Category",
			//	ItemName:   item.Category,
			//	CreatedAt:  fmt.Sprintf("%d-%s", year, month),
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(monthlyItemCountTable.InsertMonthlyItemCount(Tables.MonthlyItemCountRow{
			//	ItemType:   "SKU",
			//	ItemName:   item.SKU,
			//	CreatedAt:  fmt.Sprintf("%d-%s", year, month),
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(monthlyItemCountTable.InsertMonthlyItemCount(Tables.MonthlyItemCountRow{
			//	ItemType:   "ProductName",
			//	ItemName:   item.Title1,
			//	CreatedAt:  fmt.Sprintf("%d-%s", year, month),
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(monthlyItemCountTable.InsertMonthlyItemCount(Tables.MonthlyItemCountRow{
			//	ItemType:   "Vendor",
			//	ItemName:   item.Title2,
			//	CreatedAt:  fmt.Sprintf("%d-%s", year, month),
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(monthlyItemCountTable.InsertMonthlyItemCount(Tables.MonthlyItemCountRow{
			//	ItemType:   "Strain",
			//	ItemName:   item.Strain,
			//	CreatedAt:  fmt.Sprintf("%d-%s", year, month),
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(monthlyItemCountTable.InsertMonthlyItemCount(Tables.MonthlyItemCountRow{
			//	ItemType:   "Unit",
			//	ItemName:   item.UnitOfWeight,
			//	CreatedAt:  fmt.Sprintf("%d-%s", year, month),
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//// yearly item count
			//
			//lib.CE(yearlyItemCountTable.InsertYearlyItemCount(Tables.YearlyItemCountRow{
			//	ItemType:   "Category",
			//	ItemName:   item.Category,
			//	CreatedAt:  fmt.Sprintf("%d", year),
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(yearlyItemCountTable.InsertYearlyItemCount(Tables.YearlyItemCountRow{
			//	ItemType:   "SKU",
			//	ItemName:   item.SKU,
			//	CreatedAt:  fmt.Sprintf("%d", year),
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(yearlyItemCountTable.InsertYearlyItemCount(Tables.YearlyItemCountRow{
			//	ItemType:   "ProductName",
			//	ItemName:   item.Title1,
			//	CreatedAt:  fmt.Sprintf("%d", year),
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(yearlyItemCountTable.InsertYearlyItemCount(Tables.YearlyItemCountRow{
			//	ItemType:   "Vendor",
			//	ItemName:   item.Title2,
			//	CreatedAt:  fmt.Sprintf("%d", year),
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(yearlyItemCountTable.InsertYearlyItemCount(Tables.YearlyItemCountRow{
			//	ItemType:   "Strain",
			//	ItemName:   item.Strain,
			//	CreatedAt:  fmt.Sprintf("%d", year),
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))
			//
			//lib.CE(yearlyItemCountTable.InsertYearlyItemCount(Tables.YearlyItemCountRow{
			//	ItemType:   "Unit",
			//	ItemName:   item.UnitOfWeight,
			//	CreatedAt:  fmt.Sprintf("%d", year),
			//	ImportId:   order.LocationID,
			//	TotalItems: 1,
			//}))

			//catFound := false
			////fmt.Println("Item: ", item)
			//for i, cat := range categoryCount {
			//	if cat.Type == "Category" && cat.Name == item.Category &&
			//		cat.CreatedAt == hTime && cat.ImportId == order.LocationID {
			//		//		fmt.Println("Match Found")
			//		categoryCount[i].Count = cat.Count + 1
			//		catFound = true
			//	}
			//}
			//if !catFound {
			//	fmt.Println("Match not found")
			//	categoryCount = append(categoryCount, Tables.ItemCountRow{
			//		Type:      "Category",
			//		Name:      item.Category,
			//		CreatedAt: hTime,
			//		ImportId:  order.LocationID,
			//		Count:     1,
			//	})
			//}
			//
			//skuFound := false
			//
			//for i, it := range skuCount {
			//	if it.Type == "SKU" && it.Name == item.SKU &&
			//		it.CreatedAt == hTime && it.ImportId == order.LocationID {
			//		skuCount[i].Count = it.Count + 1
			//		skuFound = true
			//	}
			//}
			//if !skuFound {
			//	skuCount = append(skuCount, Tables.ItemCountRow{
			//		Type:      "SKU",
			//		Name:      item.SKU,
			//		CreatedAt: hTime,
			//		ImportId:  order.LocationID,
			//		Count:     1,
			//	})
			//}
			//
			//pnFound := false
			//
			//for i, it := range productNameCount {
			//	if it.Type == "ProductName" && it.Name == item.Title1 &&
			//		it.CreatedAt == hTime && it.ImportId == order.LocationID {
			//		productNameCount[i].Count = it.Count + 1
			//		pnFound = true
			//	}
			//}
			//if !pnFound {
			//	productNameCount = append(productNameCount, Tables.ItemCountRow{
			//		Type:      "ProductName",
			//		Name:      item.Title1,
			//		CreatedAt: hTime,
			//		ImportId:  order.LocationID,
			//		Count:     1,
			//	})
			//}
			//
			//vendorFound := false
			//
			//for i, it := range vendorCount {
			//	if it.Type == "Vendor" && it.Name == item.Title2 &&
			//		it.CreatedAt == hTime && it.ImportId == order.LocationID {
			//		vendorCount[i].Count = it.Count + 1
			//		vendorFound = true
			//	}
			//}
			//if !vendorFound {
			//	vendorCount = append(vendorCount, Tables.ItemCountRow{
			//		Type:      "Vendor",
			//		Name:      item.Title2,
			//		CreatedAt: hTime,
			//		ImportId:  order.LocationID,
			//		Count:     1,
			//	})
			//}
			//
			//strainFound := false
			//
			//for i, it := range strainCount {
			//	if it.Type == "Strain" && it.Name == item.Strain &&
			//		it.CreatedAt == hTime && it.ImportId == order.LocationID {
			//		strainCount[i].Count = it.Count + 1
			//		strainFound = true
			//	}
			//}
			//if !strainFound {
			//	strainCount = append(strainCount, Tables.ItemCountRow{
			//		Type:      "Strain",
			//		Name:      item.Strain,
			//		CreatedAt: hTime,
			//		ImportId:  order.LocationID,
			//		Count:     1,
			//	})
			//}
			//
			//unitFound := false
			//
			//for i, it := range unitCount {
			//	if it.Type == "Unit" && it.Name == item.UnitOfWeight &&
			//		it.CreatedAt == hTime && it.ImportId == order.LocationID {
			//		unitCount[i].Count = it.Count + 1
			//		unitFound = true
			//	}
			//}
			//if !unitFound {
			//	unitCount = append(unitCount, Tables.ItemCountRow{
			//		Type:      "Unit",
			//		Name:      item.UnitOfWeight,
			//		CreatedAt: hTime,
			//		ImportId:  order.LocationID,
			//		Count:     1,
			//	})
			//}

		}

		for _, v := range hourlyItemCounts {
			lib.CE(itemCountTable.InsertItemCount(v))
		}

		for _, v := range dailyItemCounts {
			lib.CE(dailyItemCountTable.InsertItemCount(v))
		}

		for _, v := range weeklyItemCounts {
			lib.CE(weeklyItemCountTable.InsertItemCount(v))
		}

		for _, v := range monthlyItemCounts {
			lib.CE(monthlyItemCountTable.InsertItemCount(v))
		}
		for _, v := range yearlyItemCounts {
			lib.CE(yearlyItemCountTable.InsertItemCount(v))
		}

		saleDays[exists].ItemsSold += itemCount
		var thisTot float32
		thisTot = 0

		for _, t := range totals {
			//saleDays[exists].SalesTotal.Add(decimal.NewFromFloat32(t.FinalTotal))
			thisTot += t.FinalTotal
		}

		saleDays[exists].SalesTotal = saleDays[exists].SalesTotal.Add(decimal.NewFromFloat32(thisTot))

		hourExists := -1

		for i, h := range saleDays[exists].Hours {
			if h.CreatedAt == created.Format("2006-01-02T15:00:00Z07:00") {
				hourExists = i
				saleDays[exists].Hours[i].CustomerCount++
				break
			}
		}

		if hourExists == -1 {

			saleDays[exists].Hours = append(saleDays[exists].Hours, Tables.SalesHourRow{
				CreatedAt:     created.Format("2006-01-02T15:00:00Z07:00"),
				CustomerCount: 1,
				SalesTotal:    decimal.NewFromFloat32(thisTot),
				ItemsSold:     itemCount,
				ImportId:      order.LocationID,
			})
		} else {
			saleDays[exists].Hours[hourExists].SalesTotal = saleDays[exists].Hours[hourExists].SalesTotal.Add(decimal.NewFromFloat32(thisTot))
			saleDays[exists].Hours[hourExists].ItemsSold += itemCount

		}

	}

	for _, it := range categoryCount {
		lib.CE(itemCountTable.InsertItemCount(it))
	}
	for _, it := range skuCount {
		lib.CE(itemCountTable.InsertItemCount(it))
	}
	for _, it := range productNameCount {
		lib.CE(itemCountTable.InsertItemCount(it))
	}
	for _, it := range vendorCount {
		lib.CE(itemCountTable.InsertItemCount(it))
	}
	for _, it := range strainCount {
		lib.CE(itemCountTable.InsertItemCount(it))
	}
	for _, it := range unitCount {
		lib.CE(itemCountTable.InsertItemCount(it))
	}

	fmt.Printf(spew.Sprintf("saleDays: %v\n", saleDays))

	saleDayTable, err := Tables.SaleDayTable(Tables.SalesDayTableConfig{
		Pool: db,
	})

	if err != nil {
		panic(err.Error())
	}

	saleHourTable, err := Tables.SaleHourTable(Tables.SalesHourTableConfig{
		Pool: db,
	})

	if err != nil {
		panic(err.Error())
	}

	for _, day := range saleDays {
		_, err := saleDayTable.InsertSalesDay(day)

		if err != nil {
			panic(err.Error())
		}

		for _, hour := range day.Hours {
			_, err := saleHourTable.InsertSaleHour(hour)

			if err != nil {
				panic(err.Error())
			}
		}
	}
}

func Generate() {
	sc, dbConfig := LoadConfiguration()

	db, err := FDB.New(dbConfig)
	lib.C(err)
	//orderTable, err := Tables.OrderTable(Tables.OrdersTableConfig{
	//	Pool: db,
	//})
	//lib.C(err)
	//tz, err := time.LoadLocation("America/Denver")
	//
	//lib.C(err)
	//
	//yes, err := time.ParseInLocation("01-02-2006", "01-08-2019", tz)
	//
	//lib.C(err)
	//
	//y := yes.Format(time.RFC3339)

	//orders, err :=	orderTable.GetOrdersByLocationIdBetweenDates(

	//	"Q2kMvxBHw5BF4acj3", y, "", 0)

	locTable, err := Tables.LocationTable(Tables.LocationsTableConfig{
		Pool: db,
	})
	lib.C(err)

	locs, err := locTable.GetAllLocations(sc.Filter)
	lib.C(err)

	var wg sync.WaitGroup
	wg.Add(len(locs))
	yes := time.Now().AddDate(0, 0, -2).Format("01-02-2006")
	tod := time.Now().AddDate(0, 0, 1).Format("01-02-2006")
	if len(os.Args) > 1 {
		switch os.Args[1] {
		case "c":
			yes = os.Args[2]
			tod = os.Args[3]

			if yes == "" || tod == "" {
				fmt.Printf("CreatedAfter: %s, CreatedBefore: %s", yes, tod)
				return
			}

		}
	}

	for _, loc := range locs {

		qry := fmt.Sprintf("SELECT * FROM orders where locationid='%s' AND createdat BETWEEN '%s 13:00:00' AND '%s 05:00:00';", loc.ImportID, yes, tod)

		fmt.Println(qry)
		iterator, err := db.DB.Query(qry)

		lib.C(err)
		var rows []Tables.OrderRow
		//var rows []string
		for iterator.Next() {
			var row Tables.OrderRow
			//var ca string
			err = iterator.Scan(
				&row.ID,
				&row.ClientID,
				&row.CreatedAt,
				&row.CustomerID,
				&row.CurrentPoints,
				&row.CustomerType,
				&row.CustomerName,
				&row.LocationID,
				&row.LocationName,
				&row.Voided,
				&row.CompletedOn,
				&row.OrderStatus,
				&row.OrderType,
			)
			//err = iterator.Scan(
			//	&ca,
			//)
			lib.C(err)

			fmt.Println(row)
			rows = append(rows, row)
			//rows = append(rows, ca)
		}

		go ProcessOrders(rows, db)

		fmt.Println("\n\n------------------")

	}
	fmt.Println("Waiting for threads...")
	wg.Wait()
}

func LoadConfiguration() (serverConfig FConfig.ServerConfig, dbConfig FConfig.DBConfig) {

	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))

	if err != nil {
		panic(err)
	}
	//	fmt.Printf("Loading config from: %s\n", dir)

	configFile := path.Join(dir, "config", "server.json")

	//	fmt.Printf("Config Path: %s\n", configFile)

	sc, err := ioutil.ReadFile(configFile)
	if err != nil {
		panic(err)
	}
	json.Unmarshal(sc, &serverConfig)

	configFile = path.Join(dir, "config", "db.json")
	sc, err = ioutil.ReadFile(configFile)
	if err != nil {
		panic(err)
	}
	json.Unmarshal(sc, &dbConfig)

	return
}
