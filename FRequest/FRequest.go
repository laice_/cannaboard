package FRequest

import (
	"bitbucket.org/laice_/cannaboard/FConfig"
	"fmt"
	"net/http"
	"net/url"
	"path"
	"strconv"
)

type FRequest struct {
	Config FConfig.FConfig
}

func (fReq *FRequest) Request(endpoint string) (res *http.Response) {
	client := &http.Client{}

	fhc := fReq.Config

	uri, err := url.Parse(fhc.Root)
	uri.Path = path.Join(uri.Path, endpoint)

	query := uri.Query()
	query.Set("user-api-key", fhc.Key)
	uri.RawQuery = query.Encode()

	if err != nil {
		panic(err)

	}

	u := uri.String()
	//fmt.Println(u)
	req, err := http.NewRequest("GET", u, nil)
	req.Host = fhc.Host
	req.URL = uri

	if err != nil {
		panic(err)
	}

	req.Header.Add("key", fhc.Key)
	req.Header.Add("clientId", fhc.ClientID)
	req.Header.Add("Accept", "application/json")
	res, err = client.Do(req)

	if err != nil {
		panic(err)
	}

	//defer res.Body.Close()

	return res

}

func (fReq *FRequest) RequestOrders(importID string, createdAfter string, createdBefore string, pageSize int) (res *http.Response) {
	client := &http.Client{}

	fhc := fReq.Config

	endpoint := "/v1/orders/findByLocationId/" + importID

	uri, err := url.Parse(fhc.Root)
	uri.Path = path.Join(uri.Path, endpoint)

	query := uri.Query()
	query.Set("created_after", createdAfter)
	if createdBefore != "" {
		query.Set("created_before", createdBefore)
	}
	if pageSize != 0 {
		query.Set("page_size", strconv.Itoa(pageSize))
	}

	uri.RawQuery = query.Encode()

	if err != nil {
		panic(err)

	}

	u := uri.String()
	fmt.Println(u)

	//u := "https://api.flowhub.co" + endpoint
	//fmt.Println(u)
	req, err := http.NewRequest("GET", u, nil)
	req.Host = fhc.Host
	req.URL = uri

	if err != nil {
		panic(err)
	}

	req.Header.Add("key", fhc.Key)
	//fmt.Println(fhc.Key)
	//fmt.Println(fhc.ClientID)
	req.Header.Add("clientId", fhc.ClientID)
	req.Header.Add("Accept", "application/json")
	res, err = client.Do(req)

	if err != nil {
		panic(err)
	}

	return res

}

func (req *FRequest) SetConfig(config FConfig.FConfig) {
	req.Config = config
}
