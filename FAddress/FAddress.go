package FAddress

//######################
//
//	FAddress
//
//**********************

type FAddress struct {
	StreetAddress1 string `json:"streetAddress1"`
	StreetAddress2 string `json:"streetAddress2"`
	City           string `json:"city"`
	County         string `json:"county"`
	State          string `json:"state"`
	Zip            string `json:"zip"`
	Country        string `json:"country"`
}
