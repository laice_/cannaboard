package main

import (
	"bitbucket.org/laice_/cannaboard/FConfig"
	"bitbucket.org/laice_/cannaboard/Stats"
	"encoding/json"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
)

func main() {

	Stats.Generate()

}

func LoadConfiguration() (serverConfig FConfig.ServerConfig, dbConfig FConfig.DBConfig) {

	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))

	if err != nil {
		panic(err)
	}
	//	fmt.Printf("Loading config from: %s\n", dir)

	configFile := path.Join(dir, "config", "server.json")

	//	fmt.Printf("Config Path: %s\n", configFile)

	sc, err := ioutil.ReadFile(configFile)
	if err != nil {
		panic(err)
	}
	json.Unmarshal(sc, &serverConfig)

	configFile = path.Join(dir, "config", "db.json")
	sc, err = ioutil.ReadFile(configFile)
	if err != nil {
		panic(err)
	}
	json.Unmarshal(sc, &dbConfig)

	return
}
