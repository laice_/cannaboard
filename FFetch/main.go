package main

import (
	"bitbucket.org/laice_/cannaboard/FConfig"
	"bitbucket.org/laice_/cannaboard/FData"
	"bitbucket.org/laice_/cannaboard/lib"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"
)

//######################
//
//	Main
//
//**********************

func main() {

	configs, serverConfig := loadConfigurations()

	fmt.Printf("Configurations loaded\n")

	var data *FData.FDataCollection

	hasCon, fetchPeriod, customAfter, customBefore := checkArgs()

	if hasCon {
		data = initWithConsole(configs, serverConfig)
		data.CL.Log("Data loaded")
	} else {
		fmt.Println("Data collection configs loaded")
		data = initWithoutConsole(configs, serverConfig)
		for _, d := range data.Data {
			lib.CE(d.FetchLocations())

			for i, loc := range d.Locations {

				if !d.Config.CheckFilter(loc.LocationID) {
					//fmt.Printf("Fetching Inventory for %s\n", loc.LocationName)
					//d.FetchInventory(i)
					if fetchPeriod != "" {

						switch fetchPeriod {
						case "t":
							d.FetchOrdersToday(i)
							break
						case "custom":
							if customAfter == "" || customBefore == "" {
								log.Fatal(fmt.Sprintf("Must define { customAfter: %s } and { customBefore: %s }", customAfter, customBefore))
								return
							}

							d.CustomFetch(i, customAfter, customBefore)
							break
						case "year":
							d.FetchOrdersLastYear(i)
							break
						case "m":
							fmt.Printf("%s Monthly Fetch\n", loc.LocationName)
							lm := time.Now().AddDate(0, -1, 0).Format(time.RFC3339)

							//TODO: Need to walk days from this original day up to today and
							//pull any missed days

							if !d.HasOrders(loc.ImportID, lm) {

								d.FetchOrdersLastMonth(i)
							} else {
								fmt.Printf("%s already has entries for %s\n", loc.LocationName, lm)
							}

							break
						case "y":
						case "yesterday":
						case "":
						case " ":
						default:
							fmt.Printf("%s Yesterday Fetch\n", loc.LocationName)
							d.FetchOrdersYesterday(i)
							//yesterday := time.Now().AddDate(0, 0, -1).Format(time.RFC3339)
							//if !d.HasOrders(loc.ImportID, yesterday) {
							//
							//
							//} else {
							//	fmt.Printf("%s already has entries for %s\n", loc.LocationName, yesterday)
							//}

							break
						}

					} else {
						fmt.Printf("No period found, yesterday fetch for %s\n", loc.LocationName)
						d.FetchOrdersYesterday(i)
					}

				}
			}

			d.Close()

		}
	}

}

func loadConfigurations() ([]FConfig.FConfig, FConfig.DBConfig) {

	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))

	if err != nil {
		panic(err)
	}

	fmt.Println(dir)

	configDir := path.Join(dir, "config")

	configFiles := configFileNames(configDir)

	var serverConfig FConfig.DBConfig

	scDir := path.Join(dir, "config/db.json")
	sc, err := ioutil.ReadFile(scDir)
	if err != nil {
		panic(err)
	}
	json.Unmarshal(sc, &serverConfig)

	fhConfigs := loadFlowhubConfigs(configFiles)

	return fhConfigs, serverConfig
}

/*

 */

func checkArgs() (hasCon bool, period string, ca string, cb string) {
	if len(os.Args) > 1 {
		args := os.Args[1:]

		for i := 0; i < len(args)-1; i++ {
			arg := args[i]
			switch arg {
			case "-c":
				hasCon = true
				break
			case "-p":
				period = args[i+1]
				ca = args[i+2]
				cb = args[i+3]
				i++
				break
			case "--quit":
				fmt.Printf("Started and quitting")
				os.Exit(0)
			}
		}

	}

	return
}

func initWithConsole(configs []FConfig.FConfig, serverConfig FConfig.DBConfig) *FData.FDataCollection {
	return FData.NewCollection(configs, serverConfig, true)
}

func initWithoutConsole(configs []FConfig.FConfig, serverConfig FConfig.DBConfig) *FData.FDataCollection {
	return FData.NewCollection(configs, serverConfig, false)
}

func loadFlowhubConfigs(configFiles []string) []FConfig.FConfig {
	configs := []FConfig.FConfig{}

	//dir = dir + "/config/"

	for _, c := range configFiles {
		var fhc FConfig.FConfig
		dir, err := os.Getwd() //filepath.Abs(filepath.Dir(os.Args[0]))
		lib.C(err)
		dir = path.Join(dir, "/config/", c)
		conf, err := ioutil.ReadFile(dir)
		if err != nil {
			panic(err)
		}
		lib.C(json.Unmarshal(conf, &fhc))
		configs = append(configs, fhc)

	}

	return configs
}

func configFileNames(dir string) []string {
	fileInfos, err := ioutil.ReadDir(dir)

	if err != nil {

		panic(err)
	}

	filenames := []string{}

	for _, file := range fileInfos {
		name := file.Name()

		nameArr := strings.Split(name, ".")

		if nameArr[0] != "db" {
			if nameArr[1] != "template" {
				filenames = append(filenames, file.Name())
			}
		}

	}
	return filenames

}
