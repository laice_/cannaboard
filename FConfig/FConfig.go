package FConfig

import "fmt"

type FConfig struct {
	Key         string `json:"Key"`
	Root        string `json:"Root"`
	Host        string `json:"Host"`
	ClientID    string `json:"ClientID"`
	LocIDFilter []int  `json:"LocIDFilter"`
}

type DBConfig struct {
	DBName  string `json:"dbname"`
	DBHost  string `json:"dbhost"`
	DBPort  string `json:"dbport"`
	DBUser  string `json:"dbuser"`
	DBPass  string `json:"dbpass"`
	SSLMode string `json:"sslmode"`
}

type ServerConfig struct {
	Host    string   `json:"host"`
	Port    string   `json:"port"`
	Cert    string   `json:"cert"`
	Key     string   `json:"key"`
	Secrets []string `json:"secrets"`
	Filter  []int    `json:"filter"`
}

func (fc *FConfig) CheckFilter(locId int) (filtered bool) {

	for _, f := range fc.LocIDFilter {
		if f == locId {
			fmt.Printf("Location ID %d is filtered\n", locId)
			return true
		}
	}

	return false
}
