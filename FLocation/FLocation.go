package FLocation

import (
	. "bitbucket.org/laice_/cannaboard/FAddress"
	"bitbucket.org/laice_/cannaboard/FInventory"
	"bitbucket.org/laice_/cannaboard/FOrders"
	"bitbucket.org/laice_/cannaboard/FRequest"
	"encoding/json"
	"fmt"
)

//######################
//
//	Location
//
//**********************

type FLocation struct {
	LocationID       int                   `json:"locationId"`
	LocationName     string                `json:"locationName"`
	ImportID         string                `json:"importId"`
	Website          string                `json:"website"`
	HoursOfOperation string                `json:"hoursOfOperation"`
	ClientId         int                   `json:"clientId"`
	ClientName       string                `json:"clientName"`
	LocationLogoURL  string                `json:"locationLogoURL"`
	TimeZone         string                `json:"timeZone"`
	Address          FAddress              `json:"address"`
	PhoneNumber      string                `json:"phoneNumber"`
	Email            string                `json:"email"`
	LicenseType      []string              `json:"licenseType"`
	Orders           []FOrders.FOrder      `json:"orders"`
	Inventory        []FInventory.FProduct `json:"products"`
}

func (loc *FLocation) String() string {

	location, err := json.Marshal(loc)

	if err != nil {
		panic(err)
	}

	return string(location)
}

func (loc *FLocation) Update(fReq *FRequest.FRequest) {
	fmt.Println("Updating " + loc.LocationName)

}

/*
func (loc *FLocation) getSales(fReq *FRequest.FRequest) {

	fReq.Request("/v0/locations/"+string(loc.LocationID)+"/sales", func(res http.Response) {
		var fRes FResponse.FSalesResponse
		fRes.Parse(res.Body, func(fRes *FResponse.FSalesResponse) {
			loc.Sales = fRes.Data

			fmt.Println("Sales built")

		})

	})

}
*/
